import { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

import PageCard from "components/PageCard";
import DataInputContainer from "./DataInputContainer";

//material ui
import { InputBase, styled } from "@mui/material";

import MDButton from "components/MDButton";

const ProductImagesData = ({ setDataObj }) => {
  const [imageListInputs, setImageListInputs] = useState([uuidv4()]);

  useEffect(() => {
    if (imageListInputs.length === 0) imageListInputs.push(1);
  }, [imageListInputs.length]);

  function handleDeleteImage(id) {
    setImageListInputs((old) => old.filter((imageInputId) => imageInputId !== id));
  }

  return (
    <PageCard title="Product Images">
      <form className="theme-form theme-form-2 mega-form">
        <div className="row">
          <DataInputContainer title="Main image">
            <input
              className="form-control form-choose"
              type="file"
              id="formFileMultiple"
              onChange={(e) => {}}
            />
          </DataInputContainer>
          {imageListInputs.map((imageListInputId, index) => (
            <DataInputContainer
              key={imageListInputId}
              title={`Image list (${index + 1})`}
              deletable={{
                isDeletable: true,
                handleDelete: handleDeleteImage,
                id: imageListInputId,
                isJustOne: imageListInputs.length === 1,
              }}
            >
              <input
                className="form-control form-choose"
                type="file"
                id="formFileMultiple"
                onChange={(e) => {
                  console.log(e.target.files);
                }}
                multiple
              />
            </DataInputContainer>
          ))}
          <MDButton
            onClick={() => {
              setImageListInputs((old) => [...old, uuidv4()]);
            }}
          >
            add another image
          </MDButton>
        </div>
      </form>
    </PageCard>
  );
};

export default ProductImagesData;
