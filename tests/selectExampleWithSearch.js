import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
i;
export default function CustomizedSelects() {
  const [age, setAge] = React.useState("");
  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <div>
      <FormControl sx={{ m: 1 }} variant="standard">
        <InputLabel id="demo-customized-select-label">Age</InputLabel>
        <Select
          select
          labelId="demo-customized-select-label"
          id="demo-customized-select"
          value={age}
          onChange={handleChange}
          // Select={<BootstrapSelect />}
          MenuProps={MenuProps}
        >
          <div style={{ position: "absolute", background: "white", width: "100%", zIndex: 9999 }}>
            ad
          </div>
          <div
            style={{
              maxHeight: "200px",
              overflow: "scroll",
              marginTop: "20px",
              height: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </div>
        </Select>
      </FormControl>
    </div>
  );
}
