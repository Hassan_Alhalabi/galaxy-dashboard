/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useState, useEffect } from "react";
import { useMutation } from "react-query";
import { editProfile, editPassword } from "./service";
import { object, string } from "yup";
import { toast } from "react-toastify";

// react-router components
import { useLocation, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// @material-ui core components
import { List, Popover, ListItem, Icon, IconButton, Toolbar, AppBar } from "@mui/material";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

// Material Dashboard 2 React example components
import Breadcrumbs from "examples/Breadcrumbs";

// Material Dashboard 2 My components
import FormModal from "components/FormModal";
import ProfileDataForm from "./UpdateProfileDateForm";
import PasswordDataForm from "./UpdatePasswordDataForm";

// Custom styles for DashboardNavbar
import {
  navbar,
  navbarContainer,
  navbarRow,
  navbarIconButton,
  navbarMobileMenu,
} from "examples/Navbars/DashboardNavbar/styles";

// Material Dashboard 2 React context
import {
  useMaterialUIController,
  setTransparentNavbar,
  setMiniSidenav,
  setOpenConfigurator,
} from "context";

function DashboardNavbar({ absolute, light, isMini }) {
  const [navbarType, setNavbarType] = useState();
  const [controller, dispatch] = useMaterialUIController();
  const { miniSidenav, transparentNavbar, fixedNavbar, openConfigurator, darkMode } = controller;
  const route = useLocation().pathname.split("/").slice(1);

  useEffect(() => {
    // Setting the navbar type
    if (fixedNavbar) {
      setNavbarType("sticky");
    } else {
      setNavbarType("static");
    }

    // A function that sets the transparent state of the navbar.
    function handleTransparentNavbar() {
      setTransparentNavbar(dispatch, (fixedNavbar && window.scrollY === 0) || !fixedNavbar);
    }

    /** 
     The event listener that's calling the handleTransparentNavbar function when 
     scrolling the window.
    */
    window.addEventListener("scroll", handleTransparentNavbar);

    // Call the handleTransparentNavbar function to set the state with the initial value.
    handleTransparentNavbar();

    // Remove event listener on cleanup
    return () => window.removeEventListener("scroll", handleTransparentNavbar);
  }, [dispatch, fixedNavbar]);

  const handleMiniSidenav = () => setMiniSidenav(dispatch, !miniSidenav);
  const handleConfiguratorOpen = () => setOpenConfigurator(dispatch, !openConfigurator);

  // Styles for the navbar icons
  const iconsStyle = ({ palette: { dark, white, text }, functions: { rgba } }) => ({
    color: () => {
      let colorValue = light || darkMode ? white.main : dark.main;

      if (transparentNavbar && !light) {
        colorValue = darkMode ? rgba(text.main, 0.6) : text.main;
      }

      return colorValue;
    },
  });

  /* User menu Start */
  //menu logic
  const [menuAnchor, setMenuAnchor] = useState(null);

  //edit profile
  const editProfileSchema = object({
    firstName: string().required("First name a is required field."),
    userName: string().required("Username a is required field."),
    email: string().required("Email a is required field.").email("Email should be a valid email."),
    phoneNumber: string().required("Phone number is a required field."),
  });

  const currentProfileData = JSON.parse(Cookies.get("GDUser"));

  const [profileDataObj, setProfileDataObj] = useState(currentProfileData);

  const [isEditProfileModalOpen, setIsEditProfileModalOpen] = useState(false);
  function handleEditProfileModalOpen() {
    setIsEditProfileModalOpen(true);
  }
  const editProfileMutation = useMutation(editProfile, {
    onSuccess: () => {
      toast.success("Your profile edited successfully.");
      Cookies.set("GDUser", JSON.stringify(profileDataObj));
    },
    onError: (error) => {
      error.resonse.data[0]
        ? toast.error(error.response.data[0])
        : toast.error("An Unexpected Error Occurred.");
    },
  });

  function handleEditProfileDataObj() {
    editProfileSchema
      .validate(profileDataObj)
      .then(() => {
        setIsEditProfileModalOpen(false);
        editProfileMutation.mutate(profileDataObj);
      })
      .catch((error) => {
        error.errors && error.errors.length > 0
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred.");
      });
  }

  //edit password
  const [passwordDataObj, setPasswordDataObj] = useState({
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  });

  const passwordSchema = (name) =>
    string()
      .min(5, `${name} must be more than or equal to 5 characters`)
      .required(`${name} is a required field.`);
  const editPasswordSchema = object({
    oldPassword: passwordSchema("Old password"),
    newPassword: passwordSchema("New password"),
    confirmPassword: passwordSchema("Confirm new password").test(
      "is-sync",
      "New Password must be same as the confirm password",
      (confirmPassword) => confirmPassword === passwordDataObj.newPassword
    ),
  });

  const [isEditPasswordModalOpen, setIsEditPasswordModalOpen] = useState(false);
  function handleEditPasswordModalOpen() {
    setIsEditPasswordModalOpen(true);
  }
  const editPasswordMutation = useMutation(editPassword, {
    onSuccess: () => {
      toast.success("Your password edited successfully.");
    },
    onError: (error) => {
      error.resonse.data[0]
        ? toast.error(error.response.data[0])
        : toast.error("An Unexpected Error Occurred.");
    },
  });

  function handleEditPasswordDataObj() {
    editPasswordSchema
      .validate(passwordDataObj)
      .then(() => {
        setIsEditPasswordModalOpen(false);
        editPasswordMutation.mutate(passwordDataObj);
      })
      .catch((error) => {
        error.errors && error.errors.length > 0
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred.");
      });
  }
  //logout
  const navigate = useNavigate();
  /* User menu end */

  return (
    <>
      <AppBar
        position={absolute ? "absolute" : navbarType}
        color="inherit"
        sx={(theme) => navbar(theme, { transparentNavbar, absolute, light, darkMode })}
      >
        <Toolbar
          sx={{
            ...(theme) => navbarContainer(theme),
            display: "flex !important",
            justifyContent: "flex-end !important",
            alignItems: "center !important",
            flexWrap: "wrap",
            gap: 1,
          }}
        >
          <MDBox
            color="inherit"
            mb={{ xs: 1, md: 0 }}
            sx={{
              ...(theme) => navbarRow(theme, { isMini }),
              flexGrow: 1,
              marginBottom: "0 !important",
            }}
          >
            <Breadcrumbs icon="home" title={route[route.length - 1]} route={route} light={light} />
          </MDBox>
          {isMini ? null : (
            <MDBox
              sx={{
                ...(theme) => navbarRow(theme, { isMini }),
                whiteSpace: "nowrap",
              }}
            >
              <MDBox color={light ? "white" : "inherit"}>
                <IconButton
                  onClick={({ currentTarget }) => setMenuAnchor(currentTarget)}
                  sx={navbarIconButton}
                  size="small"
                  disableRipple
                >
                  <Icon sx={iconsStyle}>account_circle</Icon>
                </IconButton>
                <IconButton
                  size="small"
                  disableRipple
                  color="inherit"
                  sx={navbarMobileMenu}
                  onClick={handleMiniSidenav}
                >
                  <Icon sx={iconsStyle} fontSize="medium">
                    {miniSidenav ? "menu_open" : "menu"}
                  </Icon>
                </IconButton>
                <IconButton
                  size="small"
                  disableRipple
                  color="inherit"
                  sx={navbarIconButton}
                  onClick={handleConfiguratorOpen}
                >
                  <Icon sx={iconsStyle}>settings</Icon>
                </IconButton>
              </MDBox>
            </MDBox>
          )}
        </Toolbar>
      </AppBar>
      {/* User Menu  */}
      <Popover
        open={menuAnchor ? true : false}
        anchorEl={menuAnchor}
        onClose={() => setMenuAnchor(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <List>
          <ListItem
            sx={{ py: 0.7, px: 1.5 }}
            button
            onClick={() => {
              handleEditProfileModalOpen();
              setMenuAnchor(null);
            }}
          >
            Edit Profile
          </ListItem>
          <ListItem
            sx={{ py: 0.7, px: 1.5 }}
            button
            onClick={() => {
              handleEditPasswordModalOpen();
              setMenuAnchor(null);
            }}
          >
            Change Password
          </ListItem>
          <ListItem
            sx={{ py: 0.7, px: 1.5 }}
            button
            onClick={() => {
              navigate("/logout");
              setMenuAnchor(null);
            }}
          >
            Logout
          </ListItem>
        </List>
      </Popover>
      {/* Edit Profile Dialog */}
      <FormModal
        title={`Edit Proile`}
        actionBtnTitle="Edit"
        isOpen={isEditProfileModalOpen}
        handleDoAction={handleEditProfileDataObj}
        handleClose={() => setIsEditProfileModalOpen(false)}
      >
        <ProfileDataForm dataObj={profileDataObj} setDataObj={setProfileDataObj} />
      </FormModal>
      {/* Edit Password Dialog */}
      <FormModal
        title={`Change Password `}
        actionBtnTitle="Edit"
        isOpen={isEditPasswordModalOpen}
        handleDoAction={handleEditPasswordDataObj}
        handleClose={() => setIsEditPasswordModalOpen(false)}
      >
        <PasswordDataForm dataObj={passwordDataObj} setDataObj={setPasswordDataObj} />
      </FormModal>
    </>
  );
}

// Setting default values for the props of DashboardNavbar
DashboardNavbar.defaultProps = {
  absolute: false,
  light: false,
  isMini: false,
};

// Typechecking props for the DashboardNavbar
DashboardNavbar.propTypes = {
  absolute: PropTypes.bool,
  light: PropTypes.bool,
  isMini: PropTypes.bool,
};

export default DashboardNavbar;
