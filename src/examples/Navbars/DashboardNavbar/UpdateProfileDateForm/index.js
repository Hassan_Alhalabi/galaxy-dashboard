import MDBox from "components/MDBox";
import MDInput from "components/MDInput";

import { Grid } from "@mui/material";

const DataObjForm = ({ dataObj, setDataObj }) => {
  return (
    <MDBox mx={1}>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.firstName}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                firstName: e.target.value,
              }))
            }
            margin="dense"
            id="user-first-name"
            label="First Name"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.lastName ? dataObj.lastName : ""}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                lastName: e.target.value,
              }))
            }
            margin="dense"
            id="user-last-name"
            label="Last Name"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.userName}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                userName: e.target.value,
              }))
            }
            margin="dense"
            id="user-user-name"
            label="Username"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.email}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                email: e.target.value,
              }))
            }
            margin="dense"
            id="user-email"
            label="Email"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
      <Grid container display="flex" justifyContent="space-between" alignItems="center">
        <MDInput
          value={dataObj.phoneNumber ? dataObj.phoneNumber : ""}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              phoneNumber: e.target.value,
            }))
          }
          margin="dense"
          id="user-phone-number"
          label="Phone Number"
          type="text"
          fullWidth
          variant="standard"
        />
      </Grid>
    </MDBox>
  );
};

export default DataObjForm;
