import MDBox from "components/MDBox";
import MDInput from "components/MDInput";

import { Grid } from "@mui/material";

const DataObjForm = ({ dataObj, setDataObj }) => {
  return (
    <MDBox mx={1}>
      <Grid container display="flex" justifyContent="space-between" alignItems="center">
        <MDInput
          value={dataObj.oldPassword}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              oldPassword: e.target.value,
            }))
          }
          margin="dense"
          id="old-passwrod"
          label="Old Password"
          type="password"
          fullWidth
          variant="standard"
        />
      </Grid>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.newPassword}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                newPassword: e.target.value,
              }))
            }
            margin="dense"
            id="new-password"
            label="New Password"
            type="password"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.confirmPassword}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                confirmPassword: e.target.value,
              }))
            }
            margin="dense"
            id="confirm-new-password"
            label="Confirm New Password"
            type="password"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
    </MDBox>
  );
};

export default DataObjForm;
