import http from "util/httpService";

export function editProfile(userData) {
  console.log(userData);
  return http.put("/Users/UpdateMyProfile", userData);
}

export function editPassword(passwordData) {
  console.log(passwordData);
  return http.put("/Users/ChangePassword", passwordData);
}
