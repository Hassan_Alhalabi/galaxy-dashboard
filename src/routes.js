import React from "react";
import Icon from "@mui/material/Icon";
import { Navigate } from "react-router-dom";
import Crop32Icon from "@mui/icons-material/Crop32";
import FormatListNumberedIcon from "@mui/icons-material/FormatListNumbered";

const Dashboard = React.lazy(() => import("./layouts/dashboard"));
const Coupons = React.lazy(() => import("./layouts/coupons"));
const Orders = React.lazy(() => import("./layouts/orders"));
const Zones = React.lazy(() => import("./layouts/zones"));
const Users = React.lazy(() => import("./layouts/users"));
const Products = React.lazy(() => import("./layouts/products"));
const Roles = React.lazy(() => import("./layouts/roles"));
const CreateProduct = React.lazy(() => import("./layouts/products/CreateProduct"));
const Tags = React.lazy(() => import("./layouts/tags"));
const EditProduct = React.lazy(() => import("./layouts/products/EditProduct"));
const ProductDetails = React.lazy(() => import("./layouts/products/ProductDetails"));
const Categories = React.lazy(() => import("./layouts/categories"));
const Attributes = React.lazy(() => import("./layouts/attributes"));
const SignIn = React.lazy(() => import("./auth/sign-in"));
const Logout = React.lazy(() => import("./auth/log-out"));
const ForgetPassword = React.lazy(() => import("./auth/reset-password/cover"));

const routes = [
  {
    type: "collapse",
    name: "Dashboard",
    key: "dashboard",
    icon: <Icon fontSize="small">dashboard</Icon>,
    route: "/dashboard",
    component: <Dashboard />,
    protected: true,
  },
  {
    type: "collapse",
    name: "Coupon",
    key: "coupon",
    icon: <Icon fontSize="small">discount</Icon>,
    route: "/coupon",
    component: <Coupons />,
    protected: true,
    requiredScopes: ["Permissions.Coupon.View"],
  },
  {
    type: "collapse",
    name: "orders",
    key: "orders",
    icon: <FormatListNumberedIcon />,
    route: "/orders",
    component: <Orders />,
    protected: true,
    requiredScopes: ["Permissions.Order.View"],
  },
  {
    type: "collapse",
    name: "Zones",
    key: "zones",
    icon: <Crop32Icon />,
    route: "/zones",
    component: <Zones />,
    protected: true,
  },
  {
    type: "collapse",
    name: "Users",
    key: "users",
    icon: <Icon fontSize="small">people</Icon>,
    route: "/users",
    component: <Users />,
    protected: true,
    requiredScopes: ["Permissions.User.View"],
  },
  {
    type: "collapse",
    name: "Products",
    key: "products",
    icon: <Icon fontSize="small">inventory</Icon>,
    route: "/products",
    component: <Products />,
    protected: true,
    requiredScopes: ["Permissions.Product.View"],
  },
  {
    type: "title",
    name: "Add Product",
    key: "add-product",
    icon: <Icon fontSize="small">add</Icon>,
    route: "/products/add",
    component: <CreateProduct />,
    protected: true,
    requiredScopes: ["Permissions.Product.Insert"],
  },
  {
    type: "collapse",
    name: "Roles",
    key: "roles",
    icon: <Icon fontSize="small">security</Icon>,
    route: "/roles",
    component: <Roles />,
    protected: true,
    requiredScopes: ["Permissions.Role.View"],
  },
  {
    type: "collapse",
    name: "Tags",
    key: "tags",
    icon: <Icon fontSize="small">style</Icon>,
    route: "/tags",
    component: <Tags />,
    protected: true,
  },
  {
    type: "collapse",
    name: "Categories",
    key: "categories",
    icon: <Icon fontSize="small">category</Icon>,
    route: "/categories",
    component: <Categories />,
    protected: true,
  },
  {
    type: "collapse",
    name: "Attributes",
    key: "attributes",
    icon: <Icon fontSize="small">category</Icon>,
    route: "/attributes",
    component: <Attributes />,
    protected: true,
  },
  {
    type: "title",
    name: "Products",
    key: "product-edit-redirect",
    icon: <Icon fontSize="small">edit</Icon>,
    route: "/products/edit",
    component: <Navigate to="/products" />,
    protected: true,
  },
  {
    type: "title",
    name: "Products",
    key: "product-details-redirect",
    icon: <Icon fontSize="small">product</Icon>,
    route: "/products/product",
    component: <Navigate to="/products" />,
    protected: true,
  },
  {
    type: "title",
    name: "Edit Product",
    key: "edit-product",
    icon: <Icon fontSize="small">edit</Icon>,
    route: "/products/edit/:id",
    component: <EditProduct />,
    protected: true,
  },
  {
    type: "title",
    name: "Product Details",
    key: "product-details",
    icon: <Icon fontSize="small">details</Icon>,
    route: "/products/product/:id",
    component: <ProductDetails />,
    protected: true,
  },
  {
    type: "title",
    name: "Sign In",
    key: "sign-in",
    icon: <Icon fontSize="small">login</Icon>,
    route: "/authentication/sign-in",
    component: <SignIn />,
    protected: false,
  },
  {
    type: "title",
    name: "logout",
    key: "logout",
    icon: <Icon fontSize="small">logout</Icon>,
    route: "/logout",
    component: <Logout />,
    protected: true,
  },
  {
    type: "title",
    name: "forget password",
    key: "forget-password",
    icon: <Icon fontSize="small">logout</Icon>,
    route: "/forget-password",
    component: <ForgetPassword />,
    protected: true,
  },
];

export default routes;
