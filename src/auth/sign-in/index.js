import { useState } from "react";
import { useLocation } from "react-router-dom";
import { object, string } from "yup";
import { toast } from "react-toastify";

import { Link } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";
import Switch from "@mui/material/Switch";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDInput from "components/MDInput";
import MDButton from "components/MDButton";

// Authentication layout components
import BasicLayout from "auth/components/BasicLayout";

// Images
import bgImage from "assets/images/bg-sign-in-basic.jpeg";
import Logo from "assets/images/logo.png";

import LoadingScreen from "react-loading-screen";

import { login } from "../authService";

function Basic() {
  let location = useLocation();

  let from = location.state?.from?.pathname || "/";

  const [isLoading, setIsLoading] = useState(false);
  const [rememberMe, setRememberMe] = useState(true);
  const [userInputs, setUserInputs] = useState({
    userName: "",
    password: "",
  });

  const handleInputsChange = (event) => {
    setUserInputs({
      ...userInputs,
      [event.target.name]: event.target.value,
    });
  };

  const handleSetRememberMe = () => setRememberMe(!rememberMe);

  const loginShema = object({
    userName: string().required("User name a is required field."),
    password: string()
      .min(5, "Password length must be bigger than 5.")
      .required("Password a is requried filed."),
  });

  const handleSignIn = async () => {
    setIsLoading(true);
    await loginShema
      .validate(userInputs)
      .then(() => {
        login(userInputs.userName, userInputs.password, rememberMe, from);
      })
      .catch((error) => {
        error.errors && error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred.");
      });
    setIsLoading(false);
  };

  return (
    <BasicLayout image={bgImage}>
      <Card>
        <MDBox pt={4} pb={3} px={3}>
          <img src={Logo} className="logo" />
          <MDBox component="form" role="form">
            <MDBox mb={2}>
              <MDInput
                type="text"
                label="User Name"
                onChange={(event) => handleInputsChange(event)}
                fullWidth
                value={userInputs.userName}
                name="userName"
                onKeyDown={(e) => {
                  if (e.key === "Enter") handleSignIn();
                }}
              />
            </MDBox>
            <MDBox mb={2}>
              <MDInput
                type="password"
                label="Password"
                onChange={(event) => handleInputsChange(event)}
                fullWidth
                value={userInputs.password}
                name="password"
                onKeyDown={(e) => {
                  if (e.key === "Enter") handleSignIn();
                }}
              />
            </MDBox>
            <MDBox display="flex" alignItems="center" ml={-1}>
              <Switch checked={rememberMe} onChange={handleSetRememberMe} />
              <MDTypography
                variant="button"
                fontWeight="regular"
                color="text"
                onClick={handleSetRememberMe}
                sx={{ cursor: "pointer", userSelect: "none", ml: -1 }}
              >
                &nbsp;&nbsp;Remember me
              </MDTypography>
            </MDBox>
            <MDBox mt={4} mb={1}>
              <MDButton onClick={handleSignIn} variant="gradient" color="info" fullWidth>
                sign in
              </MDButton>
            </MDBox>
            <MDBox mt={3} mb={1} textAlign="center">
              <Link to="/forget-password">
                <MDTypography
                  sx={{ cursor: "pointer", "&:hover": { color: "#111" } }}
                  variant="button"
                  color="text"
                >
                  Forget Your Password?
                </MDTypography>
              </Link>
            </MDBox>
          </MDBox>
        </MDBox>
      </Card>

      <LoadingScreen
        loading={isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </BasicLayout>
  );
}

export default Basic;
