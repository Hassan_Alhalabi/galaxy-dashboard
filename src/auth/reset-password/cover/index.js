/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useState } from "react";
import { useMutation } from "react-query";
import { step1Request, step2Request } from "./service";
import { toast } from "react-toastify";
import { object, string } from "yup";
import { useNavigate } from "react-router-dom";

// @mui material components
import { Card, CircularProgress } from "@mui/material";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDInput from "components/MDInput";
import MDButton from "components/MDButton";

// Authentication layout components
import CoverLayout from "auth/components/CoverLayout";

// Images
import bgImage from "assets/images/bg-reset-cover.jpeg";

function Cover() {
  const navigate = useNavigate();
  const [step, setStep] = useState("step1");

  //step 1
  const [phoneNumber, setPhoneNumber] = useState("");
  const step1Mutation = useMutation(step1Request, {
    onSuccess: () => {
      setStep("step2");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  const phoneNumberSchema = string()
    .required()
    .test("is-valid-phone-number", "Phone number must be valid number", (phoneNumber) =>
      /^(\+[0-9]{10,13})$/.test(phoneNumber)
    );
  async function handleStep1Submit() {
    await phoneNumberSchema
      .validate(phoneNumber)
      .then(() => {
        step1Mutation.mutate(phoneNumber);
      })
      .catch((error) => {
        error.errors ? toast.error(error.errors[0]) : toast.error("An Unexpected Error Occurred!");
      });
  }

  //step 2
  const step2DataSchema = object({
    code: string().required(),
    newPassword: string()
      .min(5, "New password length must be bigger than 5.")
      .required("New password is a requried field."),
    confirmPassword: string()
      .required("Confirm password is a requried field.")
      .test(
        "is-sync",
        "New password must be same as confirm password.",
        (confirmPassword) => confirmPassword === step2Data.newPassword
      ),
  });
  const [step2Data, setStep2Data] = useState({
    code: "",
    newPassword: "",
    confirmPassword: "",
  });
  const step2Mutation = useMutation(step2Request, {
    onSuccess: () => {
      toast.success("Your password reset successfully.");
      navigate("/Authentication/sign-in");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  async function handleStep2Submit() {
    await step2DataSchema
      .validate(step2Data)
      .then(() => {
        step2Mutation.mutate({ ...step2Data, phoneNumber });
      })
      .catch((error) => {
        error.errors ? toast.error(error.errors[0]) : toast.error("An Unexpected Error Occurred!");
      });
  }

  return (
    <CoverLayout coverHeight="50vh" image={bgImage}>
      <Card>
        <MDBox
          variant="gradient"
          bgColor="info"
          borderRadius="lg"
          coloredShadow="success"
          mx={2}
          mt={-3}
          py={2}
          mb={1}
          textAlign="censter"
        >
          <MDTypography variant="h3" fontWeight="medium" color="white" mt={1} textAlign="center">
            Reset Password
          </MDTypography>
          <MDTypography display="block" variant="button" color="white" my={1} textAlign="center">
            You will receive an code in maximum 60 seconds
          </MDTypography>
        </MDBox>
        <MDBox pt={4} pb={3} px={3}>
          {step === "step1" && (
            <MDBox component="form" role="form">
              <MDBox mb={4}>
                <MDInput
                  value={phoneNumber}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                  type="text"
                  label="Phone Number"
                  variant="standard"
                  fullWidth
                />
              </MDBox>
              <MDBox mt={6} mb={1}>
                <MDButton
                  onClick={() => {
                    !step1Mutation.isLoading ? handleStep1Submit() : null;
                  }}
                  variant="gradient"
                  color="info"
                  fullWidth
                >
                  {step1Mutation.isLoading ? (
                    <CircularProgress color="light" size={20} />
                  ) : (
                    "Send Code"
                  )}
                </MDButton>
              </MDBox>
            </MDBox>
          )}
          {step === "step2" && (
            <MDBox component="form" role="form">
              <MDBox mb={4}>
                <MDInput
                  value={step2Data.code}
                  onChange={(e) =>
                    setStep2Data((oldForm) => ({ ...oldForm, code: e.target.value }))
                  }
                  type="text"
                  label="Verification code"
                  variant="standard"
                  fullWidth
                />
              </MDBox>
              <MDBox mb={4}>
                <MDInput
                  value={step2Data.newPassword}
                  onChange={(e) =>
                    setStep2Data((oldForm) => ({ ...oldForm, newPassword: e.target.value }))
                  }
                  type="password"
                  label="New password"
                  variant="standard"
                  fullWidth
                />
              </MDBox>
              <MDBox mb={4}>
                <MDInput
                  value={step2Data.confirmPassword}
                  onChange={(e) =>
                    setStep2Data((oldForm) => ({ ...oldForm, confirmPassword: e.target.value }))
                  }
                  type="password"
                  label="Confirm password"
                  variant="standard"
                  fullWidth
                />
              </MDBox>
              <MDBox mt={6} mb={1}>
                <MDButton
                  onClick={() => {
                    !step2Mutation.isLoading ? handleStep2Submit() : null;
                  }}
                  variant="gradient"
                  color="info"
                  fullWidth
                >
                  {step2Mutation.isLoading ? (
                    <CircularProgress color="light" size={20} />
                  ) : (
                    "Confirm"
                  )}
                </MDButton>
              </MDBox>
              <MDBox mt={3} textAlign="center">
                <MDTypography
                  onClick={() => setStep("step1")}
                  sx={{ cursor: "pointer", "&:hover": { color: "#111" } }}
                  variant="button"
                  color="text"
                >
                  Try another number
                </MDTypography>
              </MDBox>
            </MDBox>
          )}
        </MDBox>
      </Card>
    </CoverLayout>
  );
}

export default Cover;
