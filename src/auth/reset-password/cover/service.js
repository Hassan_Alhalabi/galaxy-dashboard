import http from "util/httpService";

const apiUrl = "/Authenticate";

export function step1Request(phoneNumber) {
  return http.put("Authenticate/ForgetPassword", { phoneNumber });
}

export function step2Request(data) {
  return http.put("Authenticate/ResetPassword", data);
}
