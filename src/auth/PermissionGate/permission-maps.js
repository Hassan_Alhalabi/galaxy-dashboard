import Cookies from "js-cookie";

export const userPermissions = Cookies.get("GDUserPermissions")?.split(",");

export const PERMISSIONS = [
  "Permissions.User.View",
  "Permissions.User.Insert",
  "Permissions.User.Edit",
  "Permissions.User.Delete",
  "Permissions.Role.View",
  "Permissions.Role.Insert",
  "Permissions.Role.Edit",
  "Permissions.Role.Delete",
  "Permissions.Product.Insert",
  "Permissions.Product.Edit",
  "Permissions.Product.Delete",
  "Permissions.Product.View",
  "Permissions.Product.ViewAllDetails",
  "Permissions.CompanyProduct.View",
  "Permissions.Coupon.View",
  "Permissions.Coupon.Insert",
  "Permissions.Coupon.Edit",
  "Permissions.Order.View",
  "Permissions.Category.Insert",
  "Permissions.Category.Edit",
  "Permissions.Category.Delete",
];
