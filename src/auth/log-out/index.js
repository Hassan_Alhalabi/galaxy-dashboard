import Cookies from "js-cookie";

const Logout = () => {
  Cookies.remove("GDUserToken");
  Cookies.remove("GDUserPermissions");
  Cookies.remove("GDUser");
  location.replace("/sign-in");
  return;
};

export default Logout;
