import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "App";
import "react-toastify/dist/ReactToastify.min.css";
import { toast } from "react-toastify";

import "../src/util/global.css";
import "./assets/css/vendors/bootstrap/bootstrap.css";
import "./assets/css/vendors/bootstrap/bootstrap-reboot.css";
import "./assets/css/vendors/bootstrap/bootstrap-utilities.css";
import "./assets/css/vendors/bootstrap/bootstrap.rtl.min.css";
import "./assets/css/vendors/bootstrap/bootstrap-grid.css";
import "./assets/css/vendors/animate.css";
import "./assets/css/vendors/bootstrap-tagsinput.css";
import "./assets/css/vendors/bootstrap.css";
import "./assets/css/vendors/chartist.css";
import "./assets/css/vendors/date-picker.css";
import "./assets/css/vendors/daterange-picker.css";
import "./assets/css/vendors/dropzone.css";
import "./assets/css/vendors/echart.css";
import "./assets/css/vendors/feather-icon.css";
import "./assets/css/vendors/font-awesome.css";
import "./assets/css/vendors/mapsjs-ui.css";
import "./assets/css/vendors/owlcarousel.css";
// import "./assets/css/vendors/rating.css";
import "./assets/css/vendors/ratio.css";
import "./assets/css/vendors/scrollable.css";
import "./assets/css/vendors/scrollbar.css";
import "./assets/css/vendors/sticky.css";
import "./assets/css/vendors/svg-icon.css";
import "./assets/css/vendors/sweetalert2.css";
import "./assets/css/vendors/timepicker.css";
import "./assets/css/vendors/themify.css";
import "./assets/css/vendors/vector-map.css";
import "./assets/css/linearicon.css";
import "./assets/css/magnific-popup.css";
import "./assets/css/ratio.css";
import "./assets/css/responsive.css";
import "./assets/css/select2.min.css";
import "./assets/css/slick-theme.css";
import "./assets/css/slick.css";
import "./assets/css/vector-map.css";
import "./assets/css/vectormap.css";
import "./assets/css/style.css";

// Material Dashboard 2 React Context Provider
import { MaterialUIControllerProvider } from "context";

toast.configure();

ReactDOM.render(
  <BrowserRouter>
    <MaterialUIControllerProvider>
      <App />
    </MaterialUIControllerProvider>
  </BrowserRouter>,
  document.getElementById("root")
);
