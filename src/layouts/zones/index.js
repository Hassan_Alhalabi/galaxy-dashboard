import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getZones as getDataReq,
  createZone as createDataObj,
  editZone as editDataObj,
  deleteZone as deleteDataObj,
} from "./service";
import { object, string, number, array } from "yup";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton/index";
import MDBox from "components/MDBox/index";

//my components
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import DataDialog from "components/DataDialog/index";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import AlertDialog from "components/AlertDialog";
import PermissionsGate from "auth/PermissionGate";
import PageContainer from "components/PageContainer";

//mui components
import { Grid, IconButton, Icon } from "@mui/material";

const Zones = () => {
  const queryClient = useQueryClient();
  const dataObjSchema = object({
    nameEn: string().required("You must add zone name in english langauge."),
    nameAr: string().required("You must add zone name in arabic langauge."),
    fee: number().min(0, "fee must be greater than 0").required("Fee is a required field"),
    cities: array(
      object({
        nameEn: string().required("City name in english language is a required field."),
        nameAr: string().required("City name in arabic language is a required field."),
      })
    ),
  });

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
  } = useQuery(["zones"], getDataReq, {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    setIsDeleteModalOpen(false);
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    nameEn: "",
    nameAr: "",
    fee: 0,
    cities: [],
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      setIsCreateModalOpen(false);
      toast.success("Zone added successfully.");
      queryClient.invalidateQueries("zones");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  const handleCreateDataObj = () => {
    dataObjSchema
      .validate(dataObj)
      .then(() => {
        createMutation.mutate(dataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  };

  /* Delete Query */
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  function handleDeleteModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsDeleteModalOpen(true);
  }
  const deleteMutation = useMutation(deleteDataObj, {
    onSuccess: () => {
      toast.success("Zone deleted successfully.");
      queryClient.invalidateQueries("zones");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleDeleteDataObj() {
    setIsDeleteModalOpen(false);
    deleteMutation.mutate(selectedDataObj.zoneId);
  }

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditModalOpen(false);
      toast.success("zone edited successfully");
      queryClient.invalidateQueries("zones");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleEditDataObj() {
    dataObjSchema
      .validate(selectedDataObj)
      .then(() => {
        editMutation.mutate(selectedDataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  }

  /* Permissions Dialog */
  const [isCitesOpen, setIsCitesOpen] = useState(false);
  const [selectedZoneCities, setSelectedZoneCities] = useState(null);
  function handleOpenZoneCites(zone) {
    setSelectedZoneCities(zone);
    setIsCitesOpen(true);
  }

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PermissionsGate requiredScopes={[]}>
          <PageContainer
            title={
              <MDBox display="flex" alignItems="center" gap={1}>
                <MDTypography variant="h3">Zones</MDTypography>
                <IconButton onClick={() => setIsCreateModalOpen(true)}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </MDBox>
            }
          >
            <div className="card">
              <div className="card-body">
                <div>
                  <div className="table-responsive table-desi table-product">
                    <table className="table table-1d all-package">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Cites</th>
                          <th>Fee</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        {isPageError && (
                          <MDTypography
                            sx={{
                              mx: 2,
                              my: 2,
                              pb: 2,
                              whiteSpace: "nowrap",
                              textAlign: "center",
                              width: "100%",
                            }}
                          >
                            There is no data to display
                          </MDTypography>
                        )}
                        {isPageSuccess &&
                          dataReq.map((zone) => (
                            <tr key={zone.zoneId}>
                              <td>
                                <MDTypography variant="subtitle2">{zone.nameEn}</MDTypography>
                              </td>
                              <td>
                                <MDButton
                                  sx={{ textDecoration: "underline" }}
                                  onClick={() => handleOpenZoneCites(zone)}
                                >
                                  Cites
                                </MDButton>
                              </td>
                              <td>
                                <MDTypography variant="subtitle2">{zone.fee}</MDTypography>
                              </td>
                              <td>
                                <ul>
                                  <PermissionsGate requiredScopes={[]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleEditModalOpen(zone)}
                                      >
                                        <i className="lnr lnr-pencil"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                  <PermissionsGate requiredScopes={[]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleDeleteModalOpen(zone)}
                                      >
                                        <i className="far fa-trash-alt theme-color"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                </ul>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            {/* Container-fluid Ends */}
          </PageContainer>
          {/* Create Dialog */}
          <FormModal
            title="Add Zone"
            actionBtnTitle="Add"
            isOpen={isCreateModalOpen}
            handleDoAction={handleCreateDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={dataObj} setDataObj={setDataObj} />
          </FormModal>
          {/* Delete Dialog */}
          <AlertDialog
            isOpen={isDeleteModalOpen}
            handleClose={handleCloseModal}
            handleAction={handleDeleteDataObj}
            title="Delete!"
            desc={`Are You Sure You Want To Delete ${selectedDataObj?.nameEn}?`}
          />
          {/* Edit Dialog */}
          <FormModal
            title={`Edit Zone`}
            actionBtnTitle="Edit"
            isOpen={isEditModalOpen}
            handleDoAction={handleEditDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
          </FormModal>
          {/* Show Cities Dialog */}
          {isPageSuccess && (
            <DataDialog
              isOpen={isCitesOpen}
              title={`Cites for ${selectedZoneCities?.nameEn}`}
              handleClose={() => setIsCitesOpen(false)}
            >
              <Grid container mt={1}>
                {selectedZoneCities?.cities?.map((city) => (
                  <Grid sl={12} md={6} item key={city.cityId}>
                    <MDTypography mb={0.6}>{city.nameEn}</MDTypography>
                  </Grid>
                ))}
              </Grid>
            </DataDialog>
          )}
        </PermissionsGate>
      </DashboardLayout>
      <LoadingScreen
        loading={editMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={createMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Zones;
