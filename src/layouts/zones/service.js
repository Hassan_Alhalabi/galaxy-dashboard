import http from "util/httpService";

const apiUrl = "/Zone";

export function getZones() {
  return http.get(`${apiUrl}`);
}

export function getZone(zoneId) {
  return http.get(`${apiUrl}/${zoneId}`);
}

export function createZone(zone) {
  console.log(zone);
  return http.post(apiUrl, zone);
}

export function deleteZone(zoneId) {
  return http.delete(`${apiUrl}/${zoneId}`);
}

export function editZone(zone) {
  zone.cities = zone.cities.map((city) => {
    if (city.new) {
      city.cityId = "00000000-0000-0000-0000-000000000000";
      return city;
    }
    return city;
  });
  console.log(zone);
  return http.put(apiUrl, zone);
}
