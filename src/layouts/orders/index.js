import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getOrders as getDataReq,
  editOrder as editDataObj,
  deleteOrder as deleteDataObj,
} from "./service";
import { object, string, number, date } from "yup";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton/index";
import MDBox from "components/MDBox/index";

//my components
import OrderDetails from "./orderDetails";
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import DataDialog from "components/DataDialog/index";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Pagination from "components/MyPagination";
import AlertDialog from "components/AlertDialog";
import PermissionsGate from "auth/PermissionGate";
import PageContainer from "components/PageContainer";

//mui components
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { Grid, IconButton, Icon } from "@mui/material";

const Orders = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 30;

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
  } = useQuery(["orders", page], () => getDataReq(page, pageSize), {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditModalOpen(false);
      toast.success("Order edited successfully");
      queryClient.invalidateQueries("orders");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleEditDataObj() {
    dataObjSchema
      .validate(selectedDataObj)
      .then(() => {
        editMutation.mutate(selectedDataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  }

  /* Order Details Dialog */
  const [isOrderDetailsOpen, setIsOrderDetailsOpen] = useState(false);
  function handleOrderDetailsOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsOrderDetailsOpen(true);
  }

  const User = ({ fullName, streetAddress }) => (
    <MDBox lineHeight={1}>
      <MDTypography display="block" variant="caption" fontWeight="medium">
        {fullName}
      </MDTypography>
      <MDBox display="flex" alignItems="center" justifyContent="flex-start">
        <MDTypography variant="caption">{streetAddress}</MDTypography>
        <LocationOnIcon color="text" sx={{ transform: "scale(0.8)" }} fontSize="small" />
      </MDBox>
    </MDBox>
  );

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PermissionsGate requiredScopes={["Permissions.Order.View"]}>
          <PageContainer title="Orders List">
            <div className="card">
              <div className="card-body">
                <div>
                  <div className="table-responsive table-desi table-product">
                    <table className="table table-1d all-package">
                      <thead>
                        <tr>
                          <th style={{ textAlign: "start", paddingLeft: 30 }}>User</th>
                          <th>Status</th>
                          <th>Total products price</th>
                          <th>Options</th>
                        </tr>
                      </thead>
                      <tbody>
                        {isPageError && (
                          <MDTypography
                            sx={{
                              mx: 2,
                              my: 2,
                              pb: 2,
                              whiteSpace: "nowrap",
                              textAlign: "center",
                              width: "100%",
                            }}
                          >
                            There is no data to display
                          </MDTypography>
                        )}
                        {isPageSuccess &&
                          dataReq.companyOrders.map((order) => (
                            <tr key={order.orderId}>
                              <td style={{ textAlign: "start", paddingLeft: 30 }}>
                                <User
                                  fullName={`${order.shipping.firstName} ${
                                    order.shipping.lastName
                                  } ${
                                    order.shipping.companyName.length > 0
                                      ? `(company: ${order.shipping.companyName})`
                                      : ""
                                  }`}
                                  streetAddress={order.shipping.streetAddress}
                                />
                              </td>
                              <td>
                                <MDBox lineHeight={0.5}>
                                  <MDTypography variant="body2">
                                    {new Date(order.orderDate).toLocaleString()}
                                  </MDTypography>
                                  <MDTypography variant="body2">{order.orderStatus}</MDTypography>
                                </MDBox>
                              </td>
                              <td>
                                <MDTypography variant="body2">
                                  {order.orderPrice.total} AED
                                </MDTypography>
                              </td>
                              <td>
                                <ul style={{ paddingRight: 0 }}>
                                  <li>
                                    <IconButton
                                      component="a"
                                      onClick={() => handleOrderDetailsOpen(order)}
                                    >
                                      <span className="lnr lnr-eye"></span>
                                    </IconButton>
                                  </li>
                                  <PermissionsGate requiredScopes={[]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleEditModalOpen(order)}
                                      >
                                        <i className="lnr lnr-pencil"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                </ul>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              {/* pagination start */}
              <div className="pagination-box">
                <nav className="ms-auto me-auto" aria-label="...">
                  {isPageSuccess && (
                    <Pagination
                      setPage={setPage}
                      currentPage={dataReq.paginationInfo.pageNo}
                      totalPages={dataReq.paginationInfo.totalPages}
                    />
                  )}
                </nav>
              </div>
              {/* pagination end */}
            </div>
            {/* Container-fluid Ends */}
          </PageContainer>
          {/* Edit Dialog */}
          <FormModal
            title={`Edit Order`}
            actionBtnTitle="Edit"
            isOpen={isEditModalOpen}
            handleDoAction={handleEditDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
          </FormModal>
          {/* Show Cities Dialog */}
          {selectedDataObj && (
            <DataDialog
              maxWidth="md"
              isOpen={isOrderDetailsOpen}
              title={`Order Details`}
              handleClose={() => setIsOrderDetailsOpen(false)}
            >
              <OrderDetails selectedDataObj={selectedDataObj} />
            </DataDialog>
          )}
        </PermissionsGate>
      </DashboardLayout>
      <LoadingScreen
        loading={editMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      {/* <LoadingScreen
        loading={createMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      /> */}
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Orders;
