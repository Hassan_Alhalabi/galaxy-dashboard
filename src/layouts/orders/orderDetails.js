import { Grid } from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import getAttributesConnectionName from "util/getAttributesConnectionName";

import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox";

export default function OrderDetails({ selectedDataObj }) {
  return (
    <>
      {/* General */}
      <Grid container mt={1} gap={2}>
        <Grid xs={12} md={5} item>
          <MDBox>
            <MDTypography variant="subtitle2">
              {`${selectedDataObj.shipping.firstName} ${selectedDataObj.shipping.lastName} ${
                selectedDataObj.shipping.companyName.length > 0
                  ? `(company: ${selectedDataObj.shipping.companyName})`
                  : ""
              }`}
            </MDTypography>
            <MDBox mt={1} display="flex" alignItems="center" justifyContent="flex-start">
              <MDTypography variant="subtitle2">
                {selectedDataObj.shipping.streetAddress}
              </MDTypography>
              <LocationOnIcon color="text" sx={{ transform: "scale(0.8)" }} fontSize="small" />
            </MDBox>
            <MDTypography mt={1} variant="subtitle2">
              {new Date(selectedDataObj.orderDate).toLocaleString()}
            </MDTypography>
          </MDBox>
        </Grid>
        <Grid xs={12} md={5} item>
          <MDBox>
            <MDTypography mt={1} variant="subtitle2">
              {selectedDataObj.orderStatus}
            </MDTypography>
            <MDTypography mt={1} variant="subtitle2">
              {selectedDataObj.isInCart ? "In cart" : "Out of cart"}
            </MDTypography>
          </MDBox>
        </Grid>
      </Grid>
      <Grid pb={2} gap={2} container mt={2}>
        {/* Products */}
        <Grid lineHeight={0.8} xs={12} md={5} item flexWrap="nowrap">
          <MDTypography variant="subtitle2" fontWeight="medium">
            Order Products
          </MDTypography>
          <MDBox mt={0.5}>
            {selectedDataObj.orderProducts.map((orderProduct) => (
              <MDBox mt={0.5} display="flex" justifyContent="space-between" alignItems="center">
                <MDTypography key={orderProduct.orderProductId} variant="catption" fontSize="small">
                  {`${orderProduct.productItem.product.title} ${
                    orderProduct.productItem.attributeConnection
                      ? `(${getAttributesConnectionName(
                          orderProduct.productItem.attributeConnection
                        ).join(" - ")})`
                      : ""
                  }`}
                </MDTypography>
                <MDTypography variant="catption" fontSize="small" mr={5}>
                  {orderProduct.quantity}
                </MDTypography>
              </MDBox>
            ))}
          </MDBox>
        </Grid>
        {/* Price */}
        <Grid lineHeight={0.8} xs={12} md={5} item>
          <MDTypography variant="subtitle2" fontWeight="medium">
            Order Price
          </MDTypography>
          <MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Total Products Price :
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.total}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Product VAT:
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.productVat}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Charging Fees:
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.chargingFees}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Chargin VAT:
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.chargingVat}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Grand Totla:
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.grandTotal}
              </MDTypography>
            </MDBox>
            <MDBox display="flex" justifyContent="space-between">
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                Coupon Discount:
              </MDTypography>
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.couponDiscount}
              </MDTypography>
            </MDBox>
            {selectedDataObj.orderPrice.couponMessage && (
              <MDTypography mt={0.5} variant="catption" fontSize="small">
                {selectedDataObj.orderPrice.couponMessage}
              </MDTypography>
            )}
          </MDBox>
        </Grid>
      </Grid>
      {/* User Account */}
      {selectedDataObj.user && (
        <Grid
          p={2}
          mb={3}
          borderRadius={5}
          sx={{ background: "#eff2f7" }}
          columnGap={2}
          rowGap={1}
          container
          mt={2}
        >
          <Grid xs={12} item>
            <MDTypography fontWeight="medium">User Account</MDTypography>
          </Grid>
          <Grid lineHeight={0.8} xs={12} md={5} item>
            <MDTypography variant="catption" fontSize="small">
              Name:{" "}
              {`${selectedDataObj.user.firstName} ${selectedDataObj.user.lastName} (${selectedDataObj.user.userName})`}
            </MDTypography>
            <MDTypography mt={1} variant="catption" fontSize="small">
              email: {selectedDataObj.user.email}
            </MDTypography>
          </Grid>
          <Grid lineHeight={0.8} xs={12} md={5} item>
            <MDTypography variant="catption" fontSize="small">
              phoneNumber: {selectedDataObj.user.phoneNumber}
            </MDTypography>
          </Grid>
        </Grid>
      )}
    </>
  );
}
