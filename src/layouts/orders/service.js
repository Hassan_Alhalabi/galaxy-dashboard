import http from "util/httpService";

const apiUrl = "/Order";

export function getOrders(page, pageSize) {
  return http.get(`${apiUrl}/all?page=${page}&pageSize=${pageSize}`);
}

export function getOrder(orderId) {
  return http.get(`${apiUrl}/${orderId}`);
}

export function deleteOrder(orderId) {
  return http.delete(`${apiUrl}/${orderId}`);
}

export function editOrder(order) {
  console.log(order);
  return http.put(apiUrl, order);
}
