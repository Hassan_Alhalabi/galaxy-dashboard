import { v4 as uuidv4 } from "uuid";

import MDButton from "components/MDButton";
import MDInput from "components/MDInput";

import { Grid, IconButton } from "@mui/material";

const ZoneCities = ({ dataObj, setDataObj }) => {
  return (
    <>
      {dataObj.cities.map((city) => (
        <Grid
          container
          columnSpacing={4}
          rowSpacing={1}
          display="flex"
          justifyContent="space-between"
          alignItems="flex-end"
          key={city.cityId}
        >
          <Grid item ls={5} xs={12}>
            <MDInput
              autoFocus
              value={city.nameEn}
              onChange={(e) =>
                setDataObj((oldForm) => ({
                  ...oldForm,
                  cities: oldForm.cities.map((c) => {
                    if (city.cityId === c.cityId) {
                      c.nameEn = e.target.value;
                      return c;
                    }
                    return c;
                  }),
                }))
              }
              margin="dense"
              id="title-en"
              label="Title (EN)"
              type="text"
              fullWidth
              variant="standard"
            />
          </Grid>
          <Grid item ls={5} xs={11}>
            <MDInput
              value={city.nameAr}
              onChange={(e) =>
                setDataObj((oldForm) => ({
                  ...oldForm,
                  cities: oldForm.cities.map((c) => {
                    if (city.cityId === c.cityId) {
                      c.nameAr = e.target.value;
                      return c;
                    }
                    return c;
                  }),
                }))
              }
              margin="dense"
              id="title-ar"
              label="Title (AR)"
              type="text"
              fullWidth
              variant="standard"
              sx={{ direction: "rtl" }}
            />
          </Grid>
          <Grid item ls={2} xs={1}>
            <IconButton
              size="small"
              component="a"
              onClick={() => {
                setDataObj((oldForm) => {
                  const cities = oldForm.cities.filter((c) => {
                    return c.cityId !== city.cityId;
                  });
                  return { ...oldForm, cities };
                });
              }}
            >
              <i className="far fa-trash-alt theme-color"></i>
            </IconButton>
          </Grid>
        </Grid>
      ))}
      <MDButton
        sx={{ mt: 2 }}
        onClick={() =>
          setDataObj((oldForm) => {
            const cities = [...oldForm.cities];
            cities.push({
              cityId: uuidv4(),
              nameEn: "",
              nameAr: "",
              new: true,
            });
            return { ...oldForm, cities };
          })
        }
        fullWidth
      >
        {`Add ${dataObj.cities.length > 0 ? "another" : ""} city`}
      </MDButton>
    </>
  );
};

export default ZoneCities;
