import ZoneCities from "./ZoneCities";

import MDBox from "components/MDBox";
import MDInput from "components/MDInput";
import MDTypography from "components/MDTypography";

import { Grid } from "@mui/material";

const DataObjForm = ({ dataObj, setDataObj }) => {
  return (
    <MDBox mx={1}>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.nameEn}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                nameEn: e.target.value,
              }))
            }
            margin="dense"
            id="title-en"
            label="Title (EN)"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.nameAr}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                nameAr: e.target.value,
              }))
            }
            margin="dense"
            id="title-ar"
            label="Title (AR)"
            type="text"
            fullWidth
            variant="standard"
            sx={{ direction: "rtl" }}
          />
        </Grid>
      </Grid>
      <MDBox mt={1}>
        <MDInput
          autoFocus
          value={dataObj.fee}
          onChange={(e) =>
            setDataObj((oldForm) => ({
              ...oldForm,
              fee: e.target.value,
            }))
          }
          margin="dense"
          id="tag-title"
          label="Delivery fee"
          type="number"
          fullWidth
          variant="standard"
        />
      </MDBox>
      <MDBox mx={1} mt={2}>
        <MDTypography sx={{ color: "#7b809a" }}>Zone Cities:</MDTypography>
        <MDBox>
          <ZoneCities dataObj={dataObj} setDataObj={setDataObj} />
        </MDBox>
      </MDBox>
    </MDBox>
  );
};

export default DataObjForm;
