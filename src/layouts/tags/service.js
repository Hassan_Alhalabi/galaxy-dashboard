import http from "util/httpService";

const apiUrl = "/Tag";

export function getTags(page, pageSize) {
  return http.get(`${apiUrl}?page=${page}&pageSize=${pageSize}`);
}

export function getTag(tagId) {
  return http.get(`${apiUrl}/${tagId}`);
}

export function createTag(tag) {
  return http.post(apiUrl, tag);
}
