import MDBox from "components/MDBox";
import MDInput from "components/MDInput";

const DataObjForm = ({ dataObj, setDataObj, variant }) => {
  return (
    <MDBox mx={1}>
      <MDInput
        autoFocus
        value={dataObj.title}
        onChange={(e) =>
          setDataObj((oldForm) => ({
            ...oldForm,
            title: e.target.value,
          }))
        }
        margin="dense"
        id="tag-title"
        label="Tag Title"
        type="text"
        fullWidth
        variant="standard"
      />
    </MDBox>
  );
};

export default DataObjForm;
