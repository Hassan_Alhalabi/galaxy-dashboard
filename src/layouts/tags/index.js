import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { getTags as getDataReq, createTag as createDataObj } from "./service";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox";

//my components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Pagination from "components/MyPagination";
import PageContainer from "components/PageContainer";
import FormModal from "components/FormModal";
import DataForm from "./DataForm";

//mui components
import { Icon, IconButton } from "@mui/material";

const Tags = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 50;

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
    isPreviousData,
    refetch,
  } = useQuery(["tags", page], () => getDataReq(page, pageSize), {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    keepPreviousData: true,
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
  };

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    title: "",
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      toast.success("Tag added successfully.");
      queryClient.invalidateQueries("tags");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  const handleCreateDataObj = () => {
    setIsCreateModalOpen(false);
    createMutation.mutate(dataObj);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <PageContainer
        title={
          <MDBox display="flex" alignItems="center" gap={1}>
            <MDTypography variant="h3">Tags List</MDTypography>
            <IconButton onClick={() => setIsCreateModalOpen(true)}>
              <MDBox
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                size="large"
                bgColor="secondary"
              >
                <Icon sx={{ color: "#fff" }}>add</Icon>
              </MDBox>
            </IconButton>
          </MDBox>
        }
      >
        <div className="card">
          <div className="card-body">
            {isPageSuccess && (
              <MDBox display="flex" gap={3} flexWrap="wrap">
                {dataReq.tags.map((tag) => (
                  <MDTypography color="primary" key={tag.tagId} sx={{ overflowWrap: "anywhere" }}>
                    #{tag.title}
                  </MDTypography>
                ))}
              </MDBox>
            )}
            {isPageError && (
              <MDTypography sx={{ mx: 2, my: 2, pb: 2, whiteSpace: "nowrap", textAlign: "center" }}>
                There is no data to display
              </MDTypography>
            )}
          </div>
          <div className="pagination-box">
            <nav className="ms-auto me-auto" aria-label="...">
              {isPageSuccess && (
                <Pagination
                  setPage={setPage}
                  currentPage={dataReq.paginationInfo.pageNo}
                  totalPages={dataReq.paginationInfo.totalPages}
                />
              )}
            </nav>
          </div>
        </div>
        {/* Container-fluid Ends */}
      </PageContainer>
      <LoadingScreen
        loading={isPreviousData}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      {/* Create Dialog */}
      <FormModal
        title="Add Tag"
        actionBtnTitle="Add"
        isOpen={isCreateModalOpen}
        handleDoAction={handleCreateDataObj}
        handleClose={handleCloseModal}
      >
        <DataForm dataObj={dataObj} setDataObj={setDataObj} />
      </FormModal>
    </DashboardLayout>
  );
};

export default Tags;
