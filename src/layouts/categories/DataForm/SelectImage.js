import React from "react";
import ImageUploading from "react-images-uploading";

import MDTypography from "components/MDTypography/index";
import MDBox from "components/MDBox/index";

const SelectImage = ({ dataObj, setDataObj }) => {
  const onChange = (newImage) => {
    setDataObj((oldForm) => ({ ...oldForm, image: newImage[0] }));
  };

  return (
    <ImageUploading value={dataObj.image} onChange={onChange} dataURLKey="data_url">
      {({ dragProps, onImageUpdate, onImageRemove, onImageRemoveAll, onImageUpload }) => (
        // write your building UI
        <MDBox m="10px auto 0">
          {!dataObj.image && (
            <MDBox
              sx={{
                width: 200,
                height: 200,
                cursor: "pointer",
                outline: "0.5px solid #888",
              }}
              display="inline-flex"
              justifyContent="center"
              alignItems="center"
              alt=""
              {...dragProps}
              onClick={() => onImageUpdate(0)}
            >
              <MDTypography textAlign="center">Add Category Image (Drag and Drop)</MDTypography>
            </MDBox>
          )}
          {dataObj.image && (
            <MDBox
              sx={{
                width: 200,
                height: 200,
              }}
              display="inline-flex"
              flexDirection="column"
              alignItems="center"
            >
              <MDTypography fontWeight="bold" mb={1}>
                Category Image
              </MDTypography>
              <img
                src={dataObj.image.data_url || dataObj.image.downloadUrl}
                alt=""
                style={{
                  display: "inline-block",
                  objectFit: "cover",
                  maxWidth: 150,
                  maxHeight: 150,
                }}
              />
              <MDBox mt={1} sx={{ justifySelf: "flex-end" }}>
                <i
                  style={{ marginRight: 10, cursor: "pointer" }}
                  className="far fa-trash-alt theme-color"
                  onClick={() => {
                    setDataObj((old) => ({ ...old, image: null, imageId: "" }));
                  }}
                ></i>
                <span
                  style={{ cursor: "pointer" }}
                  className="lnr lnr-pencil"
                  onClick={() => {
                    onImageRemoveAll();
                    onImageUpload();
                  }}
                ></span>
              </MDBox>
            </MDBox>
          )}
        </MDBox>
      )}
    </ImageUploading>
  );
};

export default SelectImage;
