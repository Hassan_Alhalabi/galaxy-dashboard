//my component
import SelectImage from "./SelectImage";

import SelectSuperCategory from "./SelectSuperCategory";

import MDBox from "components/MDBox";
import MDInput from "components/MDInput";

//material ui
import { Grid } from "@mui/material";

/* ls is new breakpoint (light small) */

const DataObjForm = ({ dataObj, setDataObj, variant }) => {
  return (
    <MDBox mx={1}>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.title}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                title: e.target.value,
              }))
            }
            margin="dense"
            id="title-en"
            label="Title (EN)"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.titleAr}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                titleAr: e.target.value,
              }))
            }
            margin="dense"
            id="title-ar"
            label="Title (AR)"
            type="text"
            fullWidth
            variant="standard"
            sx={{ direction: "rtl" }}
          />
        </Grid>
      </Grid>
      <Grid
        mb={2}
        mt={2}
        container
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <SelectImage dataObj={dataObj} setDataObj={setDataObj} />
      </Grid>
      <Grid
        mb={2}
        mt={4}
        container
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <SelectSuperCategory variant={variant} dataObj={dataObj} setDataObj={setDataObj} />
      </Grid>
    </MDBox>
  );
};

export default DataObjForm;
