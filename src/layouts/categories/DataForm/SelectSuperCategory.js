import { useState, useEffect } from "react";
import { getAllCategories } from "../service";
import { toast } from "react-toastify";
import { useQuery } from "react-query";
import findParentIds from "util/findParentsIds";

import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import MDBox from "components/MDBox";

import { CircularProgress, Checkbox } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeView from "@mui/lab/TreeView";
import TreeItem from "@mui/lab/TreeItem";

const SelectSuperCategory = ({ dataObj, setDataObj, variant }) => {
  const [parentsIdsState, setParentsIdsState] = useState(null);
  /* Get all categories */
  const { data: categoriesRes, status: categoriesStatus } = useQuery(
    ["allCategories"],
    getAllCategories,
    {
      staleTime: 90 * 1000,
      onError: (error) => {
        if (error.response.data[0])
          error.isExpectedError ? toast.error(error.response.data[0]) : null;
        else toast.error("An UnexpectedError Occured");
      },
    }
  );

  useEffect(() => {
    if (variant === "edit" && categoriesStatus === "success")
      findParentIds({
        list: categoriesRes.categories,
        targetChild: dataObj.categoryId,
        setParentsIdsState,
      });
  }, [categoriesStatus === "success"]);

  return (
    <>
      {categoriesStatus === "success" && categoriesRes.categories[0] && (
        <>
          <MDBox display="flex" flexWrap="wrap" gap={2}>
            <MDTypography>Is this category a subcategory?</MDTypography>
            <MDBox>
              <MDButton
                color="productButton"
                sx={{ color: "#444 !important" }}
                variant={isSub(dataObj.superCategoryId) ? "contained" : "outlined"}
                onClick={() =>
                  setDataObj((old) => ({
                    ...old,
                    superCategoryId: categoriesRes.categories[0].categoryId,
                  }))
                }
              >
                Yes
              </MDButton>
              <MDButton
                color="productButton"
                display="inline-block"
                sx={{ ml: 2, color: "#444 !important" }}
                variant={!isSub(dataObj.superCategoryId) ? "contained" : "outlined"}
                onClick={() =>
                  setDataObj((old) => ({
                    ...old,
                    superCategoryId: "00000000-0000-0000-0000-000000000000",
                  }))
                }
              >
                No
              </MDButton>
            </MDBox>
          </MDBox>
          {isSub(dataObj.superCategoryId) &&
            (variant === "edit" ? Array.isArray(parentsIdsState) : true) && (
              <TreeView
                aria-label="rich object"
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpanded={parentsIdsState ? parentsIdsState : ["root"]}
                defaultExpandIcon={<ChevronRightIcon />}
                sx={{ flexGrow: 1, overflowY: "auto", mt: 3 }}
                multiSelect
                onNodeSelect={(e, nodeIds) =>
                  setDataObj((oldForm) => ({ ...oldForm, superCategoryId: nodeIds[0] }))
                }
              >
                {categoriesRes.categories.map((category) => renderTree(category, dataObj))}
              </TreeView>
            )}
        </>
      )}
      {categoriesStatus === "error" && (
        <MDTypography sx={{ textAlign: "center" }}>
          You can't choose the super category if need it because of an error
        </MDTypography>
      )}
      {categoriesStatus === "success" && !categoriesRes.categories[0] && (
        <MDTypography sx={{ textAlign: "center" }}>
          You must add one category at least to make this category a subcategory
        </MDTypography>
      )}
      {categoriesStatus === "loading" && <CircularProgress sx={{ margin: "0 auto" }} />}
    </>
  );
};

export default SelectSuperCategory;

function renderTree(nodes, dataObj) {
  return (
    <TreeItem
      key={nodes.categoryId}
      nodeId={nodes.categoryId}
      //user can't select itself or from it's children as a super category
      disabled={dataObj.categoryId === nodes.categoryId}
      label={
        <MDBox display="flex" alignItems="center">
          <Checkbox checked={nodes.categoryId === dataObj.superCategoryId} />
          <MDTypography>{nodes.title}</MDTypography>
          {nodes.isBest && <Chip size="small" label="Best" color="primary" />}
        </MDBox>
      }
    >
      {Array.isArray(nodes.subCategories)
        ? nodes.subCategories.map((node) => renderTree(node, dataObj))
        : null}
    </TreeItem>
  );
}

function isSub(id) {
  if (id === "00000000-0000-0000-0000-000000000000") return false;
  else return true;
}
