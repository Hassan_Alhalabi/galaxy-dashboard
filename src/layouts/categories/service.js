import http from "util/httpService";

const apiUrl = "/Category";

export function getCategories(page, pageSize) {
  return http.get(`${apiUrl}?page=${page}&pageSize=${pageSize}`);
}

export function getAllCategories() {
  return http.get(`${apiUrl}?page=1&pageSize=42874`);
}

export function getCategory(categoryId) {
  return http.get(`${apiUrl}/${categoryId}`);
}

export function createCategory(category) {
  console.log(category);
  return http.post(apiUrl, category);
}

export function deleteCategory(categoryId) {
  return http.delete(`${apiUrl}/${categoryId}`);
}

export function editCategory(category) {
  console.log(category);
  return http.put(apiUrl, category);
}
