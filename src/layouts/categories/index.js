import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getCategories as getDataReq,
  createCategory as createDataObj,
  deleteCategory as deleteDataObj,
  editCategory as editDataObj,
} from "./service";
import { renderTree } from "./renderCategoriesTree";
import FormData from "form-data";
import axios from "axios";
import { object, string } from "yup";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox/index";

//My components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import AlertDialog from "components/AlertDialog";
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import Pagination from "components/MyPagination";
import PageContainer from "components/PageContainer";
import PermissionsGate from "auth/PermissionGate";

//mui components
import { IconButton, Icon } from "@mui/material";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

const Categories = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 30;
  const [selectedImage, setSelectedImage] = useState(null);
  const [isCreatingCategory, setIsCreatingCategory] = useState(false);
  const [isEditingCategory, setIsEditingCategory] = useState(false);

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
    isPreviousData,
  } = useQuery(["categories", page], () => getDataReq(page, pageSize), {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    keepPreviousData: true,
    onError: (error) => {
      setIsCreatingCategory(false);
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    // setIsDeleteModalOpen(false);
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  const categorySchemaCreate = object({
    title: string().required("You must add the title in english language"),
    titleAr: string().required("You must add the title in arabic language"),
  });

  const categorySchemaEdit = object({
    title: string().required("You must add the title in english language"),
    titleAr: string().required("You must add the title in arabic language"),
    superCategoryId: string().test(
      "is-superCategory-same-of-the-category",
      "You can't choose super category same as the current category",
      (superCategoryId) => superCategoryId !== selectedDataObj.categoryId
    ),
  });

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    title: "",
    titleAr: "",
    imageId: "",
    superCategoryId: "00000000-0000-0000-0000-000000000000",
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      setIsCreateModalOpen(false);
      setIsCreatingCategory(false);
      toast.success("Category added successfully.");
      queryClient.invalidateQueries("categories");
      queryClient.invalidateQueries("allCategories");
    },
    onError: (error) => {
      setIsCreatingCategory(false);
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });

  const handleCreateDataObj = async () => {
    setIsCreatingCategory(true);

    await categorySchemaCreate
      .validate(dataObj)
      .then(() => {
        setIsCreateModalOpen(false);
        if (dataObj.image) {
          let data = new FormData();
          data.append("file", dataObj.image.file);
          axios
            .post("Attachment/upload", data, {
              headers: {
                accept: "application/json",
                "Accept-Language": "en-US,en;q=0.8",
                "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
              },
            })
            .then((attachmentReq) => {
              setDataObj((oldForm) => ({ ...oldForm, imageId: attachmentReq.attachmentId }));
              createMutation.mutate({ ...dataObj, imageId: attachmentReq.attachmentId });
            })
            .catch((error) => {
              error.isExpectedError ? toast.error(error.response.data[0]) : null;
              setIsCreatingCategory(false);
            });
        } else {
          createMutation.mutate(dataObj);
        }
      })
      .catch((err) => {
        setIsCreatingCategory(false);
        if (err.errors)
          err.errors[0] ? toast.error(err.errors[0]) : toast.error("An Unexpected Error Occurred");
      });
  };

  /* Delete Query */
  // const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  // function handleDeleteModalOpen(dataObj) {
  //   setSelectedDataObj(dataObj);
  //   setIsDeleteModalOpen(true);
  // }
  // const deleteMutation = useMutation(deleteDataObj, {
  //   onSuccess: () => {
  //     toast.success("Category deleted successfully.");
  //     queryClient.invalidateQueries("categories");
  //   },
  //   onError: (error) => {
  //     error.isExpectedError ? toast.error(error.response.data[0]) : null;
  //   },
  // });
  // function handleDeleteDataObj() {
  //   setIsDeleteModalOpen(false);
  //   deleteMutation.mutate(selectedDataObj.categoryId);
  // }

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditingCategory(false);
      setIsEditModalOpen(false);
      toast.success("Category edited successfully");
      queryClient.invalidateQueries("categories");
      queryClient.invalidateQueries("allCategories");
    },
    onError: (error) => {
      setIsEditingCategory(false);
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  async function handleEditDataObj() {
    setIsEditingCategory(true);

    await categorySchemaEdit
      .validate(selectedDataObj)
      .then(() => {
        setIsEditModalOpen(false);
        const imageStatus = !selectedDataObj.image
          ? "notExist"
          : selectedDataObj.image.data_url
          ? "existNew"
          : "existOld";
        if (imageStatus === "existNew") {
          let data = new FormData();
          data.append("file", selectedDataObj.image.file);
          axios
            .post("Attachment/upload", data, {
              headers: {
                accept: "application/json",
                "Accept-Language": "en-US,en;q=0.8",
                "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
              },
            })
            .then((attachmentReq) => {
              setSelectedDataObj((oldForm) => ({
                ...oldForm,
                imageId: attachmentReq.attachmentId,
              }));
              editMutation.mutate({ ...selectedDataObj, imageId: attachmentReq.attachmentId });
            })
            .catch((error) => {
              setIsEditingCategory(false);
              error.isExpectedError ? toast.error(error.response.data[0]) : null;
            });
        } else if (imageStatus === "existOld") {
          editMutation.mutate({ ...selectedDataObj, imageId: selectedDataObj.image.attachmentId });
        } else if (imageStatus === "notExist") {
          editMutation.mutate(selectedDataObj);
        }
      })
      .catch((err) => {
        setIsEditingCategory(false);
        if (err.errors)
          err.errors[0] ? toast.error(err.errors[0]) : toast.error("An Unexpected Error Occurred");
      });
  }

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PageContainer
          title={
            <MDBox display="flex" alignItems="center" gap={1}>
              <MDTypography variant="h3">Categories List</MDTypography>
              <PermissionsGate requiredScopes={["Permissions.Category.Insert"]}>
                <IconButton onClick={() => setIsCreateModalOpen(true)}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </PermissionsGate>
            </MDBox>
          }
        >
          <div className="card">
            <div className="card-body">
              {isPageSuccess && (
                <TreeView
                  aria-label="categories"
                  defaultCollapseIcon={<ExpandMoreIcon />}
                  defaultExpanded={["root"]}
                  defaultExpandIcon={<ChevronRightIcon />}
                  sx={{ flexGrow: 1, overflowY: "auto" }}
                >
                  {dataReq.categories.map((category) =>
                    renderTree({
                      category,
                      handleEditModalOpen,
                      setSelectedImage,
                      // handleDeleteModalOpen,
                    })
                  )}
                </TreeView>
              )}
              {isPageError && (
                <MDTypography
                  sx={{ mx: 2, my: 2, pb: 2, whiteSpace: "nowrap", textAlign: "center" }}
                >
                  There is no data to display
                </MDTypography>
              )}
            </div>
            <div className="pagination-box">
              <nav className="ms-auto me-auto" aria-label="...">
                {isPageSuccess && (
                  <Pagination
                    setPage={setPage}
                    currentPage={dataReq.paginationInfo.pageNo}
                    totalPages={dataReq.paginationInfo.totalPages}
                  />
                )}
              </nav>
            </div>
          </div>
          {/* Container-fluid Ends */}
        </PageContainer>
        {/* Create Dialog */}
        <FormModal
          title="Add Category"
          actionBtnTitle="Add"
          isOpen={isCreateModalOpen}
          handleDoAction={handleCreateDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm dataObj={dataObj} setDataObj={setDataObj} />
        </FormModal>
        {/* Delete Dialog */}
        {/* <AlertDialog
          isOpen={isDeleteModalOpen}
          handleClose={handleCloseModal}
          handleAction={handleDeleteDataObj}
          title="Delete!"
          desc={`Are You Sure You Want To Delete ${selectedDataObj?.title}?`}
        /> */}
        {/* Edit Dialog */}
        <FormModal
          title={`Edit Category`}
          actionBtnTitle="Edit"
          isOpen={isEditModalOpen}
          handleDoAction={handleEditDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm variant="edit" dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
        </FormModal>
        {/* Image Dialog */}
        {selectedImage && (
          <MDBox
            sx={{
              position: "fixed",
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              background: "rgba(0, 0, 0, 0.4)",
              cursor: "pointer",
            }}
            display="flex"
            justifyContent="center"
            alignItems="center"
            onClick={() => setSelectedImage(null)}
          >
            <img
              src={selectedImage}
              alt=""
              style={{ maxWidth: "100vw", maxHeight: "60vh", zIndex: 9999, borderRadius: "15px" }}
            />
            <MDBox
              sx={{
                position: "absolute",
                top: 100,
                right: 20,
                background: "rgba(255, 255, 255, 0.7)",
                borderRadius: "25px",
                zIndex: 9999,
              }}
            >
              <IconButton onClick={() => setSelectedImage(null)} size="large">
                <Icon>close</Icon>
              </IconButton>
            </MDBox>
          </MDBox>
        )}
      </DashboardLayout>
      <LoadingScreen
        loading={isPreviousData}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isCreatingCategory}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isEditingCategory}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Categories;
