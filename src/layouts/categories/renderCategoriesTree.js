import PermissionsGate from "auth/PermissionGate";

import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

import { Chip, IconButton } from "@mui/material";
import TreeItem from "@mui/lab/TreeItem";

export const renderTree = ({
  category,
  setSelectedImage,
  // handleDeleteModalOpen,
  handleEditModalOpen,
}) => (
  <TreeItem
    key={category.categoryId}
    nodeId={category.categoryId}
    label={
      <MDBox display="flex" justifyContent="space-between" alignItems="center" flexWrap="wrap">
        <MDBox display="flex" gap={1} flexWrap={2} alignItems="center">
          <MDTypography>{category.title}</MDTypography>
          {category.isBest && <Chip size="small" label="Best" color="primary" />}
        </MDBox>
        <MDBox display="flex" alignItems="center" gap={0.5} flexWrap="wrap">
          {category.image?.downloadUrl ? (
            <IconButton
              size="small"
              onClick={() => {
                setSelectedImage(category.image.downloadUrl);
              }}
            >
              <img
                style={{ borderRadius: "25px", objectFit: "cover", width: 30, height: 30 }}
                width="30"
                height="30"
                src={category.image.downloadUrl}
                alt=""
              />
            </IconButton>
          ) : (
            //to make the height of category without image equal to category with image
            <MDBox style={{ height: 40 }}></MDBox>
          )}
          <PermissionsGate requiredScopes={["Permissions.Category.Edit"]}>
            <IconButton size="small" component="a" onClick={() => handleEditModalOpen(category)}>
              <span style={{ color: "#51accc" }} className="lnr lnr-pencil"></span>
            </IconButton>
          </PermissionsGate>
          {/* <PermissionsGate requiredScopes={["Permissions.Category.Delete"]}>
            <IconButton
              size="small"
              color="primary"
              component="a"
              onClick={() => handleDeleteModalOpen(category)}
            >
              <i className="far fa-trash-alt theme-color"></i>
            </IconButton>
          </PermissionsGate> */}
        </MDBox>
      </MDBox>
    }
  >
    {Array.isArray(category.subCategories)
      ? category.subCategories.map((category) =>
          renderTree({ category, setSelectedImage, handleDeleteModalOpen, handleEditModalOpen })
        )
      : null}
  </TreeItem>
);
