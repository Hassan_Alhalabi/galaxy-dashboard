import http from "../../util/httpService";

const apiUrl = "/Roles";

export function getRoles() {
  return http.get(`${apiUrl}/GetAllRoles`);
}

export function getRole(roleId) {
  return http.get(`${apiUrl}/${roleId}`);
}

export function createRole(role) {
  console.log(role);
  return http.post(apiUrl, role);
}

export function deleteRole(roleId) {
  return http.delete(`${apiUrl}/${roleId}`);
}

export function editRole(role) {
  console.log(role);
  return http.put(apiUrl, role);
}
