import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getRoles as getDataReq,
  createRole as createDataObj,
  editRole as editDataObj,
  deleteRole as deleteDataObj,
} from "./service";
import { object, string, array } from "yup";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton/index";
import MDBox from "components/MDBox/index";

//my components
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import DataDialog from "components/DataDialog/index";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import AlertDialog from "components/AlertDialog";
import PermissionsGate from "auth/PermissionGate";
import PageContainer from "components/PageContainer";

//mui components
import { Grid, IconButton, Icon } from "@mui/material";

const Roles = () => {
  const queryClient = useQueryClient();
  const rolesSchema = object({
    name: string().required("You must provide role name"),
    roleClaims: array().min(1, "You must add at least one claim for this role"),
  });

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
  } = useQuery(["roles"], getDataReq, {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    setIsDeleteModalOpen(false);
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    name: "",
    roleClaims: [],
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      setIsCreateModalOpen(false);
      toast.success("Role added successfully.");
      queryClient.invalidateQueries("roles");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  const handleCreateDataObj = () => {
    rolesSchema
      .validate(dataObj)
      .then(() => {
        createMutation.mutate(dataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  };

  /* Delete Query */
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  function handleDeleteModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsDeleteModalOpen(true);
  }
  const deleteMutation = useMutation(deleteDataObj, {
    onSuccess: () => {
      toast.success("Role deleted successfully.");
      queryClient.invalidateQueries("roles");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleDeleteDataObj() {
    setIsDeleteModalOpen(false);
    deleteMutation.mutate(selectedDataObj.id);
  }

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditModalOpen(false);
      toast.success("role edited successfully");
      queryClient.invalidateQueries("roles");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleEditDataObj() {
    rolesSchema
      .validate(selectedDataObj)
      .then(() => {
        editMutation.mutate(selectedDataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  }

  /* Permissions Dialog */
  const [isPermissionsOpen, setIsPermissionsOpen] = useState(false);
  const [selectedRolePermissions, setSelectedRolePermissions] = useState(null);
  function handleOpenRolePermissions(permissions) {
    setSelectedRolePermissions(permissions);
    setIsPermissionsOpen(true);
  }

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PermissionsGate requiredScopes={["Permissions.Role.View"]}>
          <PageContainer
            title={
              <MDBox display="flex" alignItems="center" gap={1}>
                <MDTypography variant="h3">Roles</MDTypography>
                <IconButton onClick={() => setIsCreateModalOpen(true)}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </MDBox>
            }
          >
            <div className="card">
              <div className="card-body">
                <div>
                  <div className="table-responsive table-desi table-product">
                    <table className="table table-1d all-package">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Permissions</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        {isPageError && (
                          <MDTypography
                            sx={{
                              mx: 2,
                              my: 2,
                              pb: 2,
                              whiteSpace: "nowrap",
                              textAlign: "center",
                              width: "100%",
                            }}
                          >
                            There is no data to display
                          </MDTypography>
                        )}
                        {isPageSuccess &&
                          dataReq.map((role) => (
                            <tr key={role.id}>
                              <td>
                                <MDTypography variant="subtitle2">{role.name}</MDTypography>
                              </td>
                              <td>
                                <MDButton
                                  sx={{ textDecoration: "underline" }}
                                  onClick={() => handleOpenRolePermissions(role)}
                                >
                                  Permissions
                                </MDButton>
                              </td>
                              <td>
                                <ul>
                                  <PermissionsGate requiredScopes={["Permissions.Role.Edit"]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleEditModalOpen(role)}
                                      >
                                        <i className="lnr lnr-pencil"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                  <PermissionsGate requiredScopes={["Permissions.Role.Delete"]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleDeleteModalOpen(role)}
                                      >
                                        <i className="far fa-trash-alt theme-color"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                </ul>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            {/* Container-fluid Ends */}
          </PageContainer>
          {/* Create Dialog */}
          <FormModal
            title="Add Role"
            actionBtnTitle="Add"
            isOpen={isCreateModalOpen}
            handleDoAction={handleCreateDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={dataObj} setDataObj={setDataObj} />
          </FormModal>
          {/* Delete Dialog */}
          <AlertDialog
            isOpen={isDeleteModalOpen}
            handleClose={handleCloseModal}
            handleAction={handleDeleteDataObj}
            title="Delete!"
            desc={`Are You Sure You Want To Delete ${selectedDataObj?.name}?`}
          />
          {/* Edit Dialog */}
          <FormModal
            title={`Edit Role`}
            actionBtnTitle="Edit"
            isOpen={isEditModalOpen}
            handleDoAction={handleEditDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
          </FormModal>
          {/* Show Permission Dialog */}
          {isPageSuccess && (
            <DataDialog
              isOpen={isPermissionsOpen}
              title={`Permissions for ${selectedRolePermissions?.name}`}
              handleClose={() => setIsPermissionsOpen(false)}
            >
              <Grid container mt={1}>
                {selectedRolePermissions?.roleClaims?.map((permission) => (
                  <Grid sl={12} md={6} item key={permission.claimValue}>
                    <MDTypography mb={0.6}>
                      {permission.claimValue.split(".").slice(1).join(" ")}
                    </MDTypography>
                  </Grid>
                ))}
              </Grid>
            </DataDialog>
          )}
        </PermissionsGate>
      </DashboardLayout>
      <LoadingScreen
        loading={editMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={createMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Roles;
