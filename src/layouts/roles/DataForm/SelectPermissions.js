import * as React from "react";
import { PERMISSIONS as allPermissions } from "auth/PermissionGate/permission-maps";

import { FormControlLabel, Grid, Checkbox } from "@mui/material";

import MDTypography from "components/MDTypography/index";

export default function SelectPermissions({ dataObj, setDataObj }) {
  function handleSelectedPermissionChange(claim, checked) {
    setDataObj((oldForm) => {
      let newRoleClaims = [...oldForm.roleClaims];
      if (checked) {
        newRoleClaims.push({ claimValue: claim });
      } else {
        newRoleClaims = newRoleClaims.filter((newClaim) => newClaim.claimValue !== claim);
      }
      return { ...oldForm, roleClaims: newRoleClaims };
    });
  }

  return (
    <>
      <MDTypography mb={1}>Permissions</MDTypography>
      <Grid container>
        {allPermissions.map((claim) => (
          <Grid item key={claim} ls={12} md={6}>
            <FormControlLabel
              onChange={(e) => handleSelectedPermissionChange(claim, e.target.checked)}
              label={claim.split(".").slice(1).join(" ")}
              control={
                <Checkbox
                  checked={
                    dataObj.roleClaims.find((selectedClaim) => selectedClaim.claimValue === claim)
                      ? true
                      : false
                  }
                />
              }
            />
          </Grid>
        ))}
      </Grid>
    </>
  );
}
