import SelectPermissions from "./SelectPermissions";

import MDBox from "components/MDBox";
import MDInput from "components/MDInput";

const DataObjForm = ({ dataObj, setDataObj }) => {
  return (
    <>
      <MDBox mx={1}>
        <MDInput
          autoFocus
          value={dataObj.name}
          onChange={(e) =>
            setDataObj((oldForm) => ({
              ...oldForm,
              name: e.target.value,
            }))
          }
          margin="dense"
          id="role-title"
          label="Role Name"
          type="text"
          fullWidth
          variant="standard"
        />
      </MDBox>
      <MDBox mx={1} mt={2}>
        <SelectPermissions dataObj={dataObj} setDataObj={setDataObj} />
      </MDBox>
    </>
  );
};

export default DataObjForm;
