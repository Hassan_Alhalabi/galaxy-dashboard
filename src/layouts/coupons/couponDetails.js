import { Grid } from "@mui/material";
import camelPad from "./util/camelPad";

import MDTypography from "components/MDTypography";

export default function CouponDetails({ selectedDataObj: coupon }) {
  return (
    <>
      <Grid container gap={2}>
        <Grid xs={12} md={6} item>
          <ul style={{ listStyle: "outside", marginLeft: 20 }}>
            <li>
              <MDTypography variant="subtitle2">Code: {coupon.couponCode}</MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                Quantity: {coupon.quantity}
              </MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                Discount:{" "}
                {`${coupon.discount}${coupon.isDiscountPercentage ? "%" : "AED"} (${
                  coupon.isDiscountAfterVat ? "After VAT" : "Without VAT"
                })`}
              </MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                From {formateDate(coupon.startDate)} (UAE time)
              </MDTypography>
              <MDTypography variant="subtitle2">
                to &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{formateDate(coupon.endDate)} (UAE time)
              </MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                Type: {camelPad(coupon.couponType)}
              </MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                {`${
                  coupon.allowManyTimeForSameUser ? "Allowed" : "Not allowed"
                } many time for same the user`}
              </MDTypography>
            </li>
            <li>
              <MDTypography mt={0.7} variant="subtitle2">
                The coupon is allowed if the quantity range of the cart items is between{" "}
                {coupon.minimumSpend ? coupon.minimumSpend : "1"} -{" "}
                {coupon.maximumSpend ? coupon.maximumSpend : "∞"}
              </MDTypography>
            </li>
          </ul>
        </Grid>

        <Grid xs={12} md={4} item>
          <MDTypography mb={1.2} variant="subtitle2" fontWeight="medium">
            Coupon Products
          </MDTypography>
          {coupon.couponProducts.map((couponProduct) => (
            <MDTypography mt={0.7} key={couponProduct.couponProductId} variant="subtitle2">
              {couponProduct.product.title}
            </MDTypography>
          ))}
        </Grid>
      </Grid>
    </>
  );
}

function formateDate(date) {
  return new Date(date).toLocaleString();
}
