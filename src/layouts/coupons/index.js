import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getCoupons as getDataReq,
  createCoupon as createDataObj,
  editCoupon as editDataObj,
} from "./service";
import { object, string, number, date } from "yup";
import moment from "moment";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox/index";

//my components
import Pagination from "components/MyPagination";
import CouponDetails from "./couponDetails";
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import DataDialog from "components/DataDialog/index";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import PermissionsGate from "auth/PermissionGate";
import PageContainer from "components/PageContainer";

//mui components
import { IconButton, Icon } from "@mui/material";

const Coupons = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 20;

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
  } = useQuery(["coupons", page], () => getDataReq(page, pageSize), {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  const commonSchema = {
    couponCode: string().required("Coupon code is a rquired field."),
    quantity: number()
      .min(1, "Quantity must be bigger than 0.")
      .max(2147483647, "Quantity must be smaller than 2147483647.")
      .required("Coupon code is a rquired field."),
    discount: number()
      .min(0, "Discount must be bigger than 0.")
      .required("Discount is a requried field."),
    minimumSpend: number()
      .min(1, "Minumum items quantity allowed must be greeter than one.")
      .nullable(),
    startDate: date()
      .required("Start date is required field.")
      .min(
        new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" }),
        "Start date must be later than now according to emarate date."
      ),
  };

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    couponCode: "",
    quantity: 0,
    startDate: moment(
      new Date().toLocaleString("en-US", {
        timeZone: "Asia/Dubai",
      })
    ).format("YYYY-MM-DDTHH:mm"),
    endDate: moment(
      new Date().toLocaleString("en-US", {
        timeZone: "Asia/Dubai",
      })
    ).format("YYYY-MM-DDTHH:mm"),
    discount: 0,
    isDiscountPercentage: true,
    isDiscountAfterVat: true,
    allowManyTimeForSameUser: true,
    minimumSpend: null,
    maximumSpend: null,
    couponType: "CouponAppliesForCart",
    couponProducts: [],
  });
  const createDataObjSchema = object({
    ...commonSchema,
    maximumSpend: number()
      .min(1, "Maximum items quantity allowed must be greeter than one.")
      .nullable()
      .test(
        "Is-sync",
        "Maximum items quantity allowed must be bigger than or equal to the minumum items quantity allowed.",
        (maximumSpend) => maximumSpend >= dataObj.minimumSpend
      ),
    endDate: date()
      .required("End date is a required field.")
      .min(new Date(dataObj.startDate), "Start date must be before end data -_-"),
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      setIsCreateModalOpen(false);
      toast.success("Coupon added successfully.");
      queryClient.invalidateQueries("coupons");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  const handleCreateDataObj = () => {
    createDataObjSchema
      .validate(dataObj)
      .then(() => {
        createMutation.mutate(dataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  };

  /* Edit Query */
  const editDataObjSchema = object({
    ...commonSchema,
    maximumSpend: number()
      .min(1, "Maximum items quantity allowed must be greeter than one.")
      .nullable()
      .test(
        "Is-sync",
        "Maximum items quantity allowed must be bigger than or equal to the minumum items quantity allowed.",
        (maximumSpend) => maximumSpend >= selectedDataObj.minimumSpend
      ),
    endDate: date()
      .required("End date is a required field.")
      .min(
        new Date(
          selectedDataObj?.startDate ? selectedDataObj.startDate : new Date().toLocaleString()
        ),
        "Start date must be before end data -_-"
      ),
  });
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditModalOpen(false);
      toast.success("Coupon edited successfully");
      queryClient.invalidateQueries("coupons");
    },
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });
  function handleEditDataObj() {
    editDataObjSchema
      .validate(selectedDataObj)
      .then(() => {
        editMutation.mutate(selectedDataObj);
      })
      .catch((error) => {
        error.errors[0]
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred!");
      });
  }

  /* Coupon Details Dialog */
  const [isCouponDetailsOpen, setIsCouponDetailsOpen] = useState(false);
  function handleCouponDetailsOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsCouponDetailsOpen(true);
  }

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PermissionsGate requiredScopes={["Permissions.Coupon.View"]}>
          <PageContainer
            title={
              <MDBox display="flex" alignItems="center" gap={1}>
                <MDTypography variant="h3">Coupons</MDTypography>
                <IconButton onClick={() => setIsCreateModalOpen(true)}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </MDBox>
            }
          >
            <div className="card">
              <div className="card-body">
                <div>
                  <div className="table-responsive table-desi table-product">
                    <table className="table table-1d all-package">
                      <thead>
                        <tr>
                          <th>Code</th>
                          <th>Quantity</th>
                          <th>Discount</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        {isPageError && (
                          <MDTypography
                            sx={{
                              mx: 2,
                              my: 2,
                              pb: 2,
                              whiteSpace: "nowrap",
                              textAlign: "center",
                              width: "100%",
                            }}
                          >
                            There is no data to display
                          </MDTypography>
                        )}
                        {isPageSuccess &&
                          dataReq.coupons.map((coupon) => (
                            <tr key={coupon.couponId}>
                              <td>
                                <MDTypography variant="subtitle2">{coupon.couponCode}</MDTypography>
                              </td>
                              <td>
                                <MDTypography variant="subtitle2">{coupon.quantity}</MDTypography>
                              </td>
                              <td>
                                <MDTypography variant="subtitle2">{`${coupon.discount}${
                                  coupon.isDiscountPercentage ? "%" : "AED"
                                } (${
                                  coupon.isDiscountAfterVat ? "After VAT" : "Before VAT"
                                })`}</MDTypography>
                              </td>
                              <td>
                                <ul style={{ paddingRight: 0 }}>
                                  <li>
                                    <IconButton
                                      component="a"
                                      onClick={() => handleCouponDetailsOpen(coupon)}
                                    >
                                      <span className="lnr lnr-eye"></span>
                                    </IconButton>
                                  </li>
                                  <PermissionsGate requiredScopes={["Permissions.Coupon.Edit"]}>
                                    <li>
                                      <IconButton
                                        component="a"
                                        onClick={() => handleEditModalOpen(coupon)}
                                      >
                                        <i className="lnr lnr-pencil"></i>
                                      </IconButton>
                                    </li>
                                  </PermissionsGate>
                                </ul>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              {/* pagination start */}
              <div className="pagination-box">
                <nav className="ms-auto me-auto" aria-label="...">
                  {isPageSuccess && (
                    <Pagination
                      setPage={setPage}
                      currentPage={dataReq.paginationInfo.pageNo}
                      totalPages={dataReq.paginationInfo.totalPages}
                    />
                  )}
                </nav>
              </div>
              {/* pagination end */}
            </div>
            {/* Container-fluid Ends */}
          </PageContainer>
          {/* Create Dialog */}
          <FormModal
            title="Add Coupon"
            actionBtnTitle="Add"
            isOpen={isCreateModalOpen}
            handleDoAction={handleCreateDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm dataObj={dataObj} setDataObj={setDataObj} />
          </FormModal>
          {/* Edit Dialog */}
          <FormModal
            title={`Edit Coupon`}
            actionBtnTitle="Edit"
            isOpen={isEditModalOpen}
            handleDoAction={handleEditDataObj}
            handleClose={handleCloseModal}
          >
            <DataForm variant="edit" dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
          </FormModal>
          {/* Coupon Details */}
          {selectedDataObj && (
            <DataDialog
              maxWidth="md"
              isOpen={isCouponDetailsOpen}
              title={`Coupon Details`}
              handleClose={() => setIsCouponDetailsOpen(false)}
            >
              <CouponDetails selectedDataObj={selectedDataObj} />
            </DataDialog>
          )}
        </PermissionsGate>
      </DashboardLayout>
      <LoadingScreen
        loading={editMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={createMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Coupons;
