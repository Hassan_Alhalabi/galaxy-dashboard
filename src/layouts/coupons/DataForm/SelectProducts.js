import { useQuery } from "react-query";
import { useState, useEffect } from "react";
import { getProducts } from "../service";

//MD
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

//material ui
import {
  CircularProgress,
  FormControl,
  Select,
  InputLabel,
  OutlinedInput,
  Chip,
  MenuItem,
} from "@mui/material";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 250,
    },
  },
};

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectMultipleProducts = ({ dataObj, setDataObj, variant }) => {
  //create selected coupon product to make the objects properties in the array uniform
  const [selectedCouponProduct, setSelectedCouponProducts] = useState([]);
  //create selected coupon product ids(primative type: string) so the Select Component use it instead of objects which is reference type
  const selectedCouponProductsIds = selectedCouponProduct.map(
    (couponProduct) => couponProduct.productId
  );

  const { data: productsRes, status } = useQuery("product-products", getProducts, {
    onSuccess: () => {
      //restore old data for edit
      if (variant === "edit") {
        setSelectedCouponProducts(
          dataObj.couponProducts.map((couponProduct) => ({
            productId: couponProduct.product.productId,
            title: couponProduct.product.title,
          }))
        );
      }
    },
  });

  //sync selected coupon products with dataObj.couponProducts
  useEffect(() => {
    setSelectedCouponProducts(
      dataObj.couponProducts.map((product) => ({
        productId: product.productId,
        title: product.title,
      }))
    );
  }, [dataObj.couponProducts]);

  function handleSelectChange(e) {
    const newSelectedProductProductsIds = e.target.value;
    const newSelectedProductProducts = newSelectedProductProductsIds.map((productId) => {
      return productsRes.products.find((product) => product.productId === productId);
    });
    setDataObj((oldForm) => ({
      ...oldForm,
      couponProducts: newSelectedProductProducts.map((selectedProductProduct) => ({
        productId: selectedProductProduct.productId,
        title: selectedProductProduct.title,
      })),
    }));
  }

  return (
    <FormControl fullWidth>
      <InputLabel id="select-products">Coupon Products</InputLabel>
      <Select
        autoWidth
        labelId="select-products"
        id="select-products"
        multiple
        value={selectedCouponProductsIds}
        onChange={(e) => handleSelectChange(e)}
        input={<OutlinedInput id="select-multiple-products" label="Coupon Products" />}
        sx={{ minHeight: 40, background: "#f2f9fc !important" }}
        renderValue={() => (
          <MDBox
            sx={{
              display: "flex",
              flexWrap: "wrap",
              gap: 0.5,
              my: 2,
            }}
          >
            {selectedCouponProduct.map((couponProduct, index) => (
              <Chip
                color="primary"
                size="small"
                sx={{ padding: 2 }}
                key={index}
                label={
                  <MDTypography color="light" sx={{ whiteSpace: "nowrap", fontSize: 13 }}>
                    {couponProduct.title}
                  </MDTypography>
                }
              />
            ))}
          </MDBox>
        )}
        MenuProps={MenuProps}
      >
        {status === "success" &&
          productsRes.products.map((product) => (
            <MenuItem
              key={product.productId}
              value={product.productId}
              sx={{
                my: 1,
                "&:focus": {
                  background: "transparent",
                },
              }}
            >
              {product.title}
            </MenuItem>
          ))}
        {status === "error" && (
          <MDBox
            display="flex"
            alignItems="center"
            height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
          >
            <MDTypography px={2}>There is no data to dispaly</MDTypography>
          </MDBox>
        )}
        {status === "loading" && <Loader />}
      </Select>
    </FormControl>
  );
};

export default SelectMultipleProducts;
