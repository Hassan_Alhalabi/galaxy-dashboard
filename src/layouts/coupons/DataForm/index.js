import camelPad from "../util/camelPad";

import SelectMultipleProducts from "./SelectProducts";

import { Checkbox, Grid, MenuItem, FormControlLabel } from "@mui/material";

import MDBox from "components/MDBox";
import MDInput from "components/MDInput";
import MDTypography from "components/MDTypography";

const DataObjForm = ({ dataObj, setDataObj, variant }) => {
  return (
    <>
      <Grid
        px={1}
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.couponCode}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                couponCode: e.target.value,
              }))
            }
            margin="dense"
            id="code"
            label="Code"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.quantity}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                quantity: Number(e.target.value),
              }))
            }
            margin="dense"
            id="quantity"
            label="Quantity"
            type="number"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
      <MDBox
        px={1}
        mt={1}
        display="flex"
        flexWrap="wrap"
        alignItems="flex-end"
        sx={{ columnGap: 2 }}
      >
        <MDInput
          value={dataObj.discount}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              discount: Number(e.target.value),
            }))
          }
          margin="dense"
          id="discount"
          label="Discount"
          type="number"
          variant="standard"
        />
        <MDInput
          value={dataObj.isDiscountPercentage ? "%" : "AED"}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              isDiscountPercentage: e.target.value === "%" ? true : false,
            }))
          }
          margin="dense"
          id="is-percentage"
          label="unit"
          type="text"
          variant="standard"
          select
        >
          <MenuItem value="%">%</MenuItem>
          <MenuItem value="AED">AED</MenuItem>
        </MDInput>
        <MDInput
          value={dataObj.isDiscountAfterVat ? "After VAT" : "Without VAT"}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              isDiscountAfterVat: e.target.value === "After VAT" ? true : false,
            }))
          }
          margin="dense"
          id="discount-type"
          label="Discount Type"
          type="text"
          variant="standard"
          select
        >
          <MenuItem value="After VAT">After VAT</MenuItem>
          <MenuItem value="Without VAT">Without VAT</MenuItem>
        </MDInput>
      </MDBox>
      <Grid
        px={1}
        mt={1}
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.startDate}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                startDate: e.target.value,
              }))
            }
            margin="dense"
            id="startDate"
            label=""
            helperText="Start Date (In UAE time)"
            type="datetime-local"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.endDate}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                endDate: e.target.value,
              }))
            }
            margin="dense"
            id="endDate"
            label=""
            helperText="End Date (In UAE time)"
            type="datetime-local"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
      <MDBox px={1} mt={1}>
        <MDInput
          value={dataObj.couponType}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              couponType: e.target.value,
            }))
          }
          margin="dense"
          id="coupon-type"
          label="Coupon Type"
          type="text"
          variant="standard"
          select
          fullWidth
        >
          <MenuItem value="CouponAppliesForCart">{camelPad("CouponAppliesForCart")}</MenuItem>
          <MenuItem value="CouponAppliesForCartExceptDiscountedItem">
            {camelPad("CouponAppliesForCartExceptDiscountedItem")}
          </MenuItem>
          <MenuItem value="CouponNotAppliedOnCartIfOneItemHasDiscount">
            {camelPad("CouponNotAppliedOnCartIfOneItemHasDiscount")}
          </MenuItem>
        </MDInput>
      </MDBox>
      <MDBox px={1} mt={1.5} display="flex" justifyContent="space-between" alignItems="center">
        <FormControlLabel
          control={
            <Checkbox
              checked={dataObj.allowManyTimeForSameUser}
              onChange={(e) =>
                setDataObj((oldForm) => ({
                  ...oldForm,
                  allowManyTimeForSameUser: e.target.checked,
                }))
              }
            />
          }
          label={
            <MDTypography variant="subtitle2">Allowed many time for the same user</MDTypography>
          }
        />
      </MDBox>
      <MDBox px={1} mt={2}>
        <SelectMultipleProducts variant={variant} dataObj={dataObj} setDataObj={setDataObj} />
      </MDBox>
      <Grid
        mt={1}
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        px={1}
      >
        <Grid item xs={12} display="flex" justifyContent="space-between" alignItems="flex-end">
          <Checkbox
            checked={typeof dataObj.minimumSpend === "number"}
            onChange={(e) =>
              setDataObj((oldForm) => ({ ...oldForm, minimumSpend: e.target.checked ? 0 : null }))
            }
          />
          <MDInput
            value={typeof dataObj.minimumSpend === "number" ? dataObj.minimumSpend : ""}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                minimumSpend: Number(e.target.value),
              }))
            }
            margin="dense"
            id="minimum-spend"
            label="Minumum items quantity allowed"
            type="number"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} display="flex" alignItems="flex-end">
          <Checkbox
            checked={typeof dataObj.maximumSpend === "number"}
            onChange={(e) =>
              setDataObj((oldForm) => ({ ...oldForm, maximumSpend: e.target.checked ? 0 : null }))
            }
          />
          <MDInput
            value={typeof dataObj.maximumSpend === "number" ? dataObj.maximumSpend : ""}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                maximumSpend: Number(e.target.value),
              }))
            }
            margin="dense"
            id="maximum-spend"
            label="Maximum items quantity allowed"
            type="number"
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
    </>
  );
};

export default DataObjForm;
