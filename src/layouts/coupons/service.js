import http from "util/httpService";
import moment from "moment";

const apiUrl = "/Coupon";

export function getCoupons(page, pageSize) {
  return http.get(`${apiUrl}?page=${page}&pageSize=${pageSize}`);
}

export function getCoupon(couponId) {
  return http.get(`${apiUrl}/${couponId}`);
}

export function createCoupon(coupon) {
  console.log(coupon);
  return http.post(apiUrl, coupon);
}

export function editCoupon(coupon) {
  console.log(coupon);
  return http.put(apiUrl, coupon);
}

export function getProducts() {
  return http.get(`Product/all?page=1&pageSize=999999999`);
}
