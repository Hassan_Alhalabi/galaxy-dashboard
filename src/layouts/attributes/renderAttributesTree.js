import PermissionsGate from "auth/PermissionGate";

import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

import { IconButton } from "@mui/material";
import TreeItem from "@mui/lab/TreeItem";

export const renderTree = ({
  attribute,
  // handleDeleteModalOpen,
  handleEditModalOpen,
}) => (
  <TreeItem
    key={attribute.attributeId}
    nodeId={attribute.attributeId}
    label={
      <MDBox display="flex" justifyContent="space-between" alignItems="center" flexWrap="wrap">
        <MDBox display="flex" gap={1} flexWrap={2} alignItems="center">
          <MDTypography>{attribute.title}</MDTypography>
        </MDBox>
        <MDBox display="flex" alignItems="center" gap={0.5} flexWrap="wrap">
          <PermissionsGate requiredScopes={[]}>
            <IconButton size="small" component="a" onClick={() => handleEditModalOpen(attribute)}>
              <span style={{ color: "#51accc" }} className="lnr lnr-pencil"></span>
            </IconButton>
          </PermissionsGate>
          {/* <PermissionsGate requiredScopes={[]}>
            <IconButton
              size="small"
              color="primary"
              component="a"
              onClick={() => handleDeleteModalOpen(attribute)}
            >
              <i className="far fa-trash-alt theme-color"></i>
            </IconButton>
          </PermissionsGate> */}
        </MDBox>
      </MDBox>
    }
  >
    {Array.isArray(attribute.subAttributes)
      ? attribute.subAttributes.map((attribute) =>
          renderTree({ attribute, /* handleDeleteModalOpen, */ handleEditModalOpen })
        )
      : null}
  </TreeItem>
);
