import SelectSuperAttribute from "./SelectSuperAttribute";

import MDBox from "components/MDBox";
import MDInput from "components/MDInput";
import MDTypography from "components/MDTypography/index";

//material ui
import { Grid, MenuItem } from "@mui/material";

/* ls is new breakpoint (light small) */

const DataObjForm = ({ dataObj, setDataObj, variant }) => {
  return (
    <MDBox mx={1}>
      <Grid
        container
        columnSpacing={4}
        rowSpacing={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item ls={6} xs={12}>
          <MDInput
            autoFocus
            value={dataObj.title}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                title: e.target.value,
              }))
            }
            margin="dense"
            id="title-en"
            label="Title (EN)"
            type="text"
            fullWidth
            variant="standard"
          />
        </Grid>
        <Grid item ls={6} xs={12}>
          <MDInput
            value={dataObj.titleAr}
            onChange={(e) =>
              setDataObj((oldForms) => ({
                ...oldForms,
                titleAr: e.target.value,
              }))
            }
            margin="dense"
            id="title-ar"
            label="Title (AR)"
            type="text"
            fullWidth
            variant="standard"
            sx={{ direction: "rtl" }}
          />
        </Grid>
      </Grid>
      <Grid mb={2} mt={4}>
        <MDInput
          value={dataObj.attributeType}
          onChange={(e) =>
            setDataObj((oldForms) => ({
              ...oldForms,
              attributeType: e.target.value,
            }))
          }
          helperText="Select how do you want the buyer to select this attribute"
          margin="dense"
          id="attribteType"
          label="Attribute Type"
          type="text"
          variant="standard"
          fullWidth
          select
        >
          <MenuItem value="Color">Color</MenuItem>
          <MenuItem value="Label">Label</MenuItem>
          <MenuItem value="Select">Select</MenuItem>
        </MDInput>
      </Grid>
      <Grid mb={2} mt={4}>
        <SelectSuperAttribute variant={variant} dataObj={dataObj} setDataObj={setDataObj} />
      </Grid>
    </MDBox>
  );
};

export default DataObjForm;
