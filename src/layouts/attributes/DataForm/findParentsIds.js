export default function findParentIds({ list, targetChild, setParentsIdsState }) {
  for (let parent of list) {
    parentIdsRoller({ node: parent, targetChild, setParentsIdsState, grandParent: parent });
  }
}

function parentIdsRoller({
  node,
  parentIdsList = [],
  targetChild,
  setParentsIdsState,
  grandParent,
}) {
  if (node.attributeId === targetChild) {
    // "in success find it"
    setParentsIdsState([grandParent.attributeId, ...parentIdsList]);
  } else if (node.subAttributes.length > 0) {
    // "in there is subAttributes and could't find the child"
    node.subAttributes.map((attribute) => {
      parentIdsRoller({
        node: attribute,
        parentIdsList: [...parentIdsList, attribute.attributeId],
        targetChild,
        setParentsIdsState,
        grandParent,
      });
    });
  } else {
    // "in there is not subAttributes and could't find the child"
    return null;
  }
}
