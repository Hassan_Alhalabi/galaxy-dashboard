import { useState, useEffect } from "react";
import { getAllAttributes } from "../service";
import { toast } from "react-toastify";
import { useQuery } from "react-query";
import findParentIds from "./findParentsIds";

import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import MDBox from "components/MDBox";

import { CircularProgress, Checkbox } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeView from "@mui/lab/TreeView";
import TreeItem from "@mui/lab/TreeItem";

const SelectSuperAttribute = ({ dataObj, setDataObj, variant }) => {
  const [parentsIdsState, setParentsIdsState] = useState(null);

  /* Get all attributes */
  const { data: attributesRes, status: attributesStatus } = useQuery(
    ["allAttributes"],
    getAllAttributes,
    {
      staleTime: 90 * 1000,
      onError: (error) => {
        if (error.response.data?.length !== 0)
          error.isExpectedError
            ? toast.error(error.response.data[0])
            : toast.error("An Unexpected Error Occurred!");
      },
    }
  );

  useEffect(() => {
    if (variant === "edit" && attributesStatus === "success")
      findParentIds({
        list: attributesRes.attributes,
        targetChild: dataObj.attributeId,
        setParentsIdsState,
      });
  }, [attributesStatus === "success"]);

  return (
    <>
      {attributesStatus === "success" && attributesRes.attributes[0] && (
        <>
          <MDBox display="flex" flexWrap="wrap" gap={2}>
            <MDTypography
              sx={{ textAlign: "center", color: "#777", maxWidth: 400, textAlign: "start" }}
            >
              Is this attribute a subattribute?
            </MDTypography>
            <MDBox>
              <MDButton
                color="productButton"
                sx={{ color: "#444 !important" }}
                variant={isSub(dataObj.superAttributeId) ? "contained" : "outlined"}
                onClick={() =>
                  setDataObj((old) => ({
                    ...old,
                    superAttributeId: attributesRes.attributes[0].attributeId,
                  }))
                }
              >
                Yes
              </MDButton>
              <MDButton
                color="productButton"
                display="inline-block"
                sx={{ ml: 2, color: "#444 !important" }}
                variant={!isSub(dataObj.superAttributeId) ? "contained" : "outlined"}
                onClick={() =>
                  setDataObj((old) => ({
                    ...old,
                    superAttributeId: "00000000-0000-0000-0000-000000000000",
                  }))
                }
              >
                No
              </MDButton>
            </MDBox>
          </MDBox>
          {isSub(dataObj.superAttributeId) &&
            (variant === "edit" ? Array.isArray(parentsIdsState) : true) && (
              <TreeView
                aria-label="rich object"
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpanded={parentsIdsState ? parentsIdsState : ["root"]}
                defaultExpandIcon={<ChevronRightIcon />}
                sx={{ flexGrow: 1, overflowY: "auto", mt: 3 }}
                multiSelect
                onNodeSelect={(e, nodeIds) =>
                  setDataObj((oldForm) => ({ ...oldForm, superAttributeId: nodeIds[0] }))
                }
              >
                {attributesRes.attributes.map((attribute) => renderTree(attribute, dataObj))}
              </TreeView>
            )}
        </>
      )}
      {attributesStatus === "error" && (
        <MDTypography
          sx={{ textAlign: "center", color: "#888", maxWidth: 400, textAlign: "start" }}
        >
          You can't choose the super attribute if need it because of an error
        </MDTypography>
      )}
      {attributesStatus === "success" && !attributesRes.attributes[0] && (
        <MDTypography
          variant="subtitle2"
          sx={{ textAlign: "center", color: "#888", maxWidth: 400, textAlign: "start" }}
        >
          You must add one attribute at least to make this attribute a sub attribute.
        </MDTypography>
      )}
      {attributesStatus === "loading" && <CircularProgress sx={{ margin: "0 auto" }} />}
    </>
  );
};

export default SelectSuperAttribute;

function renderTree(nodes, dataObj) {
  return (
    <TreeItem
      key={nodes.attributeId}
      nodeId={nodes.attributeId}
      //user can't select itself or from it's children as a super attribute
      disabled={dataObj.attributeId === nodes.attributeId}
      label={
        <MDBox display="flex" alignItems="center">
          <Checkbox checked={nodes.attributeId === dataObj.superAttributeId} />
          <MDTypography>{nodes.title}</MDTypography>
          {nodes.isBest && <Chip size="small" label="Best" color="primary" />}
        </MDBox>
      }
    >
      {Array.isArray(nodes.subAttributes)
        ? nodes.subAttributes.map((node) => renderTree(node, dataObj))
        : null}
    </TreeItem>
  );
}

function isSub(id) {
  if (id === "00000000-0000-0000-0000-000000000000") return false;
  else return true;
}
