import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getAttributes as getDataReq,
  createAttribute as createDataObj,
  // deleteAttribute as deleteDataObj,
  editAttribute as editDataObj,
} from "./service";
import { renderTree } from "./renderAttributesTree";
import { object, string } from "yup";

import LoadingScreen from "react-loading-screen";

//MD comonents
import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox/index";

//My components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import AlertDialog from "components/AlertDialog";
import FormModal from "components/FormModal";
import DataForm from "./DataForm";
import Pagination from "components/MyPagination";
import PageContainer from "components/PageContainer";
import PermissionsGate from "auth/PermissionGate";

//mui components
import { IconButton, Icon } from "@mui/material";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

const Attributes = () => {
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 30;

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
    isPreviousData,
  } = useQuery(["attributes", page], () => getDataReq(page, pageSize), {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    retry: false,
    keepPreviousData: true,
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    // setIsDeleteModalOpen(false);
    setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  const attributeSchemaCreate = object({
    title: string().required("You must add the title in english language"),
    titleAr: string().required("You must add the title in arabic language"),
  });

  const attributeSchemaEdit = object({
    title: string().required("You must add the title in english language"),
    titleAr: string().required("You must add the title in arabic language"),
    superAttributeId: string().test(
      "is-superAttribute-same-of-the-attribute",
      "You can't choose super attribute same as the current attribute",
      (superAttributeId) => superAttributeId !== selectedDataObj.attributeId
    ),
  });

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    title: "",
    titleAr: "",
    attributeType: "Color",
    superAttributeId: "00000000-0000-0000-0000-000000000000",
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      setIsCreateModalOpen(false);
      toast.success("Attribute added successfully.");
      queryClient.invalidateQueries("attributes");
      queryClient.invalidateQueries("allAttributes");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });

  const handleCreateDataObj = async () => {
    attributeSchemaCreate
      .validate(dataObj)
      .then(() => {
        setIsCreateModalOpen(false);
        createMutation.mutate(dataObj);
      })
      .catch((err) => {
        if (err.errors)
          err.errors[0] ? toast.error(err.errors[0]) : toast.error("An Unexpected Error Occurred");
      });
  };

  /* Delete Query */
  // const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  // function handleDeleteModalOpen(dataObj) {
  //   setSelectedDataObj(dataObj);
  //   setIsDeleteModalOpen(true);
  // }
  // const deleteMutation = useMutation(deleteDataObj, {
  //   onSuccess: () => {
  //     toast.success("Attribute deleted successfully.");
  //     queryClient.invalidateQueries("attributes");
  //   },
  //   onError: (error) => {
  //     error.isExpectedError ? toast.error(error.response.data[0]) : null;
  //   },
  // });
  // function handleDeleteDataObj() {
  //   setIsDeleteModalOpen(false);
  //   deleteMutation.mutate(selectedDataObj.attributeId);
  // }

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      setIsEditModalOpen(false);
      toast.success("Attribute edited successfully");
      queryClient.invalidateQueries("attributes");
      queryClient.invalidateQueries("allAttributes");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  async function handleEditDataObj() {
    await attributeSchemaEdit
      .validate(selectedDataObj)
      .then(() => {
        setIsEditModalOpen(false);
        editMutation.mutate(selectedDataObj);
      })
      .catch((err) => {
        if (err.errors)
          err.errors[0] ? toast.error(err.errors[0]) : toast.error("An Unexpected Error Occurred");
      });
  }

  return (
    <>
      <DashboardLayout>
        <DashboardNavbar />
        <PageContainer
          title={
            <MDBox display="flex" alignItems="center" gap={1}>
              <MDTypography variant="h3">Attributes List</MDTypography>
              <PermissionsGate requiredScopes={[]}>
                <IconButton onClick={() => setIsCreateModalOpen(true)}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </PermissionsGate>
            </MDBox>
          }
        >
          <div className="card">
            <div className="card-body">
              {isPageSuccess && (
                <TreeView
                  aria-label="attributes"
                  defaultCollapseIcon={<ExpandMoreIcon />}
                  defaultExpanded={["root"]}
                  defaultExpandIcon={<ChevronRightIcon />}
                  sx={{ flexGrow: 1, overflowY: "auto" }}
                >
                  {dataReq.attributes.map((attribute) =>
                    renderTree({
                      attribute,
                      handleEditModalOpen,
                      // handleDeleteModalOpen,
                    })
                  )}
                </TreeView>
              )}
              {isPageError && (
                <MDTypography
                  sx={{ mx: 2, my: 2, pb: 2, whiteSpace: "nowrap", textAlign: "center" }}
                >
                  There is no data to display
                </MDTypography>
              )}
            </div>
            <div className="pagination-box">
              <nav className="ms-auto me-auto" aria-label="...">
                {isPageSuccess && (
                  <Pagination
                    setPage={setPage}
                    currentPage={dataReq.paginationInfo.pageNo}
                    totalPages={dataReq.paginationInfo.totalPages}
                  />
                )}
              </nav>
            </div>
          </div>
          {/* Container-fluid Ends */}
        </PageContainer>
        {/* Create Dialog */}
        <FormModal
          title="Add Attribute"
          actionBtnTitle="Add"
          isOpen={isCreateModalOpen}
          handleDoAction={handleCreateDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm dataObj={dataObj} setDataObj={setDataObj} />
        </FormModal>
        {/* Delete Dialog */}
        {/* <AlertDialog
          isOpen={isDeleteModalOpen}
          handleClose={handleCloseModal}
          handleAction={handleDeleteDataObj}
          title="Delete!"
          desc={`Are You Sure You Want To Delete ${selectedDataObj?.title}?`}
        /> */}
        {/* Edit Dialog */}
        <FormModal
          title={`Edit Attribute`}
          actionBtnTitle="Edit"
          isOpen={isEditModalOpen}
          handleDoAction={handleEditDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm variant="edit" dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
        </FormModal>
      </DashboardLayout>
      <LoadingScreen
        loading={isPreviousData}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={isPageLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={createMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
      <LoadingScreen
        loading={editMutation.isLoading}
        bgColor="#FFFFFFAA"
        spinnerColor="#63449b"
        textColor="#676767"
        children=""
      />
    </>
  );
};

export default Attributes;
