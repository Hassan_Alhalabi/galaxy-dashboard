import http from "util/httpService";

const apiUrl = "/Attribute";

export function getAttributes(page, pageSize) {
  return http.get(`${apiUrl}?page=${page}&pageSize=${pageSize}`);
}

export function getAllAttributes() {
  return http.get(`${apiUrl}?page=1&pageSize=42874`);
}

export function getAttribute(attributeId) {
  return http.get(`${apiUrl}/${attributeId}`);
}

export function createAttribute(attribute) {
  console.log(attribute);
  return http.post(apiUrl, attribute);
}

// export function deleteAttribute(attributeId) {
//   return http.delete(`${apiUrl}/${attributeId}`);
// }

export function editAttribute(attribute) {
  console.log(attribute);
  return http.put(apiUrl, attribute);
}
