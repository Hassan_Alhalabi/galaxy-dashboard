import http from "util/httpService";

const apiUrl = "/Product";

export function getProducts(page, pageSize) {
  return http.get(`${apiUrl}/all?page=${page}&pageSize=${pageSize}`);
}

export function getProduct(productId) {
  return http.get(`${apiUrl}/${productId}`);
}

export function createProduct(product) {
  //if sku value length is greater than 0 return it or if it's equal to 0 or null return null
  product.sku = product.sku?.length > 0 ? product.sku : null;
  console.log(product);
  return http.post(apiUrl, product);
}

export function deleteProduct(productId) {
  return http.delete(`${apiUrl}/${productId}`);
}

export function editProduct(product) {
  //if sku value length is greater than 0 return it or if it's equal to 0 or null return null
  product.sku = product.sku?.length > 0 ? product.sku : null;
  return http.put(apiUrl, product);
}

export function getCategories() {
  return http.get(`Category?page=1&pageSize=999999999`);
}

export function getTags() {
  return http.get(`Tag?page=1&pageSize=999999999`);
}

export function getAttributes() {
  return http.get(`Attribute?page=1&pageSize=999999999`);
}

export function getAttributesPossibilities(ids) {
  return http.post(`Product/GetPossibleAttributes`,ids);
}
