import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createProduct as createDataObj } from "./service";
import { useNavigate } from "react-router-dom";
import FormData from "form-data";
import axios from "axios";
import { toast } from "react-toastify";
import { object, string, number, boolean, array } from "yup";
import LoadingScreen from "react-loading-screen";
import PageContainer from "components/PageContainer";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import ProductInfoData from "./DataForms/ProductInfoData";
import ProductDescriptionData from "./DataForms/ProductDescriptionData";
import ProductInventoryData from "./DataForms/ProductInventoryData";
import ProductImagesData from "./DataForms/ProductImagesData";
import ProductAttributesData from "./DataForms/ProductAttributesData";

import { IconButton, Icon } from "@mui/material";

export default function CreateProduct() {
  const [isCreatingProduct, setIsCreatingProduct] = useState(false);
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  /* Create Query */
  const productSchema = object({
    title: string().required(),
    titleAr: string().required(),
    price: number().required().positive(),
    salePrice: number().positive(),
    isDownloadable: boolean(),
    productCategories: array().min(1, "You must add one category at least for this product."),
    // mainImageData: string().required("You must add the main image of the product."),
    availableQuantity: number()
      .min(1, "Available quantity must be greater or equal to one")
      .max(2147483647, "available quantity must be less than or equal to 2147483647"),
    minQuantity: number()
      .min(1, "Minimum quantity must be greater or equal to one")
      .max(2147483647, "Minimum quantity must be less than or equal to 2147483647"),
  });

  const [dataObj, setDataObj] = useState({
    //--product info
    title: "",
    titleAr: "",
    price: 0,
    salePrice: 0,
    isDownloadable: true,
    productCategories: [],
    productTags: [], //nullable

    //--product description
    description: "",
    descriptionAr: "",
    shortDescription: "",
    shortDescriptionAr: "",

    mainImageId: "",
    productImagesList: [], //nullabel
    mainImageData: null,
    imageListData: [],

    //--product inventory
    availableQuantity: 0,
    minQuantity: 0,
    manageStockWithStatus: true,
    stockStatus: "InStock",
    sku: "",

    //--prduct variety
    productAttributes: [], //nullable
  });

  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      toast.success("Product added successfully");
      queryClient.invalidateQueries("products");
      navigate("/products");
    },
    onError: (error) => {
      setIsCreatingProduct(false);
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });

  const handleCreateDataObj = async () => {
    //Input Validation
    await productSchema
      .validate(dataObj)
      .then((_) => {
        const isMainImageExist = dataObj.mainImageData ? true : false;

        setIsCreatingProduct(true);

        // sending post request for each image
        const imagesPromises = [];

        if (isMainImageExist) {
          let data = new FormData();
          data.append("file", dataObj.mainImageData);
          const mainImagePromise = axios.post("Attachment/upload", data, {
            headers: {
              accept: "application/json",
              "Accept-Language": "en-US,en;q=0.8",
              "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
            },
          });
          imagesPromises.push(mainImagePromise);
        }
        // push promises of the images list request
        if (dataObj.imageListData.length > 0) {
          for (let imageData of dataObj.imageListData) {
            let data = new FormData();
            data.append("file", imageData);
            imagesPromises.push(
              axios.post("Attachment/upload", data, {
                headers: {
                  accept: "application/json",
                  "Accept-Language": "en-US,en;q=0.8",
                  "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
                },
              })
            );
          }
        }
        //sending request for all images
        Promise.all(imagesPromises)
          .then((dataRes) => {
            console.log("in images success");
            setDataObj((oldForm) => ({
              ...oldForm,
              mainImageId: isMainImageExist ? dataRes[0]?.attachmentId : "",
              productImagesList: isMainImageExist ? dataRes.slice(1) : dataRes,
            }));
            createMutation.mutate({
              ...dataObj,
              mainImageId: isMainImageExist ? dataRes[0]?.attachmentId : "",
              productImagesList: isMainImageExist ? dataRes.slice(1) : dataRes,
            });
          })
          .catch((error) => {
            console.log("in images error");
            setIsCreatingProduct(false);
            error.isExpectedError ? toast.error(error.response.data[0]) : null;
          });
      })
      .catch((err) => {
        console.log("in validation error ");
        setIsCreatingProduct(false);
        if (err.errors) err.errors[0] ? toast.error(err.errors[0]) : null;
      });
  };

  return (
    <LoadingScreen
      loading={isCreatingProduct}
      bgColor="#FFFFFFAA"
      spinnerColor="#63449b"
      textColor="#676767"
    >
      <DashboardLayout>
        <DashboardNavbar />
        <PageContainer title="Create Product">
          <ProductInfoData dataObj={dataObj} setDataObj={setDataObj} />
          <ProductDescriptionData dataObj={dataObj} setDataObj={setDataObj} />
          {/* <ProductInventoryData dataObj={dataObj} setDataObj={setDataObj} /> */}
          <ProductAttributesData dataObj={dataObj} setDataObj={setDataObj} />
          <ProductImagesData dataObj={dataObj} setDataObj={setDataObj} />
        </PageContainer>
        <IconButton
          sx={{
            position: "fixed",
            bottom: 90,
            width: "fit-content",
            bgcolor: "#63449b",
            "&:hover": {
              bgcolor: "#63449b",
            },
          }}
          onClick={handleCreateDataObj}
        >
          <Icon sx={{ color: "#fff" }}>add</Icon>
        </IconButton>
      </DashboardLayout>
    </LoadingScreen>
  );
}
