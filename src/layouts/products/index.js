import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { getProducts as getDataReq, deleteProduct as deleteDataObj } from "./service";

import { Link, useNavigate } from "react-router-dom";
import LoadingScreen from "react-loading-screen";

//MD comonents
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox/index";

//my components
import AlertDialog from "components/AlertDialog";
import Pagination from "components/MyPagination";
import PermissionsGate from "auth/PermissionGate";
import PageContainer from "components/PageContainer";

//mui components
import { IconButton, Icon } from "@mui/material";

const Products = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const [page, setPage] = useState(1);
  const pageSize = 30;

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
    isPreviousData,
  } = useQuery(["products", page], () => getDataReq(page, pageSize), {
    staleTime: 15 * 1000,
    retry: false,
    keepPreviousData: true,
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    // setIsCreateModalOpen(false);
    setIsDeleteModalOpen(false);
    // setIsEditModalOpen(false);
  };
  const [selectedDataObj, setSelectedDataObj] = useState();

  /* Delete Query */
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  function handleDeleteModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsDeleteModalOpen(true);
  }
  const deleteMutation = useMutation(deleteDataObj, {
    onSuccess: () => {
      toast.success("Product deleted successfully");
      queryClient.invalidateQueries("products");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  function handleDeleteDataObj() {
    setIsDeleteModalOpen(false);
    deleteMutation.mutate(selectedDataObj.productId);
  }

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <PermissionsGate requiredScopes={["Permissions.Product.View"]}>
        <PageContainer
          title={
            <MDBox display="flex" alignItems="center" gap={1}>
              <MDTypography variant="h3">Products List</MDTypography>
              <PermissionsGate requiredScopes={["Permissions.Category.Insert"]}>
                <IconButton onClick={() => navigate("/products/add")}>
                  <MDBox
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ borderRadius: "100%", width: 40, height: 40, cursor: "pointer" }}
                    size="large"
                    bgColor="secondary"
                  >
                    <Icon sx={{ color: "#fff" }}>add</Icon>
                  </MDBox>
                </IconButton>
              </PermissionsGate>
            </MDBox>
          }
        >
          <div className="card">
            <div className="card-body">
              <div>
                <div className="table-responsive table-desi table-product">
                  <table className="table table-1d all-package">
                    <thead>
                      <tr>
                        <th>Product Image</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Published</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      {isPageError && (
                        <MDTypography sx={{ mx: 2, my: 2, pb: 2, whiteSpace: "nowrap" }}>
                          There is no data to display
                        </MDTypography>
                      )}
                      {isPageSuccess &&
                        dataReq.products.map((product) => (
                          <tr key={product.productId}>
                            <td>
                              <img
                                src={product.mainImage?.downloadUrl}
                                className="img-fluid"
                                alt=""
                              />
                            </td>
                            <td>
                              <MDTypography variant="subtitle2">{product.title}</MDTypography>
                            </td>
                            <td>
                              <MDTypography variant="subtitle2">{product.price}</MDTypography>
                            </td>
                            {product.isActive ? (
                              <td className="td-check">
                                <span className="lnr lnr-checkmark-circle"></span>
                              </td>
                            ) : (
                              <td className="td-cross">
                                <span className="lnr lnr-cross-circle"></span>
                              </td>
                            )}
                            <td>
                              <ul>
                                <li>
                                  <Link
                                    state={{ product }}
                                    to={`/products/product/${product.productId}`}
                                  >
                                    <a>
                                      <span className="lnr lnr-eye"></span>
                                    </a>
                                  </Link>
                                </li>
                                <li>
                                  <Link
                                    state={{ product }}
                                    to={`/products/edit/${product.productId}`}
                                  >
                                    <span className="lnr lnr-pencil"></span>
                                  </Link>
                                </li>
                                <PermissionsGate requiredScopes={["Permissions.Role.Edit"]}>
                                  <li>
                                    <a onClick={() => handleDeleteModalOpen(product)} className='btn p-0 btn-solid-danger'>
                                      <i className="far fa-trash-alt theme-color"></i>
                                    </a>
                                  </li>
                                </PermissionsGate>
                              </ul>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="pagination-box">
              <nav className="ms-auto me-auto" aria-label="...">
                {isPageSuccess && (
                  <Pagination
                    setPage={setPage}
                    currentPage={dataReq.paginationInfo.pageNo}
                    totalPages={dataReq.paginationInfo.totalPages}
                  />
                )}
              </nav>
            </div>
          </div>
        </PageContainer>
        {/* Container-fluid Ends */}
        <LoadingScreen
          loading={isPreviousData}
          bgColor="#FFFFFFAA"
          spinnerColor="#63449b"
          textColor="#676767"
          children=""
        />
        <LoadingScreen
          loading={isPageLoading}
          bgColor="#FFFFFFAA"
          spinnerColor="#63449b"
          textColor="#676767"
          children=""
        />
        {/* Delete Dialog */}
        <AlertDialog
          isOpen={isDeleteModalOpen}
          handleClose={handleCloseModal}
          handleAction={handleDeleteDataObj}
          title="Delete!"
          desc={`Are You Sure You Want To Delete ${selectedDataObj?.title}?`}
        />
      </PermissionsGate>
    </DashboardLayout>
  );
};

export default Products;
