import { useState } from "react"
import { useParams, useLocation } from "react-router-dom";

import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import PageCard from "components/PageCard";
import PageContainer from "components/PageContainer";

import MDTypography from 'components/MDTypography/index';
import MDBox from "components/MDBox/index"
import MDButton from "components/MDButton";

import { Rating, Tooltip, Chip } from "@mui/material";

export default function ProductDetails() {
    const param = useParams()
    const location = useLocation()

    const product = {
        productId: "dafcf92b-25be-4ff2-946c-08da28f4c099",
        title: "Sara  Product (●'◡'●)",
        description: "string adskljfsalkfjsakf kajsdfk lsjaf sdaf asdf asfsddfsdafsf sfdfsdfsdfse",
        shortDescription: "string safsadfsadfsdaweeeersfffs",
        "mainImage": {
            "attachmentId": "aacd422b-1f4f-4ea8-8ce9-08da3008cf7d",
            "fileName": "attachment_af4559ec-127a-4ebd-9ae5-78eeb802e748",
            "downloadUrl": "http://alshahbatech-003-site1.itempurl.com/api/Attachment/download/attachment_af4559ec-127a-4ebd-9ae5-78eeb802e748.png",
            "type": "image/png",
            "directory": "/media/2022/5/png",
            "size": 34031,
            "extension": ".png"
        },
        availableQuantity: 21,
        minQuantity: 1,
        price: 990,
        salePrice: 990,
        isActive: true,
        manageStockWithStatus: true, //you can buy
        stockStatus: "InStock", //is in stock or where
        sku: null, //bar code
        isDownloadable: true,
        isBest: true, //there is 10 product best
        /* isFavoriteForUser: false, */
        productEvaluation: {
            rate: 2,
            count: 0,
        },
        addedBy: {
            id: "fe7d2394-169d-4225-9a52-3cb81052f4e9",
            firstName: "Admin",
            lastName: "Admin",
            phoneNumber: null,
            email: "superadmin@domain.com",
        },
        productImagesList: [
            {
                productImageId: "0f3b934d-9205-47cd-ee69-08da3009a9ca",
                attachment: {
                    attachmentId: "aacd422b-1f4f-4ea8-8ce9-08da3008cf7d",
                    fileName: "attachment_af4559ec-127a-4ebd-9ae5-78eeb802e748",
                    downloadUrl:
                        "http://alshahbatech-003-site1.itempurl.com/api/Attachment/download/attachment_af4559ec-127a-4ebd-9ae5-78eeb802e748.png",
                    type: "image/png",
                    directory: "/media/2022/5/png",
                    size: 34031,
                    extension: ".png",
                },
            },
            {
                productImageId: "6dc2af53-f08a-4f5b-ee68-08da3009a9ca",
                attachment: {
                    attachmentId: "32a284ae-0373-4440-8ce8-08da3008cf7d",
                    fileName: "attachment_5c0a9d75-ae88-4a81-a58d-042f8a303d90",
                    downloadUrl:
                        "http://alshahbatech-003-site1.itempurl.com/api/Attachment/download/attachment_5c0a9d75-ae88-4a81-a58d-042f8a303d90.png",
                    type: "image/png",
                    directory: "/media/2022/5/png",
                    size: 43788,
                    extension: ".png",
                },
            },
        ],
        productCategories: [
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
            {
                productCategoryId: "b1fcad77-faf4-4d79-07bc-08da28f4c09b",
                category: {
                    categoryId: "c43913b3-97ce-4a07-9550-08da2829b4c3",
                    title: "cat 1",
                },
            },
        ],
        productTags: [
            {
                productTagId: "8583a746-5457-4b23-09e6-08da3009a9cc",
                tag: {
                    tagId: "6e05181e-f66b-4d01-c002-08da30091907",
                    title: "Tag 2",
                },
            },
            {
                productTagId: "157d8d54-8ce9-487d-09e5-08da3009a9cc",
                tag: {
                    tagId: "585dfd50-de55-4d96-c001-08da30091907",
                    title: "Tag 1",
                },
            },
            {
                productTagId: "8583a746-5457-4b23-09e6-08da3009a9cc",
                tag: {
                    tagId: "6e05181e-f66b-4d01-c002-08da30091907",
                    title: "Tag 2",
                },
            },
            {
                productTagId: "157d8d54-8ce9-487d-09e5-08da3009a9cc",
                tag: {
                    tagId: "585dfd50-de55-4d96-c001-08da30091907",
                    title: "Tag 1",
                },
            },
            {
                productTagId: "8583a746-5457-4b23-09e6-08da3009a9cc",
                tag: {
                    tagId: "6e05181e-f66b-4d01-c002-08da30091907",
                    title: "Tag 2",
                },
            },
            {
                productTagId: "157d8d54-8ce9-487d-09e5-08da3009a9cc",
                tag: {
                    tagId: "585dfd50-de55-4d96-c001-08da30091907",
                    title: "Tag 1",
                },
            },
            {
                productTagId: "8583a746-5457-4b23-09e6-08da3009a9cc",
                tag: {
                    tagId: "6e05181e-f66b-4d01-c002-08da30091907",
                    title: "Tag 2",
                },
            },
            {
                productTagId: "157d8d54-8ce9-487d-09e5-08da3009a9cc",
                tag: {
                    tagId: "585dfd50-de55-4d96-c001-08da30091907",
                    title: "Tag 1",
                },
            },
            {
                productTagId: "8583a746-5457-4b23-09e6-08da3009a9cc",
                tag: {
                    tagId: "6e05181e-f66b-4d01-c002-08da30091907",
                    title: "Tag 2",
                },
            },
            {
                productTagId: "157d8d54-8ce9-487d-09e5-08da3009a9cc",
                tag: {
                    tagId: "585dfd50-de55-4d96-c001-08da30091907",
                    title: "Tag 1",
                },
            },
        ],
        productAttributes: [ //the properties of a version variant="body2"(color, size, ...) of the product 
            {
                attributeId: "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                price: 0,
                salePrice: 0,
                availableQuantity: 2147483647,
            },
        ],
    };
    const [isShowMoreDescription, setIsShowMoreDescription] = useState(false)

    return (
        <DashboardLayout>
            <DashboardNavbar />
            <PageContainer>
                <PageCard>
                    <div class="col-md-8">
                        {/* first section */}
                        <MDBox display="flex" flexWrap="wrap" alignItems="center" columnGap={1}>
                            <div className="title-header" style={{ padding: 0, display: "inline-block" }}>
                                <h5>{product.title}</h5>
                            </div>
                            <MDBox display="inline-flex" alignItems="center">
                                <Tooltip sx={{ cursor: "pointer" }} title={product.productEvaluation.rate.toString()} placement="top">
                                    {/* add MDTypography in order to the tooltip to work*/}
                                    <MDTypography >
                                        <Rating sx={{ verticalAlign: "middle" }} name="read-only" value={2.5} readOnly arrow />
                                    </MDTypography>
                                </Tooltip>
                                <MDTypography variant="body2" color="text" ml={0.5} >{`(${product.productEvaluation.count})`}</MDTypography>
                            </MDBox>
                        </MDBox>
                        <MDBox display="flex" flexWrap="wrap" columnGap={2} rowGap={0.8} mt={2} >
                            {product.isBest && <Chip sx={{ fontWeight: 700 }} label="Best">
                            </Chip>}
                            <Chip sx={{ fontWeight: 700 }} label={product.isActive ? "Published" : "Unpublished"}></Chip>
                            <Chip sx={{ fontWeight: 700 }} label={`${product.availableQuantity} Items`}></Chip>

                        </MDBox>
                        {product.productCategories.length > 0 && <MDBox display="flex" flexWrap="wrap" columnGap={2} rowGap={0.8} mt={2} >
                            {product.productCategories.map((productCategorie) => (
                                <Chip label={productCategorie.category.title} />
                            ))}
                        </MDBox>}
                        {product.productTags.length > 0 && <MDBox display="flex" flexWrap="wrap" columnGap={2} mt={2} >
                            {product.productTags.map((productTage) => (
                                <MDTypography sx={{ color: "#DE1DAD" }}>#{productTage.tag.title}</MDTypography>
                            ))}
                        </MDBox>}
                        <MDBox mt={3}>
                            {!isShowMoreDescription && <>
                                <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }} >{product.shortDescription} </MDTypography><MDButton color="secondary" variant="text" sx={{ mt: 0.5, px: 1.5, py: 0, cursor: "pointer", lineHeight: 1.3, fontWeight: 400 }} onClick={() => setIsShowMoreDescription(true)}>Read more</MDButton>
                            </>}
                            {isShowMoreDescription && <>
                                <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }} >{product.description} </MDTypography><MDButton color="secondary" variant="text" sx={{ mt: 0.5, px: 1.5, py: 0, cursor: "pointer", lineHeight: 1.3, fontWeight: 400 }} onClick={() => setIsShowMoreDescription(false)}>Read less</MDButton>
                            </>}
                        </MDBox>
                        <MDBox display="flex" justifyContent="flex-start" mt={1} mb={3}>
                            <MDBox sx={{ width: "100%" }}>
                                <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3, fontWeight: "bold", }}>Original price: <span style={{ color: "#118c4f" }}>{`$${product.price}`}</span></MDTypography>
                            </MDBox>
                            <MDBox sx={{ width: "100%" }}>
                                <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3, textDecoration: "underline", fontWeight: "bold" }}>Sale price: <span style={{ textDecoration: "underline" }}>{`$${product.salePrice}`}</span></MDTypography>
                            </MDBox>
                        </MDBox>
                        <MDBox mt={2}>
                            <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }}>Product stock status: {product.stockStatus}</MDTypography>
                        </MDBox>
                        <MDBox mt={2}>
                            <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }}>Minimum quantity that can be in the stock: {product.minQuantity}</MDTypography>
                        </MDBox>
                        <MDBox mt={2}>
                            <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }}>{product.manageStockWithStatus ? "User can buy unlimited quantity of that product" : "User can't buy unlimited quantity of that product"}</MDTypography>
                        </MDBox>
                        <MDBox mt={2}>
                            <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }}>sku: {product.sku}</MDTypography>
                        </MDBox>
                    </div>
                    <div class="col-md-4">
                        <div class="order-success">
                            <div class="row g-4">
                                <MDBox mt={2}>
                                    <MDTypography variant="body6" color="text" sx={{ lineHeight: 1.3 }}>Product added by:</MDTypography>
                                    <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }} mt={2}>Name: {product.addedBy.firstName} {product.addedBy.lastName}</MDTypography>
                                    <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }} mt={2}>Email: {product.addedBy.email}</MDTypography>
                                    <MDTypography variant="body2" color="text" sx={{ lineHeight: 1.3 }} mt={2}>Phone Number: {product.addedBy.phoneNumber}</MDTypography>
                                </MDBox>
                            </div>
                        </div>
                    </div>
                </PageCard>
            </PageContainer>
        </DashboardLayout >
    );
}
