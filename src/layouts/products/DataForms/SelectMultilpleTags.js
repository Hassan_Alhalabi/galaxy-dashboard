import { useQuery } from "react-query";
import { useState, useEffect } from "react";
import { getTags } from "../service";

//MD
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

//material ui
import {
  CircularProgress,
  FormControl,
  Select,
  InputLabel,
  OutlinedInput,
  Chip,
  MenuItem,
} from "@mui/material";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 250,
    },
  },
};

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectMultipleTags = ({ dataObj, setDataObj, variant }) => {
  //create selected product tag to make the objects properties in the array uniform
  const [selectedPrductTags, setSelectedProductTags] = useState([]);
  //create selected product tag ids(primative type: string) so the Select Component use it instead of objects which is reference type
  const selectedPorudctTagsIds = selectedPrductTags.map((productTag) => productTag.tagId);

  const { data: tagsRes, status } = useQuery("product-tags", getTags, {
    onSuccess: () => {
      //restore old data for edit
      if (variant === "edit") {
        setSelectedProductTags(
          dataObj.productTags.map((productTag) => ({
            tagId: productTag.tagId,
            title: productTag.title,
          }))
        );
      }
    },
  });

  //sync selected product tags with dataObj.productTags
  useEffect(() => {
    setSelectedProductTags(
      dataObj.productTags.map((tag) => ({
        tagId: tag.tagId,
        title: tag.title,
      }))
    );
  }, [dataObj.productTags]);

  function handleSelectChange(e) {
    const newSelectedProductTagsIds = e.target.value;
    const newSelectedProductTags = newSelectedProductTagsIds.map((tagId) => {
      return tagsRes.tags.find((tag) => tag.tagId === tagId);
    });
    setDataObj((oldForm) => ({
      ...oldForm,
      productTags: newSelectedProductTags.map((selectedProductTag) => ({
        tagId: selectedProductTag.tagId,
        title: selectedProductTag.title,
      })),
    }));
  }

  return (
    <FormControl fullWidth>
      <InputLabel id="select-tags">Tags</InputLabel>
      <Select
        autoWidth
        labelId="select-tags"
        id="select-tags"
        multiple
        value={selectedPorudctTagsIds}
        onChange={(e) => handleSelectChange(e)}
        input={<OutlinedInput id="select-multiple-tags" label="Tags" />}
        sx={{ minHeight: 40, background: "#63449b0d !important" }}
        renderValue={() => (
          <MDBox
            sx={{
              display: "flex",
              flexWrap: "wrap",
              gap: 0.5,
              my: 2,
            }}
          >
            {selectedPrductTags.map((productTag) => (
              <Chip
                color="primary"
                size="small"
                sx={{ padding: 2 }}
                key={productTag.tagId}
                label={
                  <MDTypography color="light" sx={{ whiteSpace: "nowrap", fontSize: 13 }}>
                    {productTag.title}
                  </MDTypography>
                }
              />
            ))}
          </MDBox>
        )}
        MenuProps={MenuProps}
      >
        {status === "success" &&
          tagsRes.tags.map((tag) => (
            <MenuItem
              key={tag.tagId}
              value={tag.tagId}
              sx={{
                my: 1,
                "&:focus": {
                  background: "transparent",
                },
              }}
            >
              {tag.title}
            </MenuItem>
          ))}
        {status === "error" && (
          <MDTypography px={2} height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}>
            There is no data to dispaly
          </MDTypography>
        )}
        {status === "loading" && <Loader />}
      </Select>
    </FormControl>
  );
};

export default SelectMultipleTags;
