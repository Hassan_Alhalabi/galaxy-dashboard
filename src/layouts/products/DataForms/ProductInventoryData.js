import PageCard from "components/PageCard";
import DataInputContainer from "./DataInputContainer";

//material ui
import { InputBase, styled } from "@mui/material";

import MDButton from "components/MDButton";
import MDBox from "components/MDBox";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    border: "1px solid #ced4da",
    fontSize: 16,
    // width: "100%",
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      // borderColor: theme.palette.primary.main,
    },
  },
}));

const ProductInventoryData = ({ setDataObj, dataObj }) => {
  return (
    <PageCard title="Product Inventory">
      <form className="theme-form theme-form-2 mega-form">
        <div className="row">
          <DataInputContainer title="available quantity">
            <BootstrapInput
              value={dataObj.availableQuantity}
              placeholder="Available quantity (in the stock)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  availableQuantity: Number(e.target.value),
                }))
              }
              margin="dense"
              id="available-quantity"
              label="Available quantity"
              type="number"
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>
          <DataInputContainer title="minimum quantity">
            <BootstrapInput
              value={dataObj.minQuantity}
              placeholder="Minimum quantity (that can be in the stock)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  minQuantity: Number(e.target.value),
                }))
              }
              margin="dense"
              id="minimum-quantity"
              label="Minimum quantity"
              type="number"
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>
          <DataInputContainer
            variant="choose"
            title="Unlimited Quantity?"
          >
            <MDButton
              color="productButton"
              sx={{ color: "#444 !important" }}
              variant={dataObj.manageStockWithStatus ? "contained" : "outlined"}
              onClick={() =>
                setDataObj((old) => ({ ...old, manageStockWithStatus: !old.manageStockWithStatus }))
              }
            >
              Yes
            </MDButton>
            <MDButton
              color="productButton"
              display="inline-block"
              sx={{ ml: 2, color: "#444 !important" }}
              variant={!dataObj.manageStockWithStatus ? "contained" : "outlined"}
              onClick={() =>
                setDataObj((old) => ({ ...old, manageStockWithStatus: !old.manageStockWithStatus }))
              }
            >
              No
            </MDButton>
          </DataInputContainer>
          <DataInputContainer title="Stock status">
            <MDBox display="flex" flexWrap="wrap" gap={2}>
              <MDButton
                color="productButton"
                sx={{ color: "#444 !important" }}
                variant={dataObj.stockStatus === "InStock" ? "contained" : "outlined"}
                onClick={() => setDataObj((old) => ({ ...old, stockStatus: "InStock" }))}
              >
                In Stock
              </MDButton>
              <MDButton
                color="productButton"
                display="inline-block"
                sx={{ color: "#444 !important" }}
                variant={dataObj.stockStatus === "OutOfStock" ? "contained" : "outlined"}
                onClick={() => setDataObj((old) => ({ ...old, stockStatus: "OutOfStock" }))}
              >
                Out Of Stock
              </MDButton>
              <MDButton
                color="productButton"
                display="inline-block"
                sx={{ color: "#444 !important" }}
                variant={dataObj.stockStatus === "OnBackorder" ? "contained" : "outlined"}
                onClick={() => setDataObj((old) => ({ ...old, stockStatus: "OnBackorder" }))}
              >
                On Backorder
              </MDButton>
            </MDBox>
          </DataInputContainer>
          <DataInputContainer title="SKU">
            <BootstrapInput
              value={dataObj.sku || ""}
              placeholder="Stock keeping Uinit"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  sku: e.target.value,
                }))
              }
              margin="dense"
              id="sku"
              label="SKU"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>
        </div>
      </form>
    </PageCard>
  );
};

export default ProductInventoryData;
