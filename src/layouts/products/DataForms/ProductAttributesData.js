import PageCard from "components/PageCard/index";
import SelectMultipleAttributes from "./SelectMultipleAttributes";
import DataInputContainer from "./DataInputContainer";
import { InputBase, styled, IconButton } from "@mui/material";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import { useEffect, useState } from "react";
import AttributeConnectionForm from "./AttributeConnectionForm";
import {  useMutation, useQuery } from "react-query";
import { getAttributesPossibilities } from "../service";
import { getPossibleAttributes } from "util/attributes";
import { getConnections } from "util/attributes";
import { getAttributes } from "util/attributes";
import { createSubAttributes } from "util/attributes";
import { getAttributes as getAttributesReq } from "../service";
import { AccordionSummary, AccordionDetails, Switch } from "@mui/material";
import MuiAccordion from "@mui/material/Accordion";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    border: "1px solid #ced4da",
    fontSize: 16,
    // width: "100%",
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      // borderColor: theme.palette.primary.main,
    },
  },
}));

const Accordion = styled((props) => (
	<MuiAccordion disableGutters elevation={0} square {...props} />
  ))(({ theme }) => ({
	"&:before": {
	  display: "none",
	},
  }));

const ProductAttributesData = ({ setDataObj, dataObj, variant }) => {

	const [isDefaultSelect,setIsDefaultSelect] = useState(true);

	const [allAttributes,setAllAttributes] = useState([]);
	const [allSelectedAttributes,setAllSelectedAttributes] = useState([]);
	const [filteredAttributes,setFilteredAttributes] = useState([]);

	const [connectionsPossibilities,setConnectionsPossibilities] = useState([]);
	const [filteredConnections,setFilteredConnections] = useState([]);

	const [connections,setConnections] = useState([]);
	const [addedConnection,setAddedConnection] = useState({});
	const [addedConnections,setAddedConnections] = useState([]);

//   function handleDeleteSelectedAttribute(attributeId) {
//     setDataObj((oldForms) => ({
//       ...oldForms,
//       productAttributes: oldForms.productAttributes.filter(
//         (productAttribute) => productAttribute.attributeId !== attributeId
//       ),
//     }));
//   }

	const { data: attributesRes, status: allAttributesStatus} = useQuery("product-attributes", getAttributesReq, {

		onSuccess: () => {
			setAllAttributes(attributesRes);
			//restore old data for edit
			if (variant === "edit") {
				setSelectedProductAttributes(
				dataObj.productAttributes.map((productAttribute) => ({
					attributeId: productAttribute.attributeId,
					title: productAttribute.title,
				}))
				);
			}
		},

	});

  	const getPossibleAttributesMutation = useMutation((ids) => getAttributesPossibilities(ids))

	const handleGetPossibilites = (newSelectedAttributes) => {
		getPossibleAttributesMutation.mutate(newSelectedAttributes,{
			onSuccess: (res) => {
				console.log(res)
				const connections = getConnections(res);
				setConnectionsPossibilities(connections);
				setFilteredConnections(connections);
				setFilteredAttributes(getAttributes(connections));
			}
		});
	}

	function handleSelectChange(e) {

		if(event.target.value === '0') {
			return;
		}

		const newSelectedProductAttributesIds = e.target.value;
		const newSelectedProductAttributes = newSelectedProductAttributesIds.map((attributeId) => {
		  return attributesRes.attributes.find((attribute) => attribute.attributeId === attributeId);
		});

		setAllSelectedAttributes(newSelectedProductAttributes);
		handleGetPossibilites(newSelectedProductAttributesIds);

		setDataObj((oldForm) => ({
		  ...oldForm,
		  productAttributes: newSelectedProductAttributes.map((selectedProductAttribute) => ({
			attributeId: selectedProductAttribute.attributeId,
			title: selectedProductAttribute.title,
			price: 0,
			salePrice: 0,
			availableQuantity: 0,
		  })),
		}));

	}

	const filterConnections = (event) => {

		setIsDefaultSelect(false);

		const newConnection = filteredConnections.filter(con => {
			return(
				con.connAttr[event.target.name].attributeName === event.target.value
			)
		});
		
		setAddedConnection(newConnection);
		setFilteredConnections(newConnection);
		setFilteredAttributes(getAttributes(newConnection))

	}

  

 	 const handleAddConnection = () => {

		// 1- Filter Connection

		if(addedConnection.length !== 1) {
			console.log(addedConnection)
			console.log('error');
			return;
		}

		console.log(addedConnection[0]);

		const newConnections =  connectionsPossibilities.filter(con => con.attrId !== addedConnection[0].attrId);

		console.log(newConnections);

		console.log({connectionsPossibilities});

		console.log("New Connections Filtered After Adding: ",connectionsPossibilities.filter(con => con !== addedConnection[0]));

		setConnectionsPossibilities(newConnections);
		setFilteredConnections(newConnections);
		setFilteredAttributes(getAttributes(newConnections));

		// *************************

		setConnections(prev => {
			return [...prev,
				{
					price: 0,
					salePrice: null,
					manageStockWithStatus: true,
					stockStatus: 'InStock',
					availableQuantity: 0,
					minQuantity: 0,
					connAttrId: addedConnection[0].connAttrId,
					attributeConnection: createSubAttributes(
						Object.keys(addedConnection[0].connAttr).map(key => addedConnection[0].connAttr[key]),
						0
					)
				}
			]
		})

		setAddedConnections(prev => [...prev,{
			price: 0,
			salePrice: null,
			manageStockWithStatus: true,
			stockStatus: 'InStock',
			availableQuantity: 0,
			minQuantity: 0,
				attributeConnection: addedConnection[0]
			}]);
		setAddedConnection([]);
		setIsDefaultSelect(true);

  	}

	  console.log(connections)
	  console.log(addedConnections)

//   console.log('Filtered Attributes:',filteredAttributes);
//   console.log(connectionsPossibilities)

  	const handleClear = () => {
		setConnections([]);
		setAddedConnections([]);
		setFilteredConnections([]);
		setFilteredAttributes([]);
		handleGetPossibilites(allSelectedAttributes.map(attr => attr.attributeId));
		setIsDefaultSelect(true);
  	}

	const removeConnection = id => {

		const newAddedConnections = addedConnections.filter(con => con.attributeConnection.connAttrId !== id);
		const newConnections = connections.filter(con => con.connAttrId !== id);
		const removedConnection = addedConnections.filter(con => con.attributeConnection.connAttrId === id);

		console.log(removedConnection);

		// setConnectionsPossibilities(prev => [...prev,removedConnection]);
		setConnections(newConnections);
		setAddedConnections(newAddedConnections);
		setIsDefaultSelect(true);

	}

  return (
    <PageCard title="Product Attributes">
      <form className="theme-form theme-form-2 mega-form">
        <div className="row">

          <DataInputContainer title="Select Attributes">
            <SelectMultipleAttributes 
				allAttributes={allAttributes}
				allAttributesStatus={allAttributesStatus}
				variant={variant} 
				setDataObj={setDataObj} 
				dataObj={dataObj} 
				handleSelectChange={handleSelectChange}/>
          </DataInputContainer>

        </div>
      </form>

		<div className="me-3">
			{	allSelectedAttributes.length > 0 && connectionsPossibilities.length > 0 &&
				<div className="new-connection d-flex">
					{
						Array.from(filteredAttributes).map(([key,value]) => {
							return(
								<div key={key} className="d-flex flex-column mb-3">
									{key}
									<select defaultValue='0' className="form-control" name={key} onChange={(event) => filterConnections(event)}>
										<option value='0' selected={isDefaultSelect}>Select {key}</option>
									{
										
										Array.from(value.attrs).map(attr => {
											return(<option key={attr} value={attr}>{attr}</option>)
										})
									}
									</select>
								</div>
							)
						})
					}
					
				</div> 
			}
			{
				allSelectedAttributes.length > 0 && connectionsPossibilities.length > 0 &&
				<button className="btn btn-primary" onClick={handleAddConnection}>Add Product Item</button>
			}
				<button className="btn btn-primary" onClick={handleClear}>Clear</button>
			</div>

          <div className="me-3">
            {
              	addedConnections.map((con,index) => {

					return (
						<div style={{width: '100%',
										padding: '5px 10px',
										marginBottom: '5px', 
										borderRadius: '4px',
										backgroundColor: '#eee !important'}} 
								key={con.attributeConnection.connAttrId} 
								className="d-flex justify-content-between">
										<Accordion disableGutters sx={{ boxShadow: "none", my: 0 }} expanded={true}>
											<AccordionSummary sx={{ minHeight: 0 }}>
												<MDBox display="flex" justifyContent="space-between" alignItems="center">

													<AttributeConnectionForm connection={con} formValues={con}/>
													<div>

														<button className="btn btn-solid-danger" onClick={() => removeConnection(con.attributeConnection.connAttrId)}>
															<i className="fa fa-trash"></i>
														</button>

														<Switch checked={true} />
													</div>

												</MDBox>
											</AccordionSummary>
											<AccordionDetails sx={{ mx: 0, px: 0, my: 0 }}>
														Details
											</AccordionDetails>
										</Accordion>
							
						</div>
					)

              	})
            }
          </div>   
  

    </PageCard>
  );
};

export default ProductAttributesData;
