import PageCard from "components/PageCard";
import DataInputContainer from "./DataInputContainer";
import SelectMultipleCategories from "./SelectMultipleCategories";
import SelectMultipleTags from "./SelectMultilpleTags";

//material ui
import { InputBase, styled } from "@mui/material";

import MDButton from "components/MDButton";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    border: "1px solid #ced4da",
    fontSize: 16,
    // width: "100%",
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      // borderColor: theme.palette.primary.main,
    },
  },
}));

const ProductInfoData = ({ dataObj, setDataObj, variant }) => {
  return (
    <PageCard title="Product Information">
      <form className="theme-form theme-form-2 mega-form">
        <div className="row">
          <DataInputContainer title="Product title (EN)">
            <BootstrapInput
              value={dataObj.title}
              placeholder="Product title (EN)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  title: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-en"
              label="Product title (EN)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>

          <DataInputContainer title="Product title (AR)">
            <BootstrapInput
              value={dataObj.titleAr}
              placeholder="Product title (AR)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  titleAr: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-ar"
              label="Product title (ar)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>
          <DataInputContainer title="Product Price">
            <BootstrapInput
              value={dataObj.price}
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  price: Number(e.target.value),
                }))
              }
              margin="dense"
              id="product-original-price"
              label="Product original price"
              type="number"
              fullWidth
              style={{ width: "100%" }}
              variant="standard"
            />
          </DataInputContainer>
          <DataInputContainer title="Downloadable Product">
            <MDButton
              color="productButton"
              sx={{ color: "#444 !important" }}
              variant={dataObj.isDownloadable ? "contained" : "outlined"}
              onClick={() =>
                setDataObj((old) => ({ ...old, isDownloadable: !dataObj.isDownloadable }))
              }
            >
              Yes
            </MDButton>
            <MDButton
              color="productButton"
              display="inline-block"
              sx={{ ml: 2, color: "#444 !important" }}
              variant={!dataObj.isDownloadable ? "contained" : "outlined"}
              onClick={() =>
                setDataObj((old) => ({ ...old, isDownloadable: !dataObj.isDownloadable }))
              }
            >
              No
            </MDButton>
          </DataInputContainer>
          <DataInputContainer title="Categories">
            <SelectMultipleCategories variant={variant} dataObj={dataObj} setDataObj={setDataObj} />
          </DataInputContainer>
          <DataInputContainer title="Tags">
            <SelectMultipleTags variant={variant} dataObj={dataObj} setDataObj={setDataObj} />
          </DataInputContainer>
        </div>
      </form>
    </PageCard>
  );
};

export default ProductInfoData;
