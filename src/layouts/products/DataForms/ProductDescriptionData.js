import PageCard from "components/PageCard";
import DataInputContainer from "./DataInputContainer";

//material ui
import { InputBase, styled } from "@mui/material";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    border: "1px solid #ced4da",
    fontSize: 16,
    // width: "100%",
    padding: "10px 12px",
    transition: theme.transitions.create(["border-color", "background-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      // borderColor: theme.palette.primary.main,
    },
  },
}));

const ProductDescriptionData = ({ dataObj, setDataObj }) => {
  return (
    <PageCard title="Product Description">
      <form className="theme-form theme-form-2 mega-form">
        <div className="row">
          <DataInputContainer title="Product short description (EN)">
            <BootstrapInput
              value={dataObj.shortDescription}
              placeholder="short description (EN)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  shortDescription: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-en"
              label="short description (EN)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
              multiline
            />
          </DataInputContainer>
          <DataInputContainer title="Product short description (AR)">
            <BootstrapInput
              value={dataObj.shortDescriptionAr}
              placeholder="short description (AR)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  shortDescriptionAr: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-en"
              label="short description (AR)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
              multiline
            />
          </DataInputContainer>
          <DataInputContainer title="Product description (EN)">
            <BootstrapInput
              value={dataObj.description}
              placeholder="description (EN)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  description: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-en"
              label="description (EN)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
              multiline
              rows={2}
            />
          </DataInputContainer>
          <DataInputContainer title="Product description (AR)">
            <BootstrapInput
              value={dataObj.descriptionAr}
              placeholder=" description (AR)"
              onChange={(e) =>
                setDataObj((oldForms) => ({
                  ...oldForms,
                  descriptionAr: e.target.value,
                }))
              }
              margin="dense"
              id="product-title-en"
              label="description (AR)"
              type="text"
              style={{ width: "100%" }}
              variant="standard"
              multiline
              rows={2}
            />
          </DataInputContainer>
        </div>
      </form>
    </PageCard>
  );
};

export default ProductDescriptionData;
