import { FormControl, IconButton } from "@mui/material";

export default function DataInputContainer({ children, title, variant, deletable }) {
  return (
    <FormControl variant="standard" fullWidth>
      <div className="mb-4 row align-items-center" style={{ paddingLeft: 30 }}>
        <label
          className={`${
            variant === "choose" ? "col-sm-5 col-md-6" : "col-sm-3 col-md-2"
          }  col-form-label form-label-title`}
          style={{ fontWeight: 500, width: variant === "choose" ? "fit-content" : "" }}
        >
          {title}
        </label>
        <div
          className={`${variant === "choose" ? "col-sm-7 col-md-6" : "col-sm-9 col-md-10"}`}
          style={{ display: "flex", gap: 20, alignItems: "center" }}
        >
          {children}
          {deletable?.isDeletable && !deletable?.isJustOne && (
            <IconButton onClick={() => deletable?.handleDelete(deletable?.id)}>
              <i className="far fa-trash-alt theme-color"></i>
            </IconButton>
          )}
        </div>
      </div>
    </FormControl>
  );
}
