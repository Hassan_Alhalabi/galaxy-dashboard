import { useState, useEffect } from "react";

//MD
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

//material ui
import {
  CircularProgress,
  FormControl,
  Select,
  InputLabel,
  OutlinedInput,
  Chip,
  MenuItem,
} from "@mui/material";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 250,
    },
  },
};

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectMultipleAttributes = ({ dataObj,allAttributesStatus, variant,allAttributes, handleSelectChange }) => {

  const [selectedProductAttributes, setSelectedProductAttributes] = useState([]);
  const selectedPorudctAttributesIds = selectedProductAttributes.map(
    (productAttribute) => productAttribute.attributeId
  );

  //sync selected product attributes with dataObj.productAttributes
  useEffect(() => {
    setSelectedProductAttributes(
      dataObj.productAttributes.map((attribute) => ({
        attributeId: attribute.attributeId,
        title: attribute.title,
      }))
    );
  }, [dataObj.productAttributes]);

  return (
    <FormControl fullWidth>
      <InputLabel id="select-attributes">Attributes</InputLabel>
      <Select
        autoWidth
        labelId="select-attributes"
        id="select-attributes"
        multiple
        value={selectedPorudctAttributesIds}
        onChange={(e) => handleSelectChange(e)}
        input={<OutlinedInput id="select-multiple-attributes" label="Attributes" />}
        sx={{ minHeight: 40, background: "#63449b0d !important" }}
        renderValue={() => (
          <MDBox
            sx={{
              display: "flex",
              flexWrap: "wrap",
              gap: 0.5,
              my: 2,
            }}
          >
            {selectedProductAttributes.map((productAttribute) => (
              <Chip
                color="primary"
                size="small"
                sx={{ padding: 2 }}
                key={productAttribute.attributeId}
                label={
                  <MDTypography
                    color="light"
                    sx={{ color: "white", whiteSpace: "nowrap", fontSize: 13 }}
                  >
                    {productAttribute.title}
                  </MDTypography>
                }
              /> 
            ))}
          </MDBox>
        )}
        MenuProps={MenuProps}
      >
        {allAttributesStatus === "success" && allAttributes !== undefined  &&
          allAttributes.attributes.map((attribute) => (
            <MenuItem
              key={attribute.attributeId}
              value={attribute.attributeId}
              sx={{
                my: 1,
                "&:focus": {
                  background: "transparent",
                },
              }}
            >
              {attribute.title}
            </MenuItem>
          ))}
        {allAttributesStatus === "error" && <MDTypography>Theere is No Attributes Attribute</MDTypography>}
        {allAttributesStatus === "loading" || allAttributes === undefined && <Loader />}
      </Select>
    </FormControl>
  );
};

export default SelectMultipleAttributes;
