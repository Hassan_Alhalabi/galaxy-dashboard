import React from "react";
import PageCard from "components/PageCard";
import ImageUploading from "react-images-uploading";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography/index";

const ProductImagesData = ({ setDataObj, dataObj, variant }) => {
  const [images, setImages] = React.useState([{ isMainImageExist: false, file: null }]);

  const maxNumber = 69;
  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    setImages(
      imageList.map((image, index) => {
        return {
          ...image,
          isMainImageExist: image.data_url && index === 0 ? true : false,
        };
      })
    );
    setDataObj((oldForm) => ({
      ...oldForm,
      mainImageData: imageList[0]?.file,
      imageListData: imageList.slice(1).map((image) => image.file),
    }));
  };

  React.useLayoutEffect(() => {
    if (variant === "edit") {
      const mainImage = {
        file: dataObj.mainImage ? { ...dataObj.mainImage } : null,
        data_url: dataObj.mainImage?.downloadUrl,
        isMainImageExist: dataObj.mainImage?.downloadUrl ? true : false,
      };
      const imageGallery = dataObj.productImagesList.map((image) => ({
        file: { ...image },
        data_url: image.downloadUrl,
      }));

      setImages([mainImage, ...imageGallery]);

      //init dataObj to match the current images (this is helpful if the user don't change the images and click submit)
      setDataObj((oldForm) => ({
        ...oldForm,
        mainImageData: mainImage.file,
        imageListData: dataObj.productImagesList,
      }));
    }
  }, []);

  return (
    <PageCard title="Product Images">
      <form className="theme-form theme-form-2 mega-form" onSubmit={(e) => e.preventDefault()}>
        <div className="row">
          <ImageUploading
            multiple
            value={images}
            onChange={onChange}
            maxNumber={maxNumber}
            dataURLKey="data_url"
          >
            {({
              imageList,
              onImageUpload,
              onImageUpdate,
              onImageRemove,
              dragProps,
              isDragging,
            }) => (
              // write your building UI
              <MDBox display="flex" gap={6} flexWrap="wrap" mb={2}>

                
                {!imageList[0].isMainImageExist && (
                  <MDBox
                    sx={{
                      height: 200,
                      width: 250,
                      cursor: "pointer",
                      borderRadius: '12px',
                      backgroundColor: '#63449b22',
                      padding: '20px'
                    }}
                    display="inline-flex"
                    justifyContent="center"
                    alignItems="center"
                    onClick={() => onImageUpdate(0)}
                    alt=""
                    {...dragProps}
                  >
                    <MDTypography textAlign="center">
                      <div>Add Main Image (Drag and Drop)</div>
                      <div style={{
                        width: '40px',
                        height: '40px',
                        margin: '25px auto',
                        padding: '20px',
                        display: 'inline-block',
                        backgroundColor: '#63449b',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: '#FFF',
                        borderRadius: '50%',
                      }}>
                        <i className="fa fa-plus"></i>
                      </div>
                    </MDTypography>
                  </MDBox>
                )}



                {imageList.map((image, index) => {
                  return (
                    <div key={index}>
                      {index === 0 && imageList[0]?.isMainImageExist && (
                        <MDBox
                          sx={{
                            width: 250,
                            height: 200,
                          }}
                          display="inline-flex"
                          flexDirection="column"
                          alignItems="center"
                        >
                          <MDTypography fontWeight="bold" mb={1}>
                            Main Image
                          </MDTypography>
                          <img
                            src={image.data_url}
                            alt=""
                            style={{
                              display: "inline-block",
                              objectFit: "cover",
                              maxWidth: 150,
                              maxHeight: 150,
                            }}
                          />
                          <MDBox mt={1} sx={{ justifySelf: "flex-end" }}>
                            <i
                              style={{ marginRight: 10, cursor: "pointer" }}
                              className="far fa-trash-alt theme-color"
                              onClick={() => {
                                onImageRemove(index);
                                setImages((old) => [
                                  { imageType: "main-image", file: null },
                                  ...old,
                                ]);
                                setDataObj((old) => ({ ...old, mainImageData: null }));
                              }}
                            ></i>
                            <span
                              onClick={() => onImageUpdate(index)}
                              style={{ cursor: "pointer" }}
                              className="lnr lnr-pencil"
                            ></span>
                          </MDBox>
                        </MDBox>
                      )}
                      {index > 0 && (
                        <MDBox
                          sx={{
                            width: 250,
                            height: 200,
                          }}
                          display="inline-flex"
                          flexDirection="column"
                          alignItems="center"
                          justifyContent="flex-between"
                        >
                          <MDTypography mb={1}>Gallery Image</MDTypography>
                          <img
                            src={image.data_url}
                            alt=""
                            style={{
                              display: "inline-block",
                              objectFit: "cover",
                              maxWidth: 150,
                              maxHeight: 150,
                            }}
                          />
                          <MDBox mt={1} sx={{ justifySelf: "flex-end" }}>
                            <i
                              style={{
                                marginRight: 10,
                                cursor: "pointer",
                              }}
                              className="far fa-trash-alt theme-color"
                              onClick={() => onImageRemove(index)}
                            ></i>
                            <span
                              onClick={() => onImageUpdate(index)}
                              style={{ cursor: "pointer" }}
                              className="lnr lnr-pencil"
                            ></span>
                          </MDBox>
                        </MDBox>
                      )}
                    </div>
                  );
                })}


                <MDBox
                  sx={{
                    width: 250,
                    height: 200,
                    cursor: "pointer",
                    borderRadius: '12px',
                    backgroundColor: '#63449b22',
                    padding: '20px'
                  }}
                  display="inline-flex"
                  justifyContent="center"
                  alignItems="center"
                  onClick={onImageUpload}
                  alt=""
                  {...dragProps}
                >
                  <MDTypography textAlign="center" sx={{ color: isDragging ? "#2196F3" : "#333" }}>
                      <div>Add Gallery Image (Drag and Drop)</div>
                      <div style={{
                        width:' 40px',
                        height: '40px',
                        margin: '25px auto',
                        padding: '20px',
                        display: 'inline-block',
                        backgroundColor: '#63449b',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: '#FFF',
                        borderRadius: '50%',
                      }}>
                        <i className="fa fa-plus"></i>
                      </div>
                  </MDTypography>
                </MDBox>



              </MDBox>
            )}
          </ImageUploading>
        </div>
      </form>
    </PageCard>
  );
};

export default ProductImagesData;
