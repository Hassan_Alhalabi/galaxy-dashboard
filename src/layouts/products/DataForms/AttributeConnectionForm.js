
const AttributeConnectionForm = ({connection}) => {

    console.log(connection)

    return (
        <div style={{
            display: 'flex',
        }}>
            {
                Object.keys(connection.attributeConnection.connAttr).map( (key) => {
                    return(<span key={connection.attrId}>{connection.attributeConnection.connAttr[key].attributeName}</span>)
                })
            }
        </div>
    )
}

export default AttributeConnectionForm