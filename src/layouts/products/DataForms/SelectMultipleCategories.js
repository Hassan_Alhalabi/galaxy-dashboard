import { useQuery } from "react-query";
import { useState, useEffect } from "react";
import { getCategories } from "../service";

//MD
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

//material ui
import {
  CircularProgress,
  FormControl,
  Select,
  InputLabel,
  OutlinedInput,
  Chip,
  MenuItem,
} from "@mui/material";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 250,
    },
  },
};

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectMultipleCategoies = ({ dataObj, setDataObj, variant }) => {
  //create selected product categories to make the objects properties in the array uniform
  const [selectedPrductCategories, setSelectedProductCategories] = useState([]);
  //create selected product categories ids(primative type: string) so the Select Component use it instead of objects which is reference type
  const selectedPorudctCategoriesIds = selectedPrductCategories.map(
    (productCategory) => productCategory.categoryId
  );

  const { data: categoriesRes, status } = useQuery("product-categories", getCategories, {
    onSuccess: () => {
      //restore old data for edit
      if (variant === "edit") {
        setSelectedProductCategories(
          dataObj.productCategories.map((productCategory) => ({
            categoryId: productCategory.categoryId,
            title: productCategory.title,
          }))
        );
      }
    },
  });

  //sync selected product categories with dataObj.productCategories
  useEffect(() => {
    setSelectedProductCategories(
      dataObj.productCategories.map((category) => ({
        categoryId: category.categoryId,
        title: category.title,
      }))
    );
  }, [dataObj.productCategories]);

  function handleSelectChange(e) {
    const newSelectedProductCategoriesIds = e.target.value;
    const newSelectedProductCategories = newSelectedProductCategoriesIds.map((categoryId) => {
      return categoriesRes.categories.find((category) => category.categoryId === categoryId);
    });
    setDataObj((oldForm) => ({
      ...oldForm,
      productCategories: newSelectedProductCategories.map((selectedProductCategory) => ({
        categoryId: selectedProductCategory.categoryId,
        title: selectedProductCategory.title,
      })),
    }));
  }

  return (
    <FormControl fullWidth>
      <InputLabel id="select-categories">Categories</InputLabel>
      <Select
        autoWidth
        labelId="select-categories"
        id="select-categories"
        multiple
        value={selectedPorudctCategoriesIds}
        onChange={(e) => handleSelectChange(e)}
        input={<OutlinedInput id="select-multiple-categories" label="Categories" />}
        sx={{ minHeight: 40, background: "#63449b0d !important" }}
        renderValue={() => (
          <MDBox
            sx={{
              display: "flex",
              flexWrap: "wrap",
              gap: 0.5,
              my: 2,
            }}
          >
            {selectedPrductCategories.map((productCategory) => (
              <Chip
                color="primary"
                size="small"
                sx={{ padding: 2 }}
                key={productCategory.categoryId}
                label={
                  <MDTypography
                    color="light"
                    sx={{ color: /* darkMode ? "#fff" : */ "", whiteSpace: "nowrap", fontSize: 13 }}
                  >
                    {productCategory.title}
                  </MDTypography>
                }
              />
            ))}
          </MDBox>
        )}
        MenuProps={MenuProps}
      >
        {status === "success" &&
          categoriesRes.categories.map((category) => (
            <MenuItem
              key={category.categoryId}
              value={category.categoryId}
              sx={{
                my: 1,
                "&:focus": {
                  background: "transparent",
                },
              }}
            >
              {category.title}
            </MenuItem>
          ))}
        {status === "error" && <MDTypography>There is no data to dispaly</MDTypography>}
        {status === "loading" && <Loader />}
      </Select>
    </FormControl>
  );
};

export default SelectMultipleCategoies;
