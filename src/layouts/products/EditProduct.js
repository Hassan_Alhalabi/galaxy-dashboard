import { useState, useEffect } from "react";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { editProduct as editDataObj, getProduct } from "./service";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import FormData from "form-data";
import axios from "axios";
import { toast } from "react-toastify";
import { object, string, number, boolean, array } from "yup";

import LoadingScreen from "react-loading-screen";

import PageContainer from "components/PageContainer";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import ProductInfoData from "./DataForms/ProductInfoData";
import ProductDescriptionData from "./DataForms/ProductDescriptionData";
import ProductInventoryData from "./DataForms/ProductInventoryData";
import ProductImagesData from "./DataForms/ProductImagesData";
import ProductAttributesData from "./DataForms/ProductAttributesData";

import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox";

import { IconButton, Icon } from "@mui/material";

export default function EditProduct() {
  const [isEditProduct, setIsEditingProduct] = useState(false);
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const param = useParams();
  const location = useLocation();

  /* Edit Query */
  const productSchema = object({
    title: string().required(),
    titleAr: string().required(),
    price: number().required().positive(),
    salePrice: number()
      .positive()
      .test(
        "is-sale-price-less-than-price",
        "Sale price is not less than or equal to the original price.",
        (salePrice) => salePrice <= dataObj.price
      ),
    isDownloadable: boolean(),
    productCategories: array().min(1, "You must add one category at least for this product."),
    // mainImageData: string().required("You must add the main image of the product."),
    availableQuantity: number()
      .min(1, "Available quantity must be greater or equal to one")
      .max(2147483647, "available quantity must be less than or equal to 2147483647"),
    minQuantity: number()
      .min(1, "Minimum quantity must be greater or equal to one")
      .max(2147483647, "Minimum quantity must be less than or equal to 2147483647"),
  });
  const [dataObj, setDataObj] = useState({
    //--product info
    title: "",
    titleAr: "",
    price: 0,
    salePrice: 0,
    isDownloadable: true,
    productCategories: [],
    productTags: [], //nullable

    //--product description
    description: "",
    descriptionAr: "",
    shortDescription: "",
    shortDescriptionAr: "",

    mainImageId: "",
    productImagesList: [], //nullabel
    mainImageData: null,
    imageListData: [],

    //--product inventory
    availableQuantity: 0,
    minQuantity: 0,
    manageStockWithStatus: true,
    stockStatus: "InStock",
    sku: "",

    //--prduct variety
    productAttributes: [], //nullable
  });

  /* Get Query (table data) */
  const {
    isLoading: isProductLoading,
    isError: isProductError,
    isSuccess: isProductSuccess,
    refetch: refetchProduct,
  } = useQuery(["product", param.id], () => getProduct(param.id), {
    staleTime: 15 * 1000,
    retry: 3,
    enable: false,
    onSuccess: (productRes) => {
      handleInitDataObj(productRes);
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });

  //restoring data
  useEffect(async () => {
    if (!location.state?.product) {
      //get the product
      refetchProduct();
    } else {
      handleInitDataObj(location.state.product);
    }
  }, []);

  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      toast.success("Product edited successfully");
      queryClient.invalidateQueries("products");
      navigate("/products");
    },
    onError: (error) => {
      setIsEditingProduct(false);
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });

  const handleEditDataObj = async () => {
    //Input Validation
    await productSchema
      .validate(dataObj)
      .then(() => {
        const mainImageStatus = dataObj.mainImageData?.attachmentId
          ? "existOld"
          : !dataObj.mainImageData
          ? "notExist"
          : "existNew";
        setIsEditingProduct(true);
        // sending post request for each image
        const imagesPromises = [];
        //if it's exist already exist
        if (mainImageStatus === "existNew") {
          let data = new FormData();
          data.append("file", dataObj.mainImageData);
          const mainImagePromise = axios.post("Attachment/upload", data, {
            headers: {
              accept: "application/json",
              "Accept-Language": "en-US,en;q=0.8",
              "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
            },
          });
          imagesPromises.push(mainImagePromise);
        }
        // push promises of the images list request
        if (dataObj.imageListData.length > 0) {
          for (let imageData of dataObj.imageListData) {
            if (imageData.attachmentId) continue;
            let data = new FormData();
            data.append("file", imageData);
            imagesPromises.push(
              axios.post("Attachment/upload", data, {
                headers: {
                  accept: "application/json",
                  "Accept-Language": "en-US,en;q=0.8",
                  "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
                },
              })
            );
          }
        }

        //sending request for all images
        Promise.all(imagesPromises)
          .then((dataRes) => {
            console.log("in images success");
            const galleryImagesRes = mainImageStatus === "existNew" ? dataRes.slice(1) : dataRes;

            //already exist images
            setDataObj((oldForm) => ({
              ...oldForm,
              //if main image in promise or not
              mainImageId:
                mainImageStatus === "existNew"
                  ? dataRes[0].attachmentId
                  : mainImageStatus === "existOld"
                  ? dataObj.mainImageData?.attachmentId
                  : "",
              //the existed and not existed images in the database
              productImagesList: [
                ...dataObj.imageListData
                  .filter((image) => image.attachmentId)
                  .map((image) => ({ attachmentId: image.attachmentId })),
                ...galleryImagesRes,
              ],
            }));

            setIsEditingProduct(false);

            editMutation.mutate({
              ...dataObj,
              //if main image in promise or not
              mainImageId:
                mainImageStatus === "existNew"
                  ? dataRes[0].attachmentId
                  : mainImageStatus === "existOld"
                  ? dataObj.mainImageData?.attachmentId
                  : "",
              //the existed and not existed images in the database
              productImagesList: [
                ...dataObj.imageListData
                  .filter((image) => image.attachmentId)
                  .map((image) => ({ attachmentId: image.attachmentId })),
                ...galleryImagesRes,
              ],
            });
          })

          .catch((error) => {
            console.log("in images error");
            setIsEditingProduct(false);
            error.isExpectedError ? toast.error(error.response.data[0]) : null;
          });
      })

      .catch((err) => {
        console.log("in validation error");
        setIsEditingProduct(false);
        if (err.errors) err.errors[0] ? toast.error(err.errors[0]) : null;
      });
  };

  //init data obj (product from the get request => product for the edit request)
  function handleInitDataObj(product) {
    setDataObj((oldForm) => {
      const mainImageId = product.mainImage?.attachmentId || "";
      const productImagesList = product.productImagesList.map(
        (productImage) => productImage.attachment
      );
      const productCategories = product.productCategories.map(
        (productCategory) => productCategory.category
      );
      const productTags = product.productTags.map((productTag) => productTag.tag);
      const productAttributes = product.productAttributes.map(
        (productAttribute) => productAttribute.attribute
      );

      return {
        ...oldForm,
        ...product,
        mainImageId,
        productImagesList,
        productCategories,
        productTags,
        productAttributes,
      };
    });
  }

  return (
    <LoadingScreen
      loading={isProductLoading || isEditProduct}
      bgColor="#FFFFFFAA"
      spinnerColor="#63449b"
      textColor="#676767"
      children=""
    >
      <DashboardLayout>
        <DashboardNavbar />
        {/* if product i want to edit exist */}
        {dataObj.title.length > 0 && (
          <>
            <PageContainer title="Edit Product">
              <ProductInfoData variant="edit" dataObj={dataObj} setDataObj={setDataObj} />
              <ProductDescriptionData dataObj={dataObj} setDataObj={setDataObj} />
              <ProductInventoryData dataObj={dataObj} setDataObj={setDataObj} />
              <ProductImagesData variant="edit" dataObj={dataObj} setDataObj={setDataObj} />
              <ProductAttributesData variant="edit" dataObj={dataObj} setDataObj={setDataObj} />
            </PageContainer>
            <IconButton
              sx={{
                position: "fixed",
                bottom: 90,
                width: "fit-content",
                bgcolor: "#63449b",
                "&:hover": {
                  bgcolor: "#63449b",
                },
              }}
              onClick={handleEditDataObj}
            >
              <Icon sx={{ color: "#fff" }}>edit</Icon>
            </IconButton>
          </>
        )}
        {isProductError && (
          <MDBox display="flex" flexDirection="column" alignItems="center">
            <MDTypography>Error Occurred</MDTypography>
            <IconButton onClick={refetchProduct}>
              <Icon>replay</Icon>
            </IconButton>
          </MDBox>
        )}
      </DashboardLayout>
    </LoadingScreen>
  );
}
