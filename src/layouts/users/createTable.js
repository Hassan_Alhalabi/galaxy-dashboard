import DataTable from "examples/Tables/DataTable";
import { useMaterialUIController } from "context";
import axios from "axios";

import PermissionsGate from "auth/PermissionGate";

import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox";
import MDButton from "components/MDButton";
import { Icon } from "@mui/material";

const User = ({ fullName, email }) => (
  <MDBox lineHeight={1}>
    <MDTypography display="block" variant="caption" fontWeight="medium">
      {fullName}
    </MDTypography>
    <MDTypography variant="caption">{email}</MDTypography>
  </MDBox>
);

export default function CreateTable({ dataArr, columns, handleEditModalOpen }) {
  const [controller] = useMaterialUIController();
  const { darkMode } = controller;

  const rows = dataArr.map((dataObj) => {
    const isCustomer = !dataObj.role ? true : false;

    let row = {
      user: (
        <User
          fullName={`${dataObj.firstName} ${dataObj.lastName} (${dataObj.userName})`}
          email={dataObj.email}
        />
      ),
      userRole: (
        <MDTypography variant="caption" color="text" fontWeight="medium">
          {dataObj.role?.name ? dataObj.role.name : ""}
        </MDTypography>
      ),
      phoneNumber: (
        <MDTypography variant="caption" color="text" fontWeight="medium">
          {`${dataObj.phoneNumber}`}
        </MDTypography>
      ),
      action: (
        <MDBox display="flex" alignItems="center" mt={{ xs: 2, sm: 0 }} ml={{ xs: -1.5, sm: 0 }}>
          <PermissionsGate requiredScopes={["Permissions.User.Edit"]}>
            {!isCustomer && (
              <MDButton
                onClick={() => handleEditModalOpen(dataObj)}
                variant="text"
                color={darkMode ? "white" : "dark"}
              >
                <Icon>edit</Icon>
              </MDButton>
            )}
          </PermissionsGate>
        </MDBox>
      ),
    };

    return row;
  });

  return (
    <DataTable
      table={{ columns, rows }}
      noEndBorder
      isSorted={false}
      entriesPerPage={false}
      showTotalEntries={false}
      canSearch={false}
    />
  );
}
