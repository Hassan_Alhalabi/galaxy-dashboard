import React, { useState } from "react";

import { List, ListItem, Popover } from "@mui/material";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";

import MDButton from "components/MDButton";

const SelectUsersType = ({ selectedUsersType, setSelectedUsersType }) => {
  const [menuAnchor, setMenuAnchor] = useState(null);

  return (
    <>
      <MDButton onClick={({ currentTarget }) => setMenuAnchor(currentTarget)}>
        {selectedUsersType}
        <ArrowDropDown fontSize="small" />
      </MDButton>
      <Popover
        open={menuAnchor ? true : false}
        anchorEl={menuAnchor}
        onClose={() => setMenuAnchor(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        PaperProps={{ sx: { background: "#fff", mt: 1 } }}
      >
        <div>
          <List>
            <ListItem
              sx={{ py: 0.5, borderRadius: 2, px: 1 }}
              button
              onClick={() => {
                setSelectedUsersType("All");
                setMenuAnchor(null);
              }}
            >
              All
            </ListItem>
            <ListItem
              sx={{ py: 0.5, borderRadius: 2, px: 1 }}
              button
              onClick={() => {
                setSelectedUsersType("Employees");
                setMenuAnchor(null);
              }}
            >
              Employees
            </ListItem>
            <ListItem
              sx={{ py: 0.5, borderRadius: 2, px: 1 }}
              button
              onClick={() => {
                setSelectedUsersType("Customers");
                setMenuAnchor(null);
              }}
            >
              Customers
            </ListItem>
          </List>
        </div>
      </Popover>
    </>
  );
};

export default SelectUsersType;
