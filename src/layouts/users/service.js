import http from "../../util/httpService";

const apiUrl = "/Users";

export function getUsers() {
  return http.get(`${apiUrl}/all`);
}

export function createUser(user) {
  console.log(user);
  return http.post(apiUrl, user);
}

export function deleteUser(userId) {
  return http.delete(`${apiUrl}/${userId}`);
}

export function editUser(user) {
  console.log(user);
  return http.put(apiUrl, user);
}

export function getRoles() {
  return http.get(`/Roles/GetAllRoles`);
}
