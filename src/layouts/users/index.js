import React, { useState } from "react";
import { toast } from "react-toastify";
import { useQuery, useMutation, useQueryClient } from "react-query";
import {
  getUsers as getDataReq,
  deleteUser as deleteDataObj,
  createUser as createDataObj,
  editUser as editDataObj,
} from "./service";
import { object, string } from "yup";

//MD comonents
import MDBox from "components/MDBox";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import PermissionsGate from "auth/PermissionGate";

//my components
import TableLoader from "components/TableLoader";
import AlertDialog from "components/AlertDialog";
import FormModal from "components/FormModal";
import DataForm from "./Data Form";
import CreateTable from "./createTable";
import ColumnTitle from "common/ColumnTitle";
import SelectUsersType from "./SelectUsersType";

//material ui
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import { Icon } from "@mui/material";

const Users = () => {
  const queryClient = useQueryClient();
  const [selectedUsersType, setSelectedUsersType] = useState("All");

  /* Get Query (table data) */
  const {
    isLoading: isPageLoading,
    isError: isPageError,
    isSuccess: isPageSuccess,
    data: dataReq,
  } = useQuery(["users"], getDataReq, {
    retry: false,
    cacheTime: 10 * 1000, //10min
    staleTime: 5 * 60 * 1000, //5min
    keepPreviousData: true,
    onError: (error) => {
      if (error.response.data?.length !== 0)
        error.isExpectedError
          ? toast.error(error.response.data[0])
          : toast.error("An Unexpected Error Occurred!");
    },
  });

  //columns titles
  const columns = [
    {
      Header: <ColumnTitle align="left" title="User" />,
      accessor: "user",
      align: "left",
    },
    {
      Header: <ColumnTitle title="Role" />,
      accessor: "userRole",
      align: "center",
    },
    {
      Header: <ColumnTitle title="Phone" />,
      accessor: "phoneNumber",
      align: "center",
    },
    {
      Header: <ColumnTitle title="Action" />,
      accessor: "action",
      align: "center",
    },
  ];

  /* Dialgos */
  /* General */
  const handleCloseModal = () => {
    setIsCreateModalOpen(false);
    setIsDeleteModalOpen(false);
    setIsEditModalOpen(false);
  };

  const [selectedDataObj, setSelectedDataObj] = useState();

  const createSchema = object({
    firstName: string().required("First name a is required field."),
    userName: string().required("Username a is required field."),
    email: string().required("Email a is required field.").email("Email should be valid email."),
    password: string().required("Password a is required field."),
    confirmPassword: string()
      .required("Confirm password a is required field.")
      .test(
        "is-confirm-password-same-as-password",
        "Confirm password should be same as the password.",
        (confirmPassword) => confirmPassword === dataObj.password
      ),
    phoneNumber: string()
      .required("Phone number is a required field.")
      .test("is-valid-phone-number", "Phone number must be valid number", (phoneNumber) =>
        /^(\+[0-9]{10,13})$/.test(phoneNumber)
      )
      .required("Phone number is a required field.")
      .test("is-valid-phone-number", "Phone number must be a valid number.", (phoneNumber) =>
        /^(\+[0-9]{10,13})$/.test(phoneNumber)
      ),
    roleId: string().required("You must choose a role for this employee."),
  });

  const editSchema = object({
    firstName: string().required("First name a is required field."),
    userName: string().required("Username a is required field."),
    email: string().required("Email a is required field.").email("Email should be valid email."),
    phoneNumber: string()
      .test("is-valid-phone-number", "Phone number must be valid number", (phoneNumber) =>
        /^(\+[0-9]{10,13})$/.test(phoneNumber)
      )
      .required("Phone number is a required field."),
    roleId: string().required("You must choose a role for this employee."),
  });

  /* Create Query */
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const [dataObj, setDataObj] = useState({
    firstName: "",
    lastName: "",
    userName: "",
    email: "",
    password: "",
    confirmPassword: "",
    phoneNumber: "",
    roleId: "",
  });
  const createMutation = useMutation(createDataObj, {
    onSuccess: () => {
      toast.success("User added successfully");
      queryClient.invalidateQueries("users");
    },
    onError: (error) => {
      console.log(error.response);
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  const handleCreateDataObj = () => {
    createSchema
      .validate(dataObj)
      .then(() => {
        setIsCreateModalOpen(false);
        createMutation.mutate(dataObj);
      })
      .catch((error) => {
        error.errors && error.errors.length > 0
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred.");
      });
  };

  /* Delete Query */
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  function handleDeleteModalOpen(dataObj) {
    setSelectedDataObj(dataObj);
    setIsDeleteModalOpen(true);
  }
  const deleteMutation = useMutation(deleteDataObj, {
    onSuccess: () => {
      toast.success("User deleted successfully");
      queryClient.invalidateQueries("users");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  function handleDeleteDataObj() {
    setIsDeleteModalOpen(false);
    deleteMutation.mutate(selectedDataObj.id);
  }

  /* Edit Query */
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  function handleEditModalOpen(dataObj) {
    setSelectedDataObj({ ...dataObj, roleId: dataObj.role?.id });
    setIsEditModalOpen(true);
  }
  const editMutation = useMutation(editDataObj, {
    onSuccess: () => {
      toast.success("User edited successfully");
      queryClient.invalidateQueries("users");
    },
    onError: (error) => {
      error.isExpectedError ? toast.error(error.response.data[0]) : null;
    },
  });
  function handleEditDataObj() {
    editSchema
      .validate(selectedDataObj)
      .then(() => {
        setIsEditModalOpen(false);
        editMutation.mutate(selectedDataObj);
      })
      .catch((error) => {
        error.errors && error.errors.length > 0
          ? toast.error(error.errors[0])
          : toast.error("An Unexpected Error Occurred.");
      });
  }

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <PermissionsGate requiredScopes={["Permissions.User.View"]}>
        <MDBox py={3}>
          <Grid container spacing={6}>
            <Grid item xs={12}>
              {/* Add User button */}
              <PermissionsGate requiredScopes={["Permissions.User.Insert"]}>
                {isPageSuccess && (
                  <MDBox pb={6} px={3}>
                    <Grid
                      container
                      spacing={2}
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Grid item>
                        <MDButton color="secondary" onClick={() => setIsCreateModalOpen(true)}>
                          Add User&nbsp;&nbsp;<Icon>add</Icon>
                        </MDButton>
                      </Grid>
                    </Grid>
                  </MDBox>
                )}
              </PermissionsGate>
              <Card>
                <MDBox
                  mx={2}
                  mt={-3}
                  py={3}
                  px={2}
                  variant="gradient"
                  bgColor="info"
                  borderRadius="lg"
                  coloredShadow="info"
                >
                  <MDBox gap={2} display="flex" justifyContent="space-between" alignItems="center">
                    <MDTypography variant="h6" color="white">
                      Users
                    </MDTypography>
                    {isPageSuccess && (
                      <SelectUsersType
                        selectedUsersType={selectedUsersType}
                        setSelectedUsersType={setSelectedUsersType}
                      />
                    )}
                  </MDBox>
                </MDBox>
                <MDBox pt={3}>
                  {isPageLoading && <TableLoader />}
                  {isPageError && (
                    <MDTypography sx={{ textAlign: "center", mb: 2 }}>
                      There is no data to display
                    </MDTypography>
                  )}
                  {isPageSuccess && (
                    <CreateTable
                      dataArr={
                        selectedUsersType === "Customers"
                          ? dataReq.filter((user) => user.role === null)
                          : selectedUsersType === "Employees"
                          ? dataReq.filter((user) => user.role !== null)
                          : dataReq
                      }
                      columns={columns}
                      handleEditModalOpen={handleEditModalOpen}
                    />
                  )}
                </MDBox>
              </Card>
            </Grid>
          </Grid>
        </MDBox>
        {/* Create Dialog */}
        <FormModal
          title="Add Employee"
          actionBtnTitle="Add"
          isOpen={isCreateModalOpen}
          handleDoAction={handleCreateDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm dataObj={dataObj} setDataObj={setDataObj} />
        </FormModal>
        {/* Delete Dialog */}
        <AlertDialog
          isOpen={isDeleteModalOpen}
          handleClose={handleCloseModal}
          handleAction={handleDeleteDataObj}
          title="Delete!"
          desc={`Are You Sure You Want To Delete ${selectedDataObj?.firstName} ${selectedDataObj?.lastName}?`}
        />
        {/* Edit Dialog */}
        <FormModal
          title={`Edit Employee`}
          actionBtnTitle="Edit"
          isOpen={isEditModalOpen}
          handleDoAction={handleEditDataObj}
          handleClose={handleCloseModal}
        >
          <DataForm variant="edit" dataObj={selectedDataObj} setDataObj={setSelectedDataObj} />
        </FormModal>
      </PermissionsGate>
    </DashboardLayout>
  );
};

export default Users;
