import React, { useState } from "react";
import { useQuery } from "react-query";
import { toast } from "react-toastify";
import http from "util/httpService";

import {
  MenuItem,
  FormControl,
  InputLabel,
  Select,
  CircularProgress,
  OutlinedInput,
} from "@mui/material";

import MDTypography from "components/MDTypography";
import MDBox from "components/MDBox";

const ITEM_HEIGHT = 70;
const ITEM_PADDING_TOP = 50;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 200,
      width: "fit-content",
      outline: "1px solid #333",
    },
  },
};

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectRole = ({ dataObj, setDataObj, variant }) => {
  const [selectedRole, setSelectedRole] = useState();
  const {
    data: roles,
    status,
    error,
  } = useQuery("roles", getRoles, {
    //for edit
    onSuccess: (fetchedData) => {
      const currentSelectedRole = fetchedData.find((role) => {
        if (variant === "edit") return role.id === dataObj.role?.id;
        return role.id === dataObj.roleId;
      });
      setSelectedRole(currentSelectedRole);
    },
  });
  if (status === "error") toast.error(error.response.data[0]);

  return (
    <FormControl fullWidth>
      <InputLabel id="demo-role-label">Select Role</InputLabel>
      <Select
        sx={{ height: "40px" }}
        labelId="demo-role-label"
        id="demo-role"
        value={selectedRole ? selectedRole.name : ""}
        input={<OutlinedInput label="Select Role" />}
        MenuProps={MenuProps}
        variant="standard"
      >
        {status === "loading" && <Loader />}
        {status === "error" && <MDTypography>There is no data to display</MDTypography>}
        {status === "success" &&
          roles.map((role) => (
            <MenuItem
              onClick={() => {
                setSelectedRole(role);
                setDataObj((oldForms) => ({
                  ...oldForms,
                  roleId: role.id,
                }));
              }}
              key={role.id}
              value={role.name}
            >
              {role.name}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default SelectRole;

function getRoles() {
  return http.get(`/Roles/GetAllRoles`);
}
