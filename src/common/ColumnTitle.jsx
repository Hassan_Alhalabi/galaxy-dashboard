import MDTypography from "components/MDTypography";

export default function ColumnTitle({ title, align }) {
  return (
    <MDTypography
      sx={{ whiteSpace: "nowrap", userSelect: "none" }}
      display="flex"
      alignItems="center"
      flexWrap="nowrap"
      variant="h6"
      fontWeight="medium"
      justifyContent={align === "left" ? "flex-start" : "center"}
    >
      {title}
    </MDTypography>
  );
}
