import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useQuery } from "react-query";
import { useMaterialUIController } from "context";

import MDBox from "components/MDBox";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";

//material ui
import { CircularProgress, ClickAwayListener } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import CloseIcon from "@mui/icons-material/Close";
import { Select } from "@mui/material";

//services
import { getStates, getCities, getCountries } from ".././layouts/./customers/service";

/* function getStyles(name, object, theme) {
  return {
    fontWeight:
      object.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
} */

const ITEM_HEIGHT = 70;
const ITEM_PADDING_TOP = 50;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 290,
      width: "fit-content",
      paddingTop: 20,
      outline: "1px solid #333",
    },
  },
};

const selectTitleStyle = {
  borderRadius: 5,
  position: "fixed",
  height: 20,
  padding: 4,
  pl: 0,
  pr: 0,
  minWidth: "320px",
  margin: 0,
  transform: "translateY(-80px) translateX(-20px)",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  zIndex: 99,
  color: "white",
};

const selectTitleMenuItemStyle = [
  {
    "&:hover": {
      color: "inherit",
      backgroundColor: "inherit",
    },
  },
  {
    "&:focus": {
      color: "inherit",
      backgroundColor: "inherit",
    },
  },
];

const Loader = () => {
  return (
    <MDBox
      display="flex"
      justifyContent="center"
      alignItems="center"
      height={ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP - 150}
    >
      <CircularProgress size={30} color="secondary" />
    </MDBox>
  );
};

const SelectLocation = ({ setDataObj, variant, dataObj }) => {
  const [controller] = useMaterialUIController();
  const { darkMode } = controller;
  const [selectLocationPopoverOpen, setSelectLocationPopoverOpen] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [selectedState, setSelectedState] = useState(null);
  const [selectedCity, setSelectedCity] = useState(null);

  //restoring selected location if it exist
  useEffect(() => {
    setSelectedCountry(dataObj.city?.country);
    setSelectedState(dataObj.city?.state);
    setSelectedCity(dataObj.city);
  }, []);
  //empty the location(cityId) if the user re-open the data form(create) after open it and insert a location
  useEffect(() => {
    if (!selectedCity && variant !== "edit") {
      setDataObj((oldForms) => ({ ...oldForms, cityId: -1 }));
    }
    //for edit set city id to much the selected city
    if (selectedCity && variant === "edit")
      setDataObj((oldForm) => ({ ...oldForm, cityId: selectedCity.cityId }));
  }, [selectedCity]);

  /* Location Queries */
  const {
    data: countries,
    status: countriesStatus,
    error: countriesError,
  } = useQuery("countries", getCountries, {
    staleTime: 60000,
    retry: false,
    keepPreviousData: true,
  });
  const {
    data: states,
    error: statesError,
    status: statesStatus,
    refetch: statesRefetch,
  } = useQuery(
    ["customerState", selectedCountry?.countryId],
    () => getStates(selectedCountry?.countryId),
    {
      enabled: false,
      cacheTime: 0,
      staleTime: 90000,
    }
  );
  const {
    data: cities,
    error: citiesError,
    status: citiesStatus,
    refetch: citiesRefetch,
  } = useQuery(["customerCity", selectedState?.stateId], () => getCities(selectedState?.stateId), {
    enabled: false,
    cacheTime: 0,
    staleTime: 90000,
  });
  if (countriesStatus === "error")
    countriesError.isExpectedError ? toast.error(countriesError.response.data[0]) : null;
  if (statesStatus === "error")
    statesError.isExpectedError ? toast.error(statesError.response.data[0]) : null;
  if (citiesStatus === "error")
    citiesError.isExpectedError ? toast.error(citiesError.response.data[0]) : null;
  const isLoading =
    citiesStatus === "loading" || statesStatus === "loading" || countriesStatus === "loading";
  /* End */
  let currentLocationMenu = "country";
  if (selectedState) currentLocationMenu = "city";
  else if (selectedCountry) currentLocationMenu = "state";

  const value = selectedCountry
    ? `${selectedCountry.name}${selectedState ? `, ${selectedState.stateName}` : ""}${
        selectedCity ? `, ${selectedCity.cityName}` : ""
      }`
    : "Select Location";

  //handlers
  function handleCountrySelect(country) {
    setSelectedCountry(country);
  }
  useEffect(() => {
    statesRefetch();
  }, [selectedCountry]);

  function handleStateSelect(state) {
    setSelectedState(state);
  }
  useEffect(() => {
    citiesRefetch();
  }, [selectedState]);

  function handleCitySelect(e, city) {
    e.stopPropagation();
    setSelectedCity(city);
    setDataObj((oldForms) => ({ ...oldForms, cityId: city.cityId }));
    setSelectLocationPopoverOpen(false);
  }

  function handleSelectLocationClickAway() {
    setSelectLocationPopoverOpen(false);
  }

  return (
    <FormControl fullWidth variant="outlined">
      <Select
        sx={{ height: "40px" }}
        open={selectLocationPopoverOpen}
        displayEmpty
        renderValue={() => <p style={{ color: darkMode ? "#fff" : "#7b809a" }}>{value}</p>}
        labelId="demo-location-label"
        id="demo-location"
        MenuProps={MenuProps}
        onClick={() => setSelectLocationPopoverOpen(true)}
      >
        {/* --city */}
        {currentLocationMenu === "city" && (
          <ClickAwayListener onClickAway={handleSelectLocationClickAway}>
            <div>
              <MDBox sx={selectTitleStyle} bgColor="primary" color="white">
                <MenuItem disableRipple value="0" sx={selectTitleMenuItemStyle}>
                  <MDTypography sx={{ cursor: "auto", color: "white !important" }}>
                    <MDButton
                      sx={{ padding: 0 }}
                      onClick={() => {
                        setSelectedCity(null);
                        setSelectedState(null);
                      }}
                    >
                      <ArrowBackIcon color="primary" fontSize="large" />
                    </MDButton>
                    &nbsp; Select City
                  </MDTypography>
                </MenuItem>
              </MDBox>
              {citiesStatus !== "success" ? (
                <Loader />
              ) : (
                cities.cities.map((city) => (
                  <MenuItem
                    key={city.cityId}
                    value={city.cityId}
                    // style={getStyles(city.cityName, city, theme)}
                    onClick={(e) => handleCitySelect(e, city)}
                  >
                    {`${value}${!selectedCity ? ", " + city.cityName : ""}`}
                  </MenuItem>
                ))
              )}
            </div>
          </ClickAwayListener>
        )}
        {/* --state */}
        {currentLocationMenu === "state" && (
          <ClickAwayListener onClickAway={handleSelectLocationClickAway}>
            <div>
              <MDBox sx={selectTitleStyle} bgColor="primary" color="white">
                <MenuItem disableRipple value="0" sx={selectTitleMenuItemStyle}>
                  <MDTypography sx={{ cursor: "auto", color: "white !important" }}>
                    <MDButton sx={{ padding: 0 }} onClick={() => setSelectedCountry(null)}>
                      <ArrowBackIcon color="primary" fontSize="large" />
                    </MDButton>
                    &nbsp; Select State
                  </MDTypography>
                </MenuItem>
              </MDBox>
              {statesStatus !== "success" ? (
                <Loader />
              ) : (
                states.states.map((state) => (
                  <MenuItem
                    key={state.stateId}
                    value={state.stateId}
                    // style={getStyles(state.stateName, state, theme)}
                    onClick={() => handleStateSelect(state)}
                  >
                    {`${value}, ${state.stateName}`}
                  </MenuItem>
                ))
              )}
            </div>
          </ClickAwayListener>
        )}
        {/* --country */}
        {currentLocationMenu === "country" && (
          <ClickAwayListener onClickAway={handleSelectLocationClickAway}>
            <div>
              <MDBox sx={selectTitleStyle} bgColor="primary" color="white">
                <MenuItem disableRipple value="0" sx={selectTitleMenuItemStyle}>
                  <MDTypography sx={{ cursor: "auto", color: "white !important" }}>
                    <MDButton
                      onClick={(e) => {
                        e.stopPropagation();
                        setSelectLocationPopoverOpen(false);
                        setSelectedCountry(null);
                      }}
                    >
                      <CloseIcon color="primary" fontSize="large" />
                    </MDButton>
                    &nbsp; Select Country
                  </MDTypography>
                </MenuItem>
              </MDBox>
              {countriesStatus !== "success" ? (
                <Loader />
              ) : (
                countries.countries.map((country) => (
                  <MenuItem
                    key={country.countryId}
                    value={country.countryId}
                    // style={getStyles(country.name, country, theme)}
                    onClick={() => handleCountrySelect(country)}
                  >
                    {country.name}
                  </MenuItem>
                ))
              )}
            </div>
          </ClickAwayListener>
        )}
      </Select>
    </FormControl>
  );
};

export default SelectLocation;
