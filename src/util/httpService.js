import axios from "axios";
import Cookies from "js-cookie";
import { toast } from "react-toastify";

axios.defaults.baseURL = "https://alshahbatech-003-site1.itempurl.com/api";

axios.defaults.headers.common["Authorization"] = `Bearer ${Cookies.get("GDUserToken")}`;

axios.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    // if login in with no validate data 401 returned
    // if (error.response.status === 401) {
    //   location.replace("/logout");
    // }
    if (error.response.status === 403) {
      toast.error("You Are not authorized to do this action.");
    }
    if (error.response.status === 404) {
      toast.error("Not Found!");
    }

    const isExpectedError =
      error.response && error.response.status >= 400 && error.response.status < 500;

    error.isExpectedError = isExpectedError;

    if (!isExpectedError) {
      toast.error("An unexpected error occurrred.");
    }
    if (
      isExpectedError &&
      error.response.data?.length === 0 &&
      error.response.status !== 404 &&
      error.response.status !== 403
    )
      toast.error("An unexpected error occurrred.");
    // return error;
    return Promise.reject(error);
  }
);

export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
};
