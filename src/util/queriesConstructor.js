export default function queriesConstructor(queriesObject) {
  const isQueryEmpty = Object.keys(queriesObject).length === 0;

  let queriesCombined = "";
  for (const [key, value] of Object.entries(queriesObject)) {
    queriesCombined = queriesCombined.concat(`&${key}=${value}`);
  }
  //delete first (&)
  queriesCombined = queriesCombined.slice(1);

  return `${!isQueryEmpty && `?${queriesCombined}`}`;
  //ex "?key=value&key2=value2"
}
