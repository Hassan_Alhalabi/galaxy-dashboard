//categories, selectedCategory.superCategoryId, setParentsIdsState
export default function findParentIds({ list, targetChild, setParentsIdsState }) {
  for (let parent of list) {
    parentIdsRoller({ node: parent, targetChild, setParentsIdsState, grandParent: parent });
  }
}

function parentIdsRoller({
  node,
  parentIdsList = [],
  targetChild,
  setParentsIdsState,
  grandParent,
}) {
  if (node.categoryId === targetChild) {
    // "in success find it"
    setParentsIdsState([grandParent.categoryId, ...parentIdsList]);
  } else if (node.subCategories.length > 0) {
    // "in there is subCategories and could't find the child"
    node.subCategories.map((category) => {
      parentIdsRoller({
        node: category,
        parentIdsList: [...parentIdsList, category.categoryId],
        targetChild,
        setParentsIdsState,
        grandParent,
      });
    });
  } else {
    // "in there is not subCategory and could't find the child"
    return null;
  }
}
