const getAttributesConnectionName = (connection) => {
  let connectionsArray = [];

  function getAttributesConnectionNameRoller(connection) {
    connectionsArray.push(connection.attributeName);

    if (!connection.subAttributeConnections) {
      return connectionsArray;
    }

    getAttributesConnectionNameRoller(connection.subAttributeConnections);
  }

  getAttributesConnectionNameRoller(connection);

  return connectionsArray;
};

export default getAttributesConnectionName;
