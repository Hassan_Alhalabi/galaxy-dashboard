import uuid from "react-uuid";

export const getPossibleAttributes = (attribute) => {

    const possibleAttributes = [];

    const possibleAttributesRoller = (attribute) => {
      if (attribute.subAttributes.length === 0) {
        possibleAttributes.push(attribute);
      }
      attribute.subAttributes.map((attribute) => {
        possibleAttributesRoller(attribute);
      });
    };

    possibleAttributesRoller(attribute);

    return possibleAttributes;

}

const subAttr = (ob,con) => {
  if(ob.subAttributeConnections === null || ob === undefined) {
      return con;
  }
  const c = {
      ...con,
      connAttrId: con.connAttrId+ob.subAttributeConnections.attributeName,
      connAttr: {
            ...con.connAttr,
            [ob.subAttributeConnections.rootName]: {
                                                        attributeId: ob.subAttributeConnections.attributeId,
                                                        rootName: ob.subAttributeConnections.rootName,
                                                        rootId: ob.subAttributeConnections.rootId,
                                                        attributeName: ob.subAttributeConnections.attributeName,
                                                        type: ob.subAttributeConnections.attributeType
                                                    }
      }
  }

  return subAttr(ob.subAttributeConnections,c); 

}

export const getConnections = (list) => {

  let connections = []
  let connection = {};
  
  list.map((listItem) => {

      connection = { 
          attrId: uuid(),
          availableQuantity: listItem.availableQuantity,
          manageStockWithStatus: listItem.manageStockWithStatus,
          minQuantity: listItem.minQuantity,
          price: listItem.price,
          salePrice: listItem.salePrice,
          stockStatus: listItem.stockStatus,
          connAttrId: listItem.attributeConnection.attributeName,
          connAttr: {
                [listItem.attributeConnection.rootName]: { 
                                                        attributeId: listItem.attributeConnection.attributeId,
                                                        rootName: listItem.attributeConnection.rootName,
                                                        rootId: listItem.attributeConnection.rootId,
                                                        attributeName: listItem.attributeConnection.attributeName, 
                                                        type: listItem.attributeConnection.attributeType
                                                      }
          }
      }        
      connections.push(subAttr(listItem.attributeConnection,connection));

  });

  return connections;

}

export const getAttributes = (connections) => {

  let attributesMap = new Map();

  connections.map((connection) => {

      Object.keys(connection.connAttr).map(function(key, index) {
          if(attributesMap.has(key)){

              attributesMap.set( key,
                  {
                      attrs: attributesMap.get(key).attrs.add(connection.connAttr[key].attributeName),
                      type: connection.connAttr[key].type
                  }
              )

          } else {

              const newSet = new Set();
              newSet.add(connection.connAttr[key].attributeName);

              attributesMap.set( key, {
                  attrs: newSet,
                  type: connection.connAttr[key].type
              });

          }  
      });  
  }); 
  
  return attributesMap;
}

export const createSubAttributes = (attributeConnection,index) => {

    if(index === attributeConnection.length - 1) {
        return {
            attributeName: attributeConnection[index].attributeName,
            attributeId: attributeConnection[index].attributeId,
            rootName: attributeConnection[index].rootName,
            rootId: attributeConnection[index].rootId,
            subAttributeConnections: null,
        }
    } else {
        return {
            attributeName: attributeConnection[index].attributeName,
            attributeId: attributeConnection[index].attributeId,
            rootName: attributeConnection[index].rootName,
            rootId: attributeConnection[index].rootId,
            subAttributeConnections: createSubAttributes(attributeConnection,++index),
        }
    }

}