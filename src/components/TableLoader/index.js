import { Skeleton } from "@mui/material";

const TableLoader = () => {
  return (
    <div style={{width: '95%',margin: 'auto'}}>
                    <Skeleton /> 
                    <Skeleton height={90}/> 
                    <Skeleton height={90}/> 
                    <Skeleton height={90}/> 
                    <Skeleton height={90}/> 
                    <Skeleton height={90}/> 
                  </div>
  )
}

export default TableLoader;
