export default function PageContainer({ children, title }) {
  return (
    <div className="page-body">
      {title && (
        <div className="title-header">
          <h5>{title}</h5>
        </div>
      )}
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">{children}</div>
        </div>
      </div>
    </div>
  );
}
