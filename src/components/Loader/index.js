import React from "react";
import { Circles } from "react-loading-icons";

const Loader = () => {
  return <Circles fill="#63449b" height="70px" style={{ margin: "220px auto", display: "flex" }} />;
};

export default Loader;
