export default function PageCard({ children, title }) {
  return (
    <div className="card" style={{ marginTop: 15 }}>
      <div className="card-body">
        <div className="bg-inner cart-section order-details-table">
          <div className="row g-4">
            {title && (
              <div className="card-header-2" style={{ marginBottom: 20 }}>
                <h5>{title}</h5>
              </div>
            )}
            {children}
          </div>
        </div>
      </div>
    </div>
  );
}
