import MDPagination from "components/MDPagination";

import { Icon } from "@mui/material";

export default function Pagination({ setPage, totalPages, currentPage }) {
  function handlePageChnage(direction) {
    if (direction === "next") setPage((oldCurrentPage) => oldCurrentPage + 1);
    else setPage((oldCurrentPage) => oldCurrentPage - 1);
  }

  const pagesArray = [];
  for (let i = 1; i <= totalPages; i++) {
    pagesArray.push(i);
  }

  return (
    <>
      {totalPages > 1 && (
        <MDPagination variant="gradient">
          <MDPagination
            disabled={currentPage === 1 ? true : false}
            item
            onClick={() => handlePageChnage("previous")}
          >
            <Icon>keyboard_arrow_left</Icon>
          </MDPagination>
          {pagesArray.map((page) => (
            <MDPagination
              onClick={() => setPage(page)}
              key={page}
              item
              active={currentPage === page ? true : false}
            >
              {page}
            </MDPagination>
          ))}
          <MDPagination
            disabled={currentPage === totalPages ? true : false}
            item
            onClick={() => handlePageChnage("next")}
          >
            <Icon>keyboard_arrow_right</Icon>
          </MDPagination>
        </MDPagination>
      )}
    </>
  );
}
